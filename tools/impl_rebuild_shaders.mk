where-am-i = $(CURDIR)/$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THIS_MAKEFILE := $(call where-am-i)

BGFX_DIR=$(realpath ../api/bgfx)
RUNTIME_DIR=$(realpath ../bin/data)
BUILD_DIR=$(realpath ../.build)

include $(BGFX_DIR)/scripts/shader.mk

SHADERS_MASTERFOLDER=../Lattice3D/src/graphics/shaders
SUBDIRS:=$(wildcard $(SHADERS_MASTERFOLDER)/*/.)

rebuildall:$(SUBDIRS)
$(SUBDIRS):
	@make -f $(THIS_MAKEFILE) -s --no-print-directory TARGET=0 SHADERS_DIR=$@/ rebuild
	@make -f $(THIS_MAKEFILE) -s --no-print-directory TARGET=1 SHADERS_DIR=$@/ rebuild
	@make -f $(THIS_MAKEFILE) -s --no-print-directory TARGET=2 SHADERS_DIR=$@/ rebuild
	@make -f $(THIS_MAKEFILE) -s --no-print-directory TARGET=3 SHADERS_DIR=$@/ rebuild
	@make -f $(THIS_MAKEFILE) -s --no-print-directory TARGET=4 SHADERS_DIR=$@/ rebuild
	@make -f $(THIS_MAKEFILE) -s --no-print-directory TARGET=5 SHADERS_DIR=$@/ rebuild

.PHONY:rebuildall $(SUBDIRS)
