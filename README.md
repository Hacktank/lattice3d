# README #

### What is this repository for? ###
**Lattice3D** is a 3D game engine that I am writing mostly as a learning exercise. Its design goals are to provide an easy to use, intuitive, but still very powerful framework on which realtime 3D interactive applications can be built.

### How do I get set up? ###

Lattice3D is currently not set up to compile as a library, rather as an executable. There is a default demo level, which is being used as a testbed for whatever features are currently being worked on. Look in Latttice3D/src/content/...

Lattice3D compiles under visual studio 2015. It may work with other compilers, I havn't tested it.

Simply download the repo, open Lattice3D.sln, set release mode, and press f5.

### Who do I talk to? ###

My name is Jacob Tyndall, you can contact me via my email: Jacob.N.Tyndall@gmail.com