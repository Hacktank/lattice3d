#pragma once

#include "core/EngineTypes.h"

#include "core/pool_vector.h"

#include "shaders/MaterialInstance.h"

class ShaderResource;

class Renderable {
	//////////////////////////////////////////////////////////////////////////
	// STATIC
private:
	static pool_vector<Renderable*> __renderableRegistry;

	static void __registerRenderableObject(Renderable* object);
	static void __unregisterRenderableObject(Renderable* object);

public:
	static void renderAll();

	//////////////////////////////////////////////////////////////////////////
	// NON - STATIC
	bool isRenderingEnabled;
public:
	std::shared_ptr<IMaterialInstance> materialInstance;

	Renderable();
	Renderable(std::shared_ptr<IMaterialInstance>& materialInstance);
	virtual ~Renderable();

	virtual void render() = 0;
};
