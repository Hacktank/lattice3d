#pragma once

#include "core/EngineResource.h"

#include <bgfx/bgfx.h>

class ShaderResource : public EngineResource<ShaderResource> {
	friend class EngineResource<ShaderResource>;

private:
	static constexpr const engine_cstring _autoNameFormat = STR("Unnamed ShaderResource %i");

	ShaderResource(const engine_string& name);
	~ShaderResource();

	void invalidateInternals() override;

public:
	bgfx::ProgramHandle handleShaderProgram;

	static std::shared_ptr<ShaderResource> createFromFile(const engine_string& name,
														  const engine_string& vsName,
														  const engine_string& fsName,
														  NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);
};
