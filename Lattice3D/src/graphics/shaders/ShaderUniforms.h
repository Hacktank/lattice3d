#pragma once

#include <bgfx/bgfx.h>

#include "core/EngineSettings.h"

template<class DerivedType,unsigned int NumVec4>
class ShaderUniforms {
public:
	enum {
		NumVec4 = NumVec4
	};

private:
	static bgfx::UniformHandle u_params;
	static bgfx::UniformHandle u_lat_NormalMatrix;
	static bgfx::UniformHandle u_lat_eyePosition_world;

protected:
	virtual const float* getParamData() const = 0;

public:
	virtual void initializeStaticData() {
		assert(!isValid(u_params));
		assert(!isValid(u_lat_NormalMatrix));
		assert(!isValid(u_lat_eyePosition_world));
		u_params = bgfx::createUniform("u_params",bgfx::UniformType::Vec4,NumVec4);
		u_lat_NormalMatrix = bgfx::createUniform("u_lat_NormalMatrix",bgfx::UniformType::Mat3);
		u_lat_eyePosition_world = bgfx::createUniform("u_lat_eyePosition_world",bgfx::UniformType::Vec4);
	}

	virtual void uninitializeStaticData() {
		assert(isValid(u_params));
		assert(isValid(u_lat_NormalMatrix));
		assert(isValid(u_lat_eyePosition_world));
		bgfx::destroyUniform(u_params); u_params = BGFX_INVALID_HANDLE;
		bgfx::destroyUniform(u_lat_NormalMatrix); u_lat_NormalMatrix = BGFX_INVALID_HANDLE;
		bgfx::destroyUniform(u_lat_eyePosition_world); u_lat_eyePosition_world = BGFX_INVALID_HANDLE;
	}

	virtual void submit() {
		bgfx::setUniform(u_params,getParamData(),NumVec4);

		/*Matrix modelView = lat_graphics.runtime.mtxModel * lat_graphics.runtime.mtxView;
		modelView._14 = modelView._24 = modelView._34 = modelView._41 = modelView._42 = modelView._43 = 0;
		modelView._44 = 1;*/

		const Matrix_Packed_3x3 lat_NormalMatrix = lat_graphics.runtime.mtxModel.Invert().Transpose();
		bgfx::setUniform(u_lat_NormalMatrix,lat_NormalMatrix.data);

		const Vector_Packed_4x1 lat_eyePosition_world(lat_graphics.runtime.vecEyePosition,1);
		bgfx::setUniform(u_lat_eyePosition_world,lat_eyePosition_world.data);
	}
};

template<class DerivedType,unsigned int NumVec4>
bgfx::UniformHandle ShaderUniforms<DerivedType,NumVec4>::u_params = BGFX_INVALID_HANDLE;

template<class DerivedType,unsigned int NumVec4>
bgfx::UniformHandle ShaderUniforms<DerivedType,NumVec4>::u_lat_eyePosition_world = BGFX_INVALID_HANDLE;

template<class DerivedType,unsigned int NumVec4>
bgfx::UniformHandle ShaderUniforms<DerivedType,NumVec4>::u_lat_NormalMatrix = BGFX_INVALID_HANDLE;
