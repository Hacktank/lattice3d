#pragma once

#include "core/EngineTypes.h"

#include <bgfx/bgfx.h>
#include <memory>

class IMaterialInstance {
protected:
	IMaterialInstance() {}
	virtual ~IMaterialInstance() {}

public:
	// MUST ONLY BE CALLED FROM THE MAIN THREAD
	virtual uint32 updateAndSubmit(uint8 viewId,int32 depth = 0,bool preserveState = false) = 0;
};

template<class DerivedType>
class MaterialInstance : public IMaterialInstance {
private:
	static uint32 _numInstancesUsingResources;

protected:
	// custom _allocate and _delete so virtual methods can be called on construction and destruction
	template<typename ...Args>
	static DerivedType* _allocate(Args ...args) {
		DerivedType* ptr = new DerivedType(std::forward<Args>(args)...);

		if(_numInstancesUsingResources++ == 0) ptr->createResources();

		return ptr;
	}

	static void _delete(MaterialInstance* ptr) {
		if(--_numInstancesUsingResources == 0) ptr->destroyResources();
	}

	MaterialInstance() {}
	virtual ~MaterialInstance() {}

	virtual void createResources() = 0;
	virtual void destroyResources() = 0;

public:
	// MUST ONLY BE CALLED FROM THE MAIN THREAD
	virtual uint32 updateAndSubmit(uint8 viewId,int32 depth = 0,bool preserveState = false) = 0;
};

template<class DerivedType>
uint32 MaterialInstance<DerivedType>::_numInstancesUsingResources = 0;
