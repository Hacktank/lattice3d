#pragma once

#include "graphics/shaders/MaterialInstance.h"

#include "graphics/Color.h"

class MatInst_SimpleColor : public MaterialInstance<MatInst_SimpleColor> {
	friend class MaterialInstance<MatInst_SimpleColor>;

	static constexpr const char* _shaderName = "SimpleColor";

private:
	static bgfx::ProgramHandle s_handle_program;

protected:
	MatInst_SimpleColor(ColorRGBA colorMult);
	~MatInst_SimpleColor();

	void createResources() override;
	void destroyResources() override;

public:
	struct Uniforms {
		/////////////////////////
		// STATIC
		enum { NumVec4 = 1 };
		static bgfx::UniformHandle u_params;
		static void init() {
			u_params = bgfx::createUniform("u_params",bgfx::UniformType::Vec4,NumVec4);
		}
		static void submit(const Uniforms& uniforms) {
			bgfx::setUniform(u_params,uniforms.m_params,NumVec4);
		}
		static void destroy() {
			bgfx::destroyUniform(u_params);
		}

		/////////////////////////
		// DYNAMIC
		union {
			struct {
				/*0*/struct { ColorRGBA_Packed_Float4 m_colorMult; };
			};

			float m_params[NumVec4*4];
		};

		Uniforms() {
			m_colorMult = ColorRGBA(0xffffffff);
		}
	} uniforms;

	uint32 updateAndSubmit(uint8 viewId,int32 depth = 0,bool preserveState = false) override;

	static std::shared_ptr<MatInst_SimpleColor> createInstance(ColorRGBA colorMult);
};
