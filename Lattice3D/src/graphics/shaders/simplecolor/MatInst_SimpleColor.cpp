#include "MatInst_SimpleColor.h"

#include <bgfx/framework/bgfx_utils.h>

bgfx::ProgramHandle MatInst_SimpleColor::s_handle_program = BGFX_INVALID_HANDLE;
bgfx::UniformHandle MatInst_SimpleColor::Uniforms::u_params;

void MatInst_SimpleColor::createResources() {
	char vsNameBuf[64];
	char fsNameBuf[64];

	sprintf_s(vsNameBuf,"vs_%s",_shaderName);
	sprintf_s(fsNameBuf,"fs_%s",_shaderName);

	s_handle_program = loadProgram(vsNameBuf,fsNameBuf);

	Uniforms::init();
}

void MatInst_SimpleColor::destroyResources() {
	Uniforms::destroy();
	destroyProgram(s_handle_program); s_handle_program = BGFX_INVALID_HANDLE;
}

MatInst_SimpleColor::MatInst_SimpleColor(ColorRGBA colorMult) {
	uniforms.m_colorMult = colorMult;
}

MatInst_SimpleColor::~MatInst_SimpleColor() {
}

uint32 MatInst_SimpleColor::updateAndSubmit(uint8 viewId,int32 depth /*= 0*/,bool preserveState /*= false*/) {
	uniforms.submit(uniforms);
	return bgfx::submit(viewId,s_handle_program,depth,preserveState);
}

std::shared_ptr<MatInst_SimpleColor> MatInst_SimpleColor::createInstance(ColorRGBA colorMult) {
	return std::shared_ptr<MatInst_SimpleColor>(MatInst_SimpleColor::_allocate(colorMult),MatInst_SimpleColor::_delete);
}
