// shadertype=glsl

$input a_position

#include <bgfx/framework/common.sh>
#include "uniforms.sch"

void main() {
	gl_Position = mul(u_modelViewProj,vec4(a_position,1.0));
}
