#include "MatInst_Wireframe.h"

#include <bgfx/framework/bgfx_utils.h>

bgfx::ProgramHandle MatInst_Wireframe::s_handle_program = BGFX_INVALID_HANDLE;

void MatInst_Wireframe::createResources() {
	assert(!isValid(s_handle_program));

	char vsNameBuf[64];
	char fsNameBuf[64];

	sprintf_s(vsNameBuf,"vs_%s",_shaderName);
	sprintf_s(fsNameBuf,"fs_%s",_shaderName);

	s_handle_program = loadProgram(vsNameBuf,fsNameBuf);

	uniforms.initializeStaticData();
}

void MatInst_Wireframe::destroyResources() {
	assert(isValid(s_handle_program));

	uniforms.uninitializeStaticData();
	destroyProgram(s_handle_program); s_handle_program = BGFX_INVALID_HANDLE;
}

MatInst_Wireframe::MatInst_Wireframe(float opacity,
									 float thickness) {
	uniforms.m_opacity = opacity;
	uniforms.m_thickness = thickness;
}

MatInst_Wireframe::~MatInst_Wireframe() {
}

uint32 MatInst_Wireframe::updateAndSubmit(uint8 viewId,int32 depth /*= 0*/,bool preserveState /*= false*/) {
	uniforms.submit();
	return bgfx::submit(viewId,s_handle_program,depth,preserveState);
}

std::shared_ptr<MatInst_Wireframe> MatInst_Wireframe::createInstance(float opacity,
																	 float thickness) {
	return std::shared_ptr<MatInst_Wireframe>(MatInst_Wireframe::_allocate(opacity,
																		   thickness),MatInst_Wireframe::_delete);
}