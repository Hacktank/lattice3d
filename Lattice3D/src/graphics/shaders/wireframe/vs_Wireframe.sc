// shadertype=glsl

$input a_position, a_color, a_bary
$output v_color, v_bary

#include <bgfx/framework/common.sh>
#include "uniforms.sch"

void main() {
	v_color = a_color;
	v_bary = a_bary;
	gl_Position = mul(u_viewProj,vec4(a_position,1.0));
}
