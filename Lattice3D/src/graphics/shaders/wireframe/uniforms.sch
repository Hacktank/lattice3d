// shadertype=glsl

#include "../engine_uniforms.sch"

uniform vec4 u_params[1];
#define u_opacity               u_params[0].x
#define u_thickness             u_params[0].y
