// shadertype=glsl

$input v_color, v_bary

#include <bgfx/framework/common.sh>
#include "uniforms.sch"

void main() {
	vec3 color   = v_color.rgb;
	float opacity = u_opacity * v_color.a;
	float thickness = u_thickness;

	if(!gl_FrontFacing) { opacity *= 0.5; }

	vec3 fw = abs(dFdx(v_bary)) + abs(dFdy(v_bary));
	vec3 val = smoothstep(vec3_splat(0.0), fw*thickness, v_bary);
	float edge = min(min(val.x, val.y), val.z); // Gets to 0.0 around the edges.

	vec4 rgba = vec4(color, (1.0-edge)*opacity);
	gl_FragColor = rgba;
}
