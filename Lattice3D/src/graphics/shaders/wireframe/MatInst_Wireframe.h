#pragma once

#include "graphics/shaders/MaterialInstance.h"

#include "graphics/Color.h"

#include "core/EngineSettings.h"

#include "graphics/shaders/ShaderUniforms.h"

// NOTE: requires verticies with barycentric coordinates and color!!!
class MatInst_Wireframe : public MaterialInstance<MatInst_Wireframe> {
	friend class MaterialInstance<MatInst_Wireframe>;

	static constexpr const char* _shaderName = "Wireframe";

private:
	static bgfx::ProgramHandle s_handle_program;

protected:
	MatInst_Wireframe(float opacity,
					  float thickness);
	~MatInst_Wireframe();

	void createResources() override;
	void destroyResources() override;

public:
	struct Uniforms : public ShaderUniforms<Uniforms,1> {
		const float* getParamData() const override { return (float*)m_params; }

		union {
			struct {
				/*0*/struct { float m_opacity; float m_thickness; float _unused01[2]; };
			};

			float m_params[NumVec4*4];
		};

		Uniforms() {
			m_opacity = 0.7f;
			m_thickness = 1.5f;
		}
	} uniforms;

	uint32 updateAndSubmit(uint8 viewId,int32 depth = 0,bool preserveState = false) override;

	static std::shared_ptr<MatInst_Wireframe> createInstance(float opacity,
															 float thickness);
};
