vec4 v_color      : COLOR = vec4(1.0, 1.0, 1.0, 1.0);
vec3 v_bary       : TEXCOORD7 = vec3(0.0, 0.0, 0.0);

vec3 a_position   : POSITION;
vec4 a_color      : COLOR;
vec3 a_bary       : TEXCOORD7;
