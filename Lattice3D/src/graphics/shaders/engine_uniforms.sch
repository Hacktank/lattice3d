// shadertype=glsl

// equals model.inverse().transpose()
uniform mat3 u_lat_NormalMatrix;
uniform vec3 u_lat_eyePosition_world;

#define u_lat_wireframe_opacity 0.7
#define u_lat_wireframe_thickness 1.5