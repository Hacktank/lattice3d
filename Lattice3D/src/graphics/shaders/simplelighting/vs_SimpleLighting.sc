// shadertype=glsl

$input a_position, a_normal
$output v_normal_world, v_surfacePos_world

#include <bgfx/framework/common.sh>
#include "uniforms.sch"

void main() {
	//vec4 normal = a_normal * 2.0 - 1.0;
	//vec3 wnormal = mul(u_model[0], vec4(normal.xyz, 0.0) ).xyz;
	//vec3 viewNormal = normalize(mul(u_view, vec4(wnormal, 0.0) ).xyz);
	//v_normal = normalize(mul(u_model[0], vec4(a_normal.xyz, 0.0) ).xyz);
	//v_normal.xyz = mul(u_lat_NormalMatrix,a_normal.xyz);

	v_normal_world = normalize(mul(u_lat_NormalMatrix,a_normal));
	v_surfacePos_world = mul(u_model[0],vec4(a_position,1.0)).xyz;

	gl_Position = mul(u_modelViewProj,vec4(a_position,1.0));
	//v_color0 = a_color0 * u_colorMult;
}
