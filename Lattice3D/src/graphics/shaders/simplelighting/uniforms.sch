// shadertype=glsl

#include "../engine_uniforms.sch"

uniform vec4 u_params[4];
#define u_colorMult             u_params[0]
#define m_lightPosition         u_params[1]
#define u_lightColor            u_params[2]
#define u_lightAmbientStrength  u_params[3].x
#define u_lightAttenuationCoef  u_params[3].y
