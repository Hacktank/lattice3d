#pragma once

#include "graphics/shaders/MaterialInstance.h"

#include "graphics/Color.h"

#include "core/EngineSettings.h"

#include "graphics/shaders/ShaderUniforms.h"

class MatInst_SimpleLighting : public MaterialInstance<MatInst_SimpleLighting> {
	friend class MaterialInstance<MatInst_SimpleLighting>;

	static constexpr const char* _shaderName = "SimpleLighting";

private:
	static bgfx::ProgramHandle s_handle_program;

protected:
	MatInst_SimpleLighting(ColorRGBA colorMult,
						   const Vector4& lightPosition,
						   float lightAmbientStrength,
						   float lightAttenuationCoef,
						   ColorRGBA lightColor);
	~MatInst_SimpleLighting();

	void createResources() override;
	void destroyResources() override;

public:
	struct Uniforms : public ShaderUniforms<Uniforms,4> {
		const float* getParamData() const override { return (float*)m_params; }

		union {
			struct {
				/*0*/struct { ColorRGBA_Packed_Float4 m_colorMult; };
				/*1*/struct { Vector4 m_lightPosition; }; // lightPosition.w = 0 for directional light, 1 otherwise
				/*2*/struct { ColorRGBA_Packed_Float4 m_lightColor; };
				/*3*/struct { float m_lightAmbientStrength; float m_lightAttenuationCoef; float _unused01[2]; };
			};

			float m_params[NumVec4*4];
		};

		Uniforms() {
			m_colorMult = ColorRGBA(0xffffffff);
			m_lightPosition = {1,5,0,0}; // directional, mostly downward
			m_lightAmbientStrength = 0.25f;
			m_lightAttenuationCoef = 0.0000015f;
			m_lightColor = ColorRGBA(0xffffffff);
		}
	} uniforms;

	uint32 updateAndSubmit(uint8 viewId,int32 depth = 0,bool preserveState = false) override;

	static std::shared_ptr<MatInst_SimpleLighting> createInstance(ColorRGBA colorMult,
																  const Vector4& lightPosition,
																  float lightAmbientStrength,
																  float lightAttenuationCoef,
																  ColorRGBA lightColor);
};
