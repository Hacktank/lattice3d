vec3 v_normal_world     : NORMAL    = vec3(0.0, 0.0, 1.0);
vec3 v_surfacePos_world : TEXCOORD7 = vec3(0.0, 0.0, 0.0);

vec3 a_position   : POSITION;
vec3 a_normal     : NORMAL;
