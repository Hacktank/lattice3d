#include "MatInst_SimpleLighting.h"

#include <bgfx/framework/bgfx_utils.h>

bgfx::ProgramHandle MatInst_SimpleLighting::s_handle_program = BGFX_INVALID_HANDLE;

void MatInst_SimpleLighting::createResources() {
	assert(!isValid(s_handle_program));

	char vsNameBuf[64];
	char fsNameBuf[64];

	sprintf_s(vsNameBuf,"vs_%s",_shaderName);
	sprintf_s(fsNameBuf,"fs_%s",_shaderName);

	s_handle_program = loadProgram(vsNameBuf,fsNameBuf);

	uniforms.initializeStaticData();
}

void MatInst_SimpleLighting::destroyResources() {
	assert(isValid(s_handle_program));

	uniforms.uninitializeStaticData();
	destroyProgram(s_handle_program); s_handle_program = BGFX_INVALID_HANDLE;
}

MatInst_SimpleLighting::MatInst_SimpleLighting(ColorRGBA colorMult,
											   const Vector4& lightPosition,
											   float lightAmbientStrength,
											   float lightAttenuationCoef,
											   ColorRGBA lightColor) {
	uniforms.m_colorMult = colorMult;
	uniforms.m_lightPosition = lightPosition;
	uniforms.m_lightColor = lightColor;
	uniforms.m_lightAmbientStrength = lightAmbientStrength;
	uniforms.m_lightAttenuationCoef = lightAttenuationCoef;
}

MatInst_SimpleLighting::~MatInst_SimpleLighting() {
}

uint32 MatInst_SimpleLighting::updateAndSubmit(uint8 viewId,int32 depth /*= 0*/,bool preserveState /*= false*/) {
	uniforms.submit();
	return bgfx::submit(viewId,s_handle_program,depth,preserveState);
}

std::shared_ptr<MatInst_SimpleLighting> MatInst_SimpleLighting::createInstance(ColorRGBA colorMult,
																			   const Vector4& lightPosition,
																			   float lightAmbientStrength,
																			   float lightAttenuationCoef,
																			   ColorRGBA lightColor) {
	return std::shared_ptr<MatInst_SimpleLighting>(MatInst_SimpleLighting::_allocate(colorMult,
																					 lightPosition,
																					 lightAmbientStrength,
																					 lightAttenuationCoef,
																					 lightColor),MatInst_SimpleLighting::_delete);
}