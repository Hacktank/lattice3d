// shadertype=glsl

$input v_normal_world, v_surfacePos_world

#include <bgfx/framework/common.sh>
#include "uniforms.sch"

void main() {
	vec4 surfaceColor = u_colorMult;
	//vec3 surfaceToEye = normalize(u_lat_eyePosition_world - v_surfacePos_world);

	vec3 surfaceToLight;
	float lightAttenuation;
	if(m_lightPosition.w == 0.0) {
		// directional light	
		surfaceToLight = normalize(m_lightPosition.xyz);
		lightAttenuation = 1.0;
	} else {
		// point light
		surfaceToLight = m_lightPosition.xyz - v_surfacePos_world;
		float distanceToLight = length(m_lightPosition.xyz - v_surfacePos_world);
		surfaceToLight = surfaceToLight / distanceToLight; // normalize

		lightAttenuation = 1.0 / (1.0 + u_lightAttenuationCoef * pow(distanceToLight,2));
	}
	float diffuseCoefficient = max(0.0,dot(v_normal_world,surfaceToLight));

	vec3 lightAmbient = u_lightAmbientStrength * surfaceColor.rgb * u_lightColor.rgb;
	vec3 lightDiffuse = lightAttenuation * diffuseCoefficient * surfaceColor.rgb * u_lightColor.rgb;

	gl_FragColor = vec4(lightDiffuse + lightAmbient,surfaceColor.a);
}
