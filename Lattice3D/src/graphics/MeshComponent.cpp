#include "MeshComponent.h"

#include "MeshResource.h"
#include "ShaderResource.h"

#include "shaders/MaterialInstance.h"

#include "core/EngineSettings.h"

MeshComponent::MeshComponent(const ObjectConstructionData& ocd) : ActorComponent(ocd) {
	meshResource = nullptr;
	isRenderingEnabled = true;
	wireframe = false;
}

MeshComponent::MeshComponent(const ObjectConstructionData& ocd,const std::shared_ptr<MeshResource>& mesh,std::shared_ptr<IMaterialInstance> materialInstance) : ActorComponent(ocd),Renderable(materialInstance) {
	meshResource = mesh;
	isRenderingEnabled = true;
}

MeshComponent::~MeshComponent() {
}

void MeshComponent::render() {
	if(meshResource == nullptr || !meshResource->isValid()) return;
	if(materialInstance == nullptr) return;

	const Matrix compToWorld = getComponentToWorld();

	bgfx::setTransform(&compToWorld);
	lat_graphics.runtime.mtxModel = compToWorld;

	bgfx::setVertexBuffer(meshResource->handleVertexBuffer);

	// if we are to draw as wireframe we must create a transient index buffer holding the list
	// of lines that comprise our triangles
	if(lat_graphics.globalWireframe || wireframe) {
		bgfx::setState(BGFX_STATE_DEFAULT | BGFX_STATE_PT_LINES);
		uint32 numIndiciesAsLineList = bgfx::topologyConvert(bgfx::TopologyConvert::TriListToLineList,
															 nullptr,
															 0,
															 meshResource->indexData,
															 meshResource->numIndicies,
															 true);

		// check if there is enough space in the transient index cache
		// if there isn't, we just pass the triangle list, the third line will not be drawn
		// but it still looks ok, hopefully it wouldn't be noticed.
		if(bgfx::checkAvailTransientIndexBuffer(numIndiciesAsLineList)) {
			bgfx::TransientIndexBuffer transientIndexBuffer;
			bgfx::allocTransientIndexBuffer(&transientIndexBuffer,numIndiciesAsLineList);
			
			// transient buffers only support 16 bit indicies, and lattice3d meshes are stored as 32 bit
			// so we must go through all of the conversion results and remove any lines that contain out-of-bounds indicies
			// and copy/convert the remaining data to the 16 bit transient buffer
			std::unique_ptr<uint32[]> indexBuff32(new uint32[numIndiciesAsLineList]);

			bgfx::topologyConvert(bgfx::TopologyConvert::TriListToLineList,
								  indexBuff32.get(),
								  numIndiciesAsLineList * sizeof(uint32),
								  meshResource->indexData,
								  meshResource->numIndicies,
								  true);

			uint16* targetBuffer = reinterpret_cast<uint16*>(transientIndexBuffer.data);
			uint32 buffInsertionPosition = 0;
			for(uint32 i = 0; i < numIndiciesAsLineList; i++) {
				static const uint32 skipMask = 0xffff0000;
				if(indexBuff32[i] & skipMask) {
					if(i%2==0) {
						// skip this and next
						i += 1;
					} else {
						// skip this and previous
						buffInsertionPosition -= 1;
					}
					continue;
				}
				targetBuffer[buffInsertionPosition++] = static_cast<uint16>(indexBuff32[i]);
			}

			bgfx::setIndexBuffer(&transientIndexBuffer);
		} else {
			bgfx::setIndexBuffer(meshResource->handleIndexBuffer);
		}
	} else {
		bgfx::setState(BGFX_STATE_DEFAULT);
		bgfx::setIndexBuffer(meshResource->handleIndexBuffer);
	}

	materialInstance->updateAndSubmit(0);
}
