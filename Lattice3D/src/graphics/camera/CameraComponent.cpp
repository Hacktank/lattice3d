#include "CameraComponent.h"

#include "core/Engine.h"

#include "system/Application.h"

CameraComponent::CameraComponent(const ObjectConstructionData& ocd) : ActorComponent(ocd) {
	cameraSettings.fov = 1.1f;
	cameraSettings.nearPlaneDst = 0.1f;
	cameraSettings.farPlaneDst = 16000.0f;

	_lastFramePosition_world = Vector3::Zero;

	tickGroup = TickGroup::ENTITY;
}

CameraComponent::~CameraComponent() {
}

void CameraComponent::rotateAxis(const Vector3 &axis,float angle) {
	transform.rotation = transform.rotation * Quaternion::CreateFromAxisAngle(axis,angle);
}

void CameraComponent::rotateYaw(float angle) {
	rotateAxis(Vector3::UnitY,angle);
}

void CameraComponent::rotatePitch(float angle) {
	rotateAxis(Vector3::UnitX,angle);
}

void CameraComponent::rotateRoll(float angle) {
	rotateAxis(Vector3::UnitZ,angle);
}

void CameraComponent::setLookAt(const Vector3 &target) {
	transform.rotation = Quaternion::CreateFromRotationMatrix(lookDirection(Vector3::UnitZ,target-transform.position));
}

void CameraComponent::setLookDirection(const Vector3 &direction) {
	setLookAt(transform.position + direction);
}

Vector3 CameraComponent::getExtrapolatedVelocity() const {
	const Vector3 currentPosition_world = getComponentToWorld().Translation();
	return currentPosition_world - _lastFramePosition_world;
}

Matrix CameraComponent::getViewMatrix() const {
	Transform componentToWorld = getComponentToWorld();
	return Matrix::CreateTranslation(-componentToWorld.position) * /* Matrix::CreateScale(componentToWorld.scale) **/  Matrix::CreateFromQuaternion(componentToWorld.rotation).Invert();
}

Matrix CameraComponent::getProjectionMatrix() const {
	const float screenWidth = static_cast<float>(EngineSettings::Instance().system.application->windowWidth);
	const float screenHeight = static_cast<float>(EngineSettings::Instance().system.application->windowHeight);
	return DirectX::XMMatrixPerspectiveFovLH(cameraSettings.fov,
											 screenWidth / screenHeight,
											 cameraSettings.nearPlaneDst,
											 cameraSettings.farPlaneDst);
}

void CameraComponent::onTick(uint32 groups,engine_duration deltaTime) {
	const Vector3 currentPosition_world = getComponentToWorld().Translation();
	_lastFramePosition_world = currentPosition_world;
}