#include "MouseLookCameraComponent.h"

#include "core/Utility.h"

#include "core/Engine.h"

#include <iostream>

MouseLookCameraComponent::MouseLookCameraComponent(const ObjectConstructionData& ocd,bool operateOutsideWorld) : CameraComponent(ocd) {
	for(auto &keyCode : controlKeyCodes) {
		keyCode = -1;
	}

	setSensitivityX(0.001f);
	setSensitivityY(0.001f);
	setMoveSpeed(0.001f);

	inputEnabled = true;

	this->operateOutsideWorld = operateOutsideWorld;
	if(operateOutsideWorld) {
		tickGroup = TickGroup::PRE_GRAPHICS;
	}
}

MouseLookCameraComponent::~MouseLookCameraComponent() {
	lat_system.input.setMouseFPSMode(false);
}

void MouseLookCameraComponent::initialize(float sensitivity_x,
										  float sensitivity_y,
										  float verticalRange,
										  float movespeed,
										  int key_f,
										  int key_l,
										  int key_b,
										  int key_r,
										  int key_u,
										  int key_d,
										  int key_spd_fine,
										  int key_spd_fast,
										  int key_shift) {
	setSensitivityX(sensitivity_x);
	setSensitivityY(sensitivity_y);
	setVerticalRange(verticalRange);
	setMoveSpeed(movespeed);
	setKeyF(key_f);
	setKeyL(key_l);
	setKeyB(key_b);
	setKeyR(key_r);
	setKeyU(key_u);
	setKeyD(key_d);
	setKeySpeedFine(key_spd_fine);
	setKeySpeedFast(key_spd_fast);
	setKeyShift(key_shift);

	rotateWithMouse = false;
}

bool MouseLookCameraComponent::isInputEnabled() const {
	return inputEnabled;
}

void MouseLookCameraComponent::setInputEnabled(bool enabled) {
	inputEnabled = enabled;
}

int MouseLookCameraComponent::getKeyF() const {
	return controlKeyCodes[CML_F];
}

void MouseLookCameraComponent::setKeyF(int key) {
	controlKeyCodes[CML_F] = key;
}

int MouseLookCameraComponent::getKeyL() const {
	return controlKeyCodes[CML_L];
}

void MouseLookCameraComponent::setKeyL(int key) {
	controlKeyCodes[CML_L] = key;
}

int MouseLookCameraComponent::getKeyB() const {
	return controlKeyCodes[CML_B];
}

void MouseLookCameraComponent::setKeyB(int key) {
	controlKeyCodes[CML_B] = key;
}

int MouseLookCameraComponent::getKeyR() const {
	return controlKeyCodes[CML_R];
}

void MouseLookCameraComponent::setKeyR(int key) {
	controlKeyCodes[CML_R] = key;
}

int MouseLookCameraComponent::getKeyU() const {
	return controlKeyCodes[CML_U];
}

void MouseLookCameraComponent::setKeyU(int key) {
	controlKeyCodes[CML_U] = key;
}

int MouseLookCameraComponent::getKeyD() const {
	return controlKeyCodes[CML_D];
}

void MouseLookCameraComponent::setKeyD(int key) {
	controlKeyCodes[CML_D] = key;
}

int MouseLookCameraComponent::getKeySpeedFine() const {
	return controlKeyCodes[CML_SPD_FINE];
}

void MouseLookCameraComponent::setKeySpeedFine(int key) {
	controlKeyCodes[CML_SPD_FINE] = key;
}

int MouseLookCameraComponent::getKeySpeedFast() const {
	return controlKeyCodes[CML_SPD_FAST];
}

void MouseLookCameraComponent::setKeySpeedFast(int key) {
	controlKeyCodes[CML_SPD_FAST] = key;
}

int MouseLookCameraComponent::getKeyShift() const {
	return controlKeyCodes[CML_SHIFT];
}

void MouseLookCameraComponent::setKeyShift(int key) {
	controlKeyCodes[CML_SHIFT] = key;
}

float MouseLookCameraComponent::getSensitivityX() const {
	return mouseLookData.sensitivity.x;
}

void MouseLookCameraComponent::setSensitivityX(float sen) {
	mouseLookData.sensitivity.x = sen;
}

float MouseLookCameraComponent::getSensitivityY() const {
	return mouseLookData.sensitivity.y;
}

void MouseLookCameraComponent::setSensitivityY(float sen) {
	mouseLookData.sensitivity.y = sen;
}

float MouseLookCameraComponent::getVerticalRange() const {
	return mouseLookData.verticalRange;
}

void MouseLookCameraComponent::setVerticalRange(float verticalRange) {
	mouseLookData.verticalRange = verticalRange;
}

float MouseLookCameraComponent::getMoveSpeed() const {
	return movementSpeed;
}

void MouseLookCameraComponent::setMoveSpeed(float spd) {
	movementSpeed = spd;
}

void MouseLookCameraComponent::onTick(uint32 groups,engine_duration deltaTime) {
	__super::onTick(groups,deltaTime);

	if(operateOutsideWorld) {
		deltaTime = lat_time.timeDeltaLastFrame; // base camera movement off of real time instead of possibly scaled world time
	}

	if(inputEnabled) {
		// check for the shift key press and release to set fps mode for the mouse
		if(lat_system.input.getKeyDown(controlKeyCodes[CML_SHIFT])) {
			rotateWithMouse = true;
			lat_system.input.setMouseFPSMode(rotateWithMouse);
		} else if(lat_system.input.getKeyUp(controlKeyCodes[CML_SHIFT])) {
			rotateWithMouse = false;
			lat_system.input.setMouseFPSMode(rotateWithMouse);
		}

		// rotate the camera based on mouse movement
		if(rotateWithMouse) {
			const Vector2 mouseMovement = lat_system.input.getMouseMotion();

			const Quaternion frameYaw = Quaternion::CreateFromAxisAngle(Vector3::UnitY,mouseMovement.x * mouseLookData.sensitivity.x * (mouseLookData.invertX ? -1.0f : 1.0f));
			const Quaternion framePitch = Quaternion::CreateFromAxisAngle(Vector3::UnitX,mouseMovement.y * mouseLookData.sensitivity.y * (mouseLookData.invertY ? -1.0f : 1.0f));

			transform.rotation = framePitch * transform.rotation * frameYaw;
		}

		// check input and apply camera motion
		float speedToMove = movementSpeed * (float)deltaTime.count();
		const Matrix &cameraMatrix = transform.getMatrix();

		for(int i = 0; i < CML_NUM; i++) {
			const auto &keyCode = controlKeyCodes[i];
			if(keyCode != -1 && lat_system.input.getKeyIsPressed(keyCode)) {
				switch(i) {
					case CML_SPD_FINE: {
						speedToMove *= 0.15f;
						break;
					}
					case CML_SPD_FAST: {
						speedToMove *= 5.0f;
						break;
					}
					case CML_F: {
						transform.position += cameraMatrix.Forward() * speedToMove;
						break;
					}
					case CML_L: {
						transform.position += cameraMatrix.Left() * speedToMove;
						break;
					}
					case CML_B: {
						transform.position += cameraMatrix.Backward() * speedToMove;
						break;
					}
					case CML_R: {
						transform.position += cameraMatrix.Right() * speedToMove;
						break;
					}
					case CML_U: {
						transform.position += Vector3::Up * speedToMove;
						break;
					}
					case CML_D: {
						transform.position += Vector3::Down * speedToMove;
						break;
					}
				}
			}
		}
	}
}