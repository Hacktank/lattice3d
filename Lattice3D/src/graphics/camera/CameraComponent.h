#pragma once

#include "core/ActorComponent.h"
#include "core/Tickable.h"

class CameraComponent : public ActorComponent,public Tickable {
private:
	Vector3 _lastFramePosition_world;

public:
	struct {
		float fov;
		float nearPlaneDst;
		float farPlaneDst;
	} cameraSettings;

	CameraComponent(const ObjectConstructionData& ocd);
	virtual ~CameraComponent();

	virtual void rotateAxis(const Vector3 &axis,float angle);
	virtual void rotateYaw(float angle);
	virtual void rotatePitch(float angle);
	virtual void rotateRoll(float angle);

	virtual void setLookAt(const Vector3 &target);
	virtual void setLookDirection(const Vector3 &direction);

	virtual Vector3 getExtrapolatedVelocity() const;

	virtual Matrix getViewMatrix() const;
	virtual Matrix getProjectionMatrix() const;

	// Tickable
	virtual void onTick(uint32 groups,engine_duration deltaTime);
};
