#pragma once

#include "CameraComponent.h"

#include <array>

class MouseLookCameraComponent : public CameraComponent {
private:
	enum {
		CML_SPD_FINE = 0,
		CML_SPD_FAST,
		CML_SHIFT,
		CML_F,
		CML_L,
		CML_B,
		CML_R,
		CML_U,
		CML_D,

		CML_NUM
	};

	bool inputEnabled;
	std::array<uint32,CML_NUM> controlKeyCodes;
	float movementSpeed;
	bool rotateWithMouse;

	bool operateOutsideWorld;

	struct {
		bool invertX;
		bool invertY;

		Vector2 sensitivity;
		float verticalRange;

		float rotationY;
	} mouseLookData;

public:
	MouseLookCameraComponent(const ObjectConstructionData& ocd,bool operateOutsideWorld = true);
	virtual ~MouseLookCameraComponent();

	void initialize(float sensitivity_x,
					float sensitivity_y,
					float verticalRange,
					float movespeed,
					int key_f,
					int key_l,
					int key_b,
					int key_r,
					int key_u,
					int key_d,
					int key_spd_fine,
					int key_spd_fast,
					int key_shift);

	bool isInputEnabled() const;
	void setInputEnabled(bool enabled);

	int getKeyF() const;
	void setKeyF(int key);

	int getKeyL() const;
	void setKeyL(int key);

	int getKeyB() const;
	void setKeyB(int key);

	int getKeyR() const;
	void setKeyR(int key);

	int getKeyU() const;
	void setKeyU(int key);

	int getKeyD() const;
	void setKeyD(int key);

	int getKeySpeedFine() const;
	void setKeySpeedFine(int key);

	int getKeySpeedFast() const;
	void setKeySpeedFast(int key);

	int getKeyShift() const;
	void setKeyShift(int key);

	float getSensitivityX() const;
	void setSensitivityX(float sen);

	float getSensitivityY() const;
	void setSensitivityY(float sen);

	float getVerticalRange() const;
	void setVerticalRange(float verticalRange);

	float getMoveSpeed() const;
	void setMoveSpeed(float spd);

	// Tickable
	virtual void onTick(uint32 groups,engine_duration deltaTime);
};
