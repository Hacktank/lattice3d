#pragma once

#include "core/EngineResource.h"

#include <bgfx/bgfx.h>
#include <functional>
#include <memory.h>

#include <VHACD.h>
#include "core/ConvexHull.h"

class ConvexHullResource;

class MeshResource : public EngineResource<MeshResource> {
	friend class EngineResource<MeshResource>;

private:
	static constexpr const engine_cstring _autoNameFormat = STR("Unnamed MeshResource %i");

	MeshResource(const engine_string& name);

	~MeshResource();

	void invalidateInternals() override;

	bool ownsLifetimeOfInternalData;

	static void processFlags(MeshResource* resource,uint32 flags);

public:
	static constexpr uint32 flg_compute_compute_convex_hull = 1<<0;
	static constexpr uint32 flg_compute_compute_convex_decomposition = 1<<1;

	struct VolumeTreeNode {
		std::shared_ptr<ConvexHullResource> hull;
		std::vector<VolumeTreeNode> children;
	};

	bgfx::VertexBufferHandle handleVertexBuffer;
	bgfx::IndexBufferHandle handleIndexBuffer;

	const bgfx::VertexDecl* vertexDecl;
	const void* vertexData;
	uint32 numVerticies;
	const uint32* indexData;
	uint32 numIndicies;

	VolumeTreeNode collisionVolumeTree;

	static std::shared_ptr<MeshResource> createFromData(const engine_string& name,
														const bgfx::VertexDecl& vertexDecl,
														const void* vertexData,
														uint32 vertexCount,
														const uint32* indexData,
														uint32 indexCount,
														bool isDataStatic,
														uint32 flags = 0,
														NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);

	static std::shared_ptr<MeshResource> createFromFile(const engine_string& name,
														const bgfx::VertexDecl& vertexDecl,
														const engine_string& fileName,
														const std::function<void(void* vertexData,uint32 numVerticies,const uint32* indicies,uint32 numIndicies,const Vector3* vertexPositions)> lam_initializeVerticies,
														uint32 flags = 0,
														NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);

	static std::shared_ptr<MeshResource> createFromConvexHull(const engine_string& name,
															  const bgfx::VertexDecl& vertexDecl,
															  const class ConvexHull& convexHull,
															  bool distinctFaces,
															  const std::function<void(void* vertexData,uint32 numVerticies,const uint32* indicies,uint32 numIndicies,const Vector3* vertexPositions)> lam_initializeVerticies,
															  uint32 flags = 0,
															  NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);

	static std::shared_ptr<MeshResource> createFromVHACDConvexHull(const engine_string& name,
																   const bgfx::VertexDecl& vertexDecl,
																   const VHACD::IVHACD::ConvexHull& convexHull,
																   const std::function<void(void* vertexData,uint32 numVerticies,const uint32* indicies,uint32 numIndicies,const Vector3* vertexPositions)> lam_initializeVerticies,
																   uint32 flags = 0,
																   NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);
};
