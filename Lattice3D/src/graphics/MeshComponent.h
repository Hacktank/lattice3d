#pragma once

#include "core/ActorComponent.h"

#include "Renderable.h"

#include "shaders/MaterialInstance.h"

class MeshComponent : public ActorComponent,public Renderable {
public:
	bool isRenderingEnabled;
	std::shared_ptr<class MeshResource> meshResource;
	bool wireframe;

	MeshComponent(const ObjectConstructionData& ocd);
	MeshComponent(const ObjectConstructionData& ocd,const std::shared_ptr<MeshResource>& mesh,std::shared_ptr<IMaterialInstance> materialInstance);
	virtual ~MeshComponent();

	virtual void render() override;
};
