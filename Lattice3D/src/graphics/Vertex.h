#pragma once

#include "core/EngineTypes.h"
#include <bgfx/framework/bgfx_utils.h>

#include "Color.h"

struct VertexPosColor {
	Vector3 position;
	ColorRGBA color_rgba;

	static void init() {
		s_decl
			.begin()
			.add(bgfx::Attrib::Position,3,bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0,4,bgfx::AttribType::Uint8,true)
			.end();
	};

	static void initMeshDefault(void* vertexData,uint32 numVertices,const uint32* indices,uint32 numIndices,const Vector3* vertexPositions);

	static bgfx::VertexDecl s_decl;
};

struct VertexPosNormalTexCoord {
	Vector3 position;
	Vector3 normal;
	Vector2 texCoord;

	static void init() {
		s_decl
			.begin()
			.add(bgfx::Attrib::Position,3,bgfx::AttribType::Float)
			.add(bgfx::Attrib::Normal,3,bgfx::AttribType::Float)
			.add(bgfx::Attrib::TexCoord0,2,bgfx::AttribType::Float)
			.end();
	};

	static void initMeshDefault(void* vertexData,uint32 numVertices,const uint32* indices,uint32 numIndices,const Vector3* vertexPositions);

	static bgfx::VertexDecl s_decl;
};

struct VertexPosColorBarycentric {
	Vector3 position;
	ColorRGBA color_rgba;
	Vector3 barycentric;

	static void init() {
		s_decl
			.begin()
			.add(bgfx::Attrib::Position,3,bgfx::AttribType::Float)
			.add(bgfx::Attrib::Color0,4,bgfx::AttribType::Uint8,true)
			.add(bgfx::Attrib::TexCoord7,3,bgfx::AttribType::Float)
			.end();
	};

	static void initMeshDefault(void* vertexData,uint32 numVertices,const uint32* indices,uint32 numIndices,const Vector3* vertexPositions);

	static bgfx::VertexDecl s_decl;
};

typedef VertexPosNormalTexCoord LatDefaultVertex;