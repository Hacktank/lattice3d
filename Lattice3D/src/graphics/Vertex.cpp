#include "Vertex.h"

static constexpr uint32 DefaultVertexColorRGBA = 0xffa0a0a0;

void VertexPosColor::initMeshDefault(void* vertexData,uint32 numVertices,const uint32* indices,uint32 numIndices,const Vector3* vertexPositions) {
	assert(numIndices%3 == 0);

	VertexPosColor* verticies = reinterpret_cast<VertexPosColor*>(vertexData);

	// set positions and default color
	for(uint32 v = 0; v < numVertices; v++) {
		verticies[v].position = vertexPositions[v];
		verticies[v].color_rgba = DefaultVertexColorRGBA;
	}
}

bgfx::VertexDecl VertexPosColor::s_decl;

void VertexPosNormalTexCoord::initMeshDefault(void* vertexData,uint32 numVertices,const uint32* indices,uint32 numIndices,const Vector3* vertexPositions) {
	assert(numIndices%3 == 0);

	VertexPosNormalTexCoord* verticies = reinterpret_cast<VertexPosNormalTexCoord*>(vertexData);

	// set positions and calculate texture coordinates
	for(uint32 v = 0; v < numVertices; v++) {
		verticies[v].position = vertexPositions[v];
		verticies[v].normal = {0,0,0};

		// TODO: texture coords
	}

	// accumulate normals
	for(uint32 i = 0; i < numIndices; i += 3) {
		VertexPosNormalTexCoord& a = verticies[indices[i]];
		VertexPosNormalTexCoord& b = verticies[indices[i+1]];
		VertexPosNormalTexCoord& c = verticies[indices[i+2]];

		const Vector3 U = b.position - a.position;
		const Vector3 V = c.position - a.position;

		const Vector3 triangleNormal = (b.position-a.position) % (c.position-a.position);

		a.normal += triangleNormal;
		b.normal += triangleNormal;
		c.normal += triangleNormal;
	}

	// normalize all accumulated normals
	for(uint32 v = 0; v < numVertices; v++) {
		verticies[v].normal.Normalize();
	}
}

bgfx::VertexDecl VertexPosNormalTexCoord::s_decl;

void VertexPosColorBarycentric::initMeshDefault(void* vertexData,uint32 numVertices,const uint32* indices,uint32 numIndices,const Vector3* vertexPositions) {
	assert(numIndices%3 == 0);

	VertexPosColorBarycentric* verticies = reinterpret_cast<VertexPosColorBarycentric*>(vertexData);

	// set positions and color
	for(uint32 v = 0; v < numVertices; v++) {
		verticies[v].position = vertexPositions[v];
		verticies[v].color_rgba = DefaultVertexColorRGBA;
	}

	// set barycentric
	for(uint32 i = 0; i < numIndices; i += 3) {
		VertexPosColorBarycentric& a = verticies[indices[i]];
		VertexPosColorBarycentric& b = verticies[indices[i+1]];
		VertexPosColorBarycentric& c = verticies[indices[i+2]];
		a.barycentric = {1,0,0};
		b.barycentric = {0,1,0};
		c.barycentric = {0,0,1};
	}
}

bgfx::VertexDecl VertexPosColorBarycentric::s_decl;
