#include "ShaderResource.h"

#include <bgfx/framework/bgfx_utils.h>

ShaderResource::ShaderResource(const engine_string& name) :
	EngineResource(name) {
}

ShaderResource::~ShaderResource() {

}

void ShaderResource::invalidateInternals() {
	if(areInternalsValid) {
		bgfx::destroyProgram(handleShaderProgram); handleShaderProgram = BGFX_INVALID_HANDLE;

		areInternalsValid = false;
	}
}

std::shared_ptr<ShaderResource> ShaderResource::createFromFile(const engine_string& name,
															   const engine_string& vsName,
															   const engine_string& fsName,
															   NameCollisionMode nameCollisionMode) {
	auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,0);
	if(newResource->areInternalsValid) return newResource;

	newResource->handleShaderProgram = loadProgram(vsName.c_str(),fsName.c_str());

	newResource->areInternalsValid = true;
	return newResource;
}