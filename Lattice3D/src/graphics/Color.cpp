#include "Color.h"

#include "core/EngineSettings.h"

ColorRGBA colorRandomRGBA() {
	return lat_rand.getInteger()&0xffffffff;
}

ColorRGBA colorRandomRGBA(uint8 alphaOverride) {
	return (alphaOverride << 24) | (lat_rand.getInteger()&0x00ffffff);
}

ColorRGBA colorAdd(ColorRGBA col0,ColorRGBA col1,ColorFunctionAlphaMode alphaMode /*= ColorFunctionAlphaMode::CFAM_USE_FIRST*/) {
	ColorRGBA ret;

	auto lam_func = [](uint16 c0,uint16 c1)->uint8 { return uint8(clamp(c0+c1,0,255)); };
	for(uint8 i = 0; i < 3; i++) ret[i] = lam_func(col0[i],col1[i]);

	switch(alphaMode) {
		case ColorFunctionAlphaMode::CFAM_FUNCTION:
			ret.a = lam_func(col0.a,col1.a);
			break;
		case ColorFunctionAlphaMode::CFAM_USE_FIRST:
			ret.a = col0.a;
			break;
		case ColorFunctionAlphaMode::CFAM_USE_SECOND:
			ret.a = col1.a;
			break;
		case ColorFunctionAlphaMode::CFAM_AVERAGE:
			ret.a = uint8((uint16(col0.a) + uint16(col1.a))/2);
			break;
		default:
			assert(false);
	}

	return ret;
}

ColorRGBA colorScale(ColorRGBA col0,float factor,ColorFunctionAlphaMode alphaMode /*= ColorFunctionAlphaMode::CFAM_USE_FIRST*/) {
	ColorRGBA ret;

	auto lam_func = [](uint8 c0,float factor)->uint8 { return uint8(clamp(float(c0) * factor,0.f,255.f)); };
	for(uint8 i = 0; i < 3; i++) ret[i] = lam_func(col0[i],factor);

	switch(alphaMode) {
		case ColorFunctionAlphaMode::CFAM_FUNCTION:
			ret.a = lam_func(col0.a,factor);
			break;
		case ColorFunctionAlphaMode::CFAM_USE_FIRST:
			ret.a = col0.a;
			break;
		default:
			assert(false);
	}

	return ret;
}

ColorRGBA colorLERP(ColorRGBA col0,ColorRGBA col1,float progress,ColorFunctionAlphaMode alphaMode /*= ColorFunctionAlphaMode::CFAM_FUNCTION*/) {
	ColorRGBA ret;

	auto lam_func = [](uint8 c0,uint8 c1,float progress)->uint8 { return uint8(int16(c0) + int16(float(int16(c1)-int16(c0)) * progress)); };
	for(uint8 i = 0; i < 3; i++) ret[i] = lam_func(col0[i],col1[i],progress);

	switch(alphaMode) {
		case ColorFunctionAlphaMode::CFAM_FUNCTION:
			ret.a = lam_func(col0.a,col1.a,progress);
			break;
		case ColorFunctionAlphaMode::CFAM_USE_FIRST:
			ret.a = col0.a;
			break;
		case ColorFunctionAlphaMode::CFAM_USE_SECOND:
			ret.a = col1.a;
			break;
		case ColorFunctionAlphaMode::CFAM_AVERAGE:
			ret.a = (col0.a + col1.a)/2;
			break;
		default:
			assert(false);
	}

	return ret;
}

ColorRGBA colorAverage(ColorRGBA col0,ColorRGBA col1,ColorFunctionAlphaMode alphaMode /*= ColorFunctionAlphaMode::CFAM_USE_FIRST*/) {
	ColorRGBA ret;

	auto lam_func = [](uint8 c0,uint8 c1)->uint8 { return uint8((uint16(c1)+uint16(c0))/2); };
	for(uint8 i = 0; i < 3; i++) ret[i] = lam_func(col0[i],col1[i]);

	switch(alphaMode) {
		case ColorFunctionAlphaMode::CFAM_FUNCTION:
			ret.a = lam_func(col0.a,col1.a);
			break;
		case ColorFunctionAlphaMode::CFAM_USE_FIRST:
			ret.a = col0.a;
			break;
		case ColorFunctionAlphaMode::CFAM_USE_SECOND:
			ret.a = col1.a;
			break;
		case ColorFunctionAlphaMode::CFAM_AVERAGE:
			ret.a = uint8((uint16(col0.a) + uint16(col1.a))/2);
			break;
		default:
			assert(false);
	}

	return ret;
}
