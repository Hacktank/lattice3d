#pragma once

#include "core/EngineTypes.h"

#include <cstring>

inline uint32 color_rgba_to_abgr(uint32 rgba) {
	return ((rgba&0xff)<<24) | ((rgba&0x00ff)<<8) | ((rgba&0x0000ff)>>8) | ((rgba&0x000000ff)>>24);
}

struct ColorRGBA_Packed_Float4 {
	union {
		struct {
			float r,g,b,a;
		};
		float data[4];
	};
};

// 0xRRGGBBAA
struct ColorRGBA {
	union {
		struct {
			uint8 r,g,b,a;
		};
		uint8 data[4];
		uint32 rgba;
	};

	ColorRGBA() {};
	ColorRGBA(const ColorRGBA& color) : r(color.r),g(color.g),b(color.b),a(color.a) {}
	ColorRGBA(uint8 r,uint8 g,uint8 b,uint8 a) : r(r),g(g),b(b),a(a) {}
	ColorRGBA(uint32 rgba) : rgba(rgba) {}
	ColorRGBA(DXColor dxColor) : ColorRGBA(dxColor.RGBA().v) {}

	ColorRGBA& operator=(const ColorRGBA& l) { memcpy(data,l.data,sizeof(data)); return *this; }
	bool operator==(const ColorRGBA& l) const { return data == l.data; }

	uint8& operator[](int i) { return data[i]; }
	const uint8& operator[](int i) const { return data[i]; }

	operator uint32() const { return rgba; }
	operator ColorRGBA_Packed_Float4() const { return packFloat4(); }

	ColorRGBA_Packed_Float4 packFloat4() const {
		return{
			float(data[0])/255.0f,
			float(data[1])/255.0f,
			float(data[2])/255.0f,
			float(data[3])/255.0f
		};
	}
};

enum class ColorFunctionAlphaMode {
	CFAM_FUNCTION, // treat alpha the same as other channels
	CFAM_USE_FIRST, // use only the first given color's alpha
	CFAM_USE_SECOND, // use only the second given color's alpha
	CFAM_AVERAGE // average the two given color's alphas together
};

ColorRGBA colorRandomRGBA();
ColorRGBA colorRandomRGBA(uint8 alphaOverride);
ColorRGBA colorAdd(ColorRGBA col0,ColorRGBA col1,ColorFunctionAlphaMode alphaMode = ColorFunctionAlphaMode::CFAM_USE_FIRST);
ColorRGBA colorScale(ColorRGBA col0,float factor,ColorFunctionAlphaMode alphaMode = ColorFunctionAlphaMode::CFAM_USE_FIRST);
ColorRGBA colorLERP(ColorRGBA col0,ColorRGBA col1,float progress,ColorFunctionAlphaMode alphaMode = ColorFunctionAlphaMode::CFAM_FUNCTION);
ColorRGBA colorAverage(ColorRGBA col0,ColorRGBA col1,ColorFunctionAlphaMode alphaMode = ColorFunctionAlphaMode::CFAM_USE_FIRST);
