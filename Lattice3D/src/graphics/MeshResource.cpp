#include "MeshResource.h"

#include "core/ConvexHull.h"

#include <memory>

#include <bgfx/framework/bgfx_utils.h>
#include <stdio.h>
#include <algorithm>
#include <fstream>

#include "core/ConvexHullResource.h"
#include "core/EngineSettings.h"

bool loadOFF(const string_normal& fileName,std::vector<Vector3>& points,std::vector<uint32>& indicies) {
	// NOTE: the .off files I have are wound in the opposite direction that bgfx needs, this function twiddles them!

	// code adapted from https://github.com/kmammou/v-hacd
	std::ifstream is(fileName,std::ifstream::in);

	if(is) {
		const string_normal strOFF("OFF");
		string_normal firstLine;

		std::getline(is,firstLine);
		if(firstLine != strOFF) {
			printf("Loading error: format not recognized\n");
			return false;
		} else {
			int nv = 0;
			int nf = 0;
			int ne = 0;

			is >> nv >> nf >> ne;

			points.resize(nv);
			indicies.clear();
			for(int v = 0; v < nv; v++) {
				for(int vi = 0; vi < 3; vi++) {
					is >> points[v][vi];
				}
			}
			int s;
			for(int t = 0; t < nf; ++t) {
				is >> s;
				if(s == 3) {
					// NOTE: twiddling the winding direction!
					uint32 a,b,c;
					is >> a >> b >> c;

					indicies.push_back(c);
					indicies.push_back(b);
					indicies.push_back(a);
				} else {
					uint32 a,b,c,d;
					is >> a >> b >> c >> d;

					indicies.push_back(c);
					indicies.push_back(b);
					indicies.push_back(a);

					indicies.push_back(b);
					indicies.push_back(d);
					indicies.push_back(a);
				}
			}
		}
	} else {
		printf("IO Error: cannot open file '%s'",fileName.c_str());
		return false;
	}
	return true;
}

MeshResource::MeshResource(const engine_string& name) : EngineResource(name) {
	ownsLifetimeOfInternalData = false;
	vertexData = nullptr;
	numVerticies = 0;
	indexData = nullptr;
	numIndicies = 0;
}

MeshResource::~MeshResource() {
}

void MeshResource::invalidateInternals() {
	if(areInternalsValid) {
		bgfx::destroyVertexBuffer(handleVertexBuffer); handleVertexBuffer = BGFX_INVALID_HANDLE;
		bgfx::destroyIndexBuffer(handleIndexBuffer); handleIndexBuffer = BGFX_INVALID_HANDLE;

		if(ownsLifetimeOfInternalData) {
			delete[] vertexData;
			delete[] indexData;
			ownsLifetimeOfInternalData = false;
		}

		vertexData = nullptr;
		numVerticies = 0;
		indexData = nullptr;
		numIndicies = 0;

		areInternalsValid = false;
	}
}

void MeshResource::processFlags(MeshResource* resource,uint32 flags) {
	const uint32 computeConvexHull = flags & flg_compute_compute_convex_hull;
	const uint32 decomposeConvexHulls = flags & flg_compute_compute_convex_decomposition;

	if(computeConvexHull || decomposeConvexHulls) {
		uint16 vertexPositionOffset = resource->vertexDecl->getOffset(bgfx::Attrib::Position);
		uint16 vertexStride = resource->vertexDecl->getStride();

		std::unique_ptr<Vector3[]> vertexPositions(new Vector3[resource->numVerticies]);
		for(uint16 i = 0; i < resource->numVerticies; i++) {
			vertexPositions[i] = *(Vector3*)&((uint8*)resource->vertexData)[vertexStride * i + vertexPositionOffset];
		}

		if(computeConvexHull) {
			auto chullResource = ConvexHullResource::createFromData(STR(""),MathUtilities::quickHull(vertexPositions.get(),resource->numVerticies));
			resource->collisionVolumeTree.hull = chullResource;
		}
		if(decomposeConvexHulls) {
			VHACD::IVHACD* meshDecomposer = VHACD::CreateVHACD();

			bool decompositionSuccessful = meshDecomposer->Compute((float*)vertexPositions.get(),
																   3,
																   resource->numVerticies,
																   (const int*)resource->indexData,
																   3,
																   resource->numIndicies/3,
																   lat_system.mesh_decomposition.par_defaultParameters);

			if(decompositionSuccessful) {
				const unsigned int numConvexHulls = meshDecomposer->GetNConvexHulls();
				VHACD::IVHACD::ConvexHull currentConvexHull;

				for(unsigned int i = 0; i < numConvexHulls; i++) {
					meshDecomposer->GetConvexHull(i,currentConvexHull);

					resource->collisionVolumeTree.children.emplace_back();
					auto& newVolumeNode = resource->collisionVolumeTree.children.back();

					newVolumeNode.hull = ConvexHullResource::createFromData(STR(""),ConvexHull::createFromHVCADConvexHull(currentConvexHull));
				}
			}

			meshDecomposer->Clean();
			meshDecomposer->Release();
		}
	}
}

std::shared_ptr<MeshResource> MeshResource::createFromData(const engine_string& name,
														   const bgfx::VertexDecl& vertexDecl,
														   const void* vertexData,
														   uint32 vertexCount,
														   const uint32* indexData,
														   uint32 indexCount,
														   bool isDataStatic,
														   uint32 flags,
														   NameCollisionMode nameCollisionMode) {
	auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,flags);
	if(newResource->areInternalsValid) return newResource;
	newResource->creationFlags = flags;

	newResource->collisionVolumeTree = VolumeTreeNode();

	if(isDataStatic) {
		const uint32 vertexBufferSize = vertexDecl.getSize(vertexCount);
		const uint32 indexBufferSize = sizeof(uint32) * indexCount;

		newResource->ownsLifetimeOfInternalData = false;
		newResource->vertexDecl = &vertexDecl;

		newResource->vertexData = vertexData;
		newResource->numVerticies = vertexCount;

		newResource->indexData = indexData;
		newResource->numIndicies = indexCount;

		newResource->handleVertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(newResource->vertexData,vertexBufferSize),vertexDecl);
		newResource->handleIndexBuffer = bgfx::createIndexBuffer(bgfx::makeRef(newResource->indexData,indexBufferSize),BGFX_BUFFER_INDEX32);
	} else {
		const uint32 vertexBufferSize = vertexDecl.getSize(vertexCount);
		const uint32 indexBufferSize = sizeof(uint32) * indexCount;

		newResource->ownsLifetimeOfInternalData = true;
		newResource->vertexDecl = &vertexDecl;

		newResource->vertexData = new uint8[vertexBufferSize];
		memcpy((void*)newResource->vertexData,vertexData,vertexBufferSize);
		newResource->numVerticies = vertexCount;

		newResource->indexData = new uint32[indexCount];
		memcpy((void*)newResource->indexData,indexData,indexBufferSize);
		newResource->numIndicies = indexCount;

		newResource->handleVertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(newResource->vertexData,vertexBufferSize),vertexDecl);
		newResource->handleIndexBuffer = bgfx::createIndexBuffer(bgfx::makeRef(newResource->indexData,indexBufferSize),BGFX_BUFFER_INDEX32);
	}

	processFlags(newResource.get(),flags);
	newResource->areInternalsValid = true;
	return newResource;
}

std::shared_ptr<MeshResource> MeshResource::createFromFile(const engine_string& name,
														   const bgfx::VertexDecl& vertexDecl,
														   const engine_string& fileName,
														   const std::function<void(void* vertexData,uint32 numVerticies,const uint32* indicies,uint32 numIndicies,const Vector3* vertexPositions)> lam_initializeVerticies,
														   uint32 flags,
														   NameCollisionMode nameCollisionMode) {
	auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,flags);
	if(newResource->areInternalsValid) return newResource;
	newResource->creationFlags = flags;

	newResource->collisionVolumeTree = VolumeTreeNode();

	const auto lastDotPosition = fileName.find_last_of(STR('.'));
	if(lastDotPosition != engine_string::npos) {
		engine_string fileExtention = fileName.substr(lastDotPosition+1);
		std::transform(fileExtention.begin(),fileExtention.end(),fileExtention.begin(),::tolower);

		std::vector<Vector3> vertexPositions;
		std::vector<uint32> indexData;

		if(fileExtention == STR("off")) {
			try {
				if(!loadOFF(fileName,vertexPositions,indexData)) {
					printf("IO Error: failed to load file '%s'",fileName.c_str());
					return nullptr;
				}
			} catch(const std::exception& e) {
				printf("IO Error: failed to load file '%s', exception: %s",fileName.c_str(),e.what());
				return nullptr;
			}
		}  else {
			printf("IO Error: invalid filetype '%s' deduced from filename: '%s'",fileExtention.c_str(),fileName.c_str());
			return nullptr;
		}

		const uint32 numVerts = uint32(vertexPositions.size());
		const uint32 numIndex = uint32(indexData.size());

		const uint32 vertexBufferSize = vertexDecl.getSize(numVerts);
		const uint32 indexBufferSize = sizeof(uint32) * numIndex;

		newResource->ownsLifetimeOfInternalData = true;
		newResource->vertexDecl = &vertexDecl;

		newResource->vertexData = new uint8[vertexBufferSize];
		newResource->numVerticies = numVerts;

		newResource->indexData = new uint32[numIndex];
		memcpy((void*)newResource->indexData,indexData.data(),indexBufferSize);
		newResource->numIndicies = numIndex;

		lam_initializeVerticies((void*)newResource->vertexData,uint32(vertexPositions.size()),newResource->indexData,numIndex,vertexPositions.data());

		newResource->handleVertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(newResource->vertexData,vertexBufferSize),vertexDecl);
		newResource->handleIndexBuffer = bgfx::createIndexBuffer(bgfx::makeRef(newResource->indexData,indexBufferSize),BGFX_BUFFER_INDEX32);

		processFlags(newResource.get(),flags);
		newResource->areInternalsValid = true;
		return newResource;
	} else {
		printf("IO Error: cannot determine filetype of '%s'",fileName.c_str());
	}

	return nullptr;
}

std::shared_ptr<MeshResource> MeshResource::createFromConvexHull(const engine_string& name,
																 const bgfx::VertexDecl& vertexDecl,
																 const class ConvexHull& convexHull,
																 bool distinctFaces,
																 const std::function<void(void* vertexData,uint32 numVerticies,const uint32* indicies,uint32 numIndicies,const Vector3* vertexPositions)> lam_initializeVerticies,
																 uint32 flags,
																 NameCollisionMode nameCollisionMode) {
	auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,flags);
	if(newResource->areInternalsValid) return newResource;
	newResource->creationFlags = flags;

	newResource->collisionVolumeTree = VolumeTreeNode();

	const uint32 numFaces = (uint32)convexHull.triangles.size();
	const uint32 numVerts = distinctFaces ? numFaces*3 : (uint32)convexHull.verticies.size();
	const uint32 numIndex = numFaces * 3;

	const uint32 vertexBufferSize = vertexDecl.getSize(numVerts);
	const uint32 indexBufferSize = sizeof(uint32) * numIndex;

	newResource->ownsLifetimeOfInternalData = true;
	newResource->vertexDecl = &vertexDecl;

	newResource->vertexData = new uint8[vertexBufferSize];
	newResource->numVerticies = numVerts;

	newResource->indexData = new uint32[numIndex];
	newResource->numIndicies = numIndex;

	std::unique_ptr<Vector3[]> vertexPositions(new Vector3[numVerts]);

	if(distinctFaces) {
		// vertices
		for(uint32 face = 0; face < numFaces; face++) {
			for(uint8 faceVert = 0; faceVert < 3; faceVert++) {
				vertexPositions[face*3 + faceVert] = convexHull.verticies[convexHull.triangles[face][faceVert]];
				//lam_initializeVertex(&vertexData[vertexDecl.getStride()*(face*3 + faceVert)],convexHull.verticies[convexHull.triangles[face][faceVert]]);
			}
		}

		// indices
		for(uint32 i = 0; i < numVerts; i++) {
			((uint32*)newResource->indexData)[i] = i;
		}
	} else {
		// vertices
		for(uint32 vert = 0; vert < numVerts; vert++) {
			vertexPositions[vert] = convexHull.verticies[vert];
			//lam_initializeVertex(&vertexData[vertexDecl.getStride()*(vert)],convexHull.verticies[vert]);
		}

		// indices
		for(uint32 face = 0; face < numFaces; face++) {
			for(uint8 faceVert = 0; faceVert < 3; faceVert++) {
				((uint32*)newResource->indexData)[face*3 + faceVert] = convexHull.triangles[face][faceVert];
			}
		}
	}

	lam_initializeVerticies((void*)newResource->vertexData,numVerts,newResource->indexData,numIndex,vertexPositions.get());

	newResource->handleVertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(newResource->vertexData,vertexBufferSize),vertexDecl);
	newResource->handleIndexBuffer = bgfx::createIndexBuffer(bgfx::makeRef(newResource->indexData,indexBufferSize),BGFX_BUFFER_INDEX32);

	processFlags(newResource.get(),flags);
	newResource->areInternalsValid = true;
	return newResource;
}

std::shared_ptr<MeshResource> MeshResource::createFromVHACDConvexHull(const engine_string& name,
																	  const bgfx::VertexDecl& vertexDecl,
																	  const VHACD::IVHACD::ConvexHull& convexHull,
																	  const std::function<void(void* vertexData,uint32 numVerticies,const uint32* indicies,uint32 numIndicies,const Vector3* vertexPositions)> lam_initializeVerticies,
																	  uint32 flags,
																	  NameCollisionMode nameCollisionMode) {
	auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,flags);
	if(newResource->areInternalsValid) return newResource;
	newResource->creationFlags = flags;

	newResource->collisionVolumeTree = VolumeTreeNode();

	const uint32 numFaces = convexHull.m_nTriangles;
	const uint32 numVerts = convexHull.m_nPoints;
	const uint32 numIndex = numFaces * 3;

	const uint32 vertexBufferSize = vertexDecl.getSize(numVerts);
	const uint32 indexBufferSize = sizeof(uint32) * numIndex;

	newResource->ownsLifetimeOfInternalData = true;
	newResource->vertexDecl = &vertexDecl;

	newResource->vertexData = new uint8[vertexBufferSize];
	newResource->numVerticies = numVerts;

	newResource->indexData = new uint32[numIndex];
	newResource->numIndicies = numIndex;

	std::unique_ptr<Vector3[]> vertexPositions(new Vector3[numVerts]);

	// vertices
	for(uint32 vert = 0; vert < numVerts; vert++) {
		for(uint8 i = 0; i < 3; i++) {
			vertexPositions[vert][i] = (float)convexHull.m_points[vert*3 + i];
		}
	}

	// indices
	for(uint32 face = 0; face < numFaces; face++) {
		for(uint8 faceVert = 0; faceVert < 3; faceVert++) {
			// NOTE: twiddling the winding direction!
			((uint32*)newResource->indexData)[face*3 + faceVert] = convexHull.m_triangles[face*3 + (2 - faceVert)];
		}
	}

	lam_initializeVerticies((void*)newResource->vertexData,numVerts,newResource->indexData,numIndex,vertexPositions.get());

	newResource->handleVertexBuffer = bgfx::createVertexBuffer(bgfx::makeRef(newResource->vertexData,vertexBufferSize),vertexDecl);
	newResource->handleIndexBuffer = bgfx::createIndexBuffer(bgfx::makeRef(newResource->indexData,indexBufferSize),BGFX_BUFFER_INDEX32);

	processFlags(newResource.get(),flags);
	newResource->areInternalsValid = true;
	return newResource;
}