#include "Renderable.h"

#include "ShaderResource.h"

#include "core/LatObject.h"

//////////////////////////////////////////////////////////////////////////
// STATIC
pool_vector<Renderable*> Renderable::__renderableRegistry;

void Renderable::__registerRenderableObject(Renderable* object) {
	__renderableRegistry.emplace(object);
}

void Renderable::__unregisterRenderableObject(Renderable* object) {
	__renderableRegistry.remove(object);
}

void Renderable::renderAll() {
	for(auto &renderable : __renderableRegistry) {
		renderable->render();
	}
}

//////////////////////////////////////////////////////////////////////////
// NON - STATIC
Renderable::Renderable() {
	isRenderingEnabled = true;
	materialInstance = nullptr;
	__registerRenderableObject(this);
}

Renderable::Renderable(std::shared_ptr<IMaterialInstance>& materialInstance) :
	materialInstance(materialInstance) {
	__registerRenderableObject(this);
}

Renderable::~Renderable() {
	__unregisterRenderableObject(this);
}