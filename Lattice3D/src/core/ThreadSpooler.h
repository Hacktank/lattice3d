#pragma once

#include "EngineTypes.h"

#include <functional>
#include <thread>
#include <mutex>
#include <future>
#include <condition_variable>
#include <atomic>

#include <vector>
#include <queue>

//////////////////////////////////////////////////////////////////////////
// ThreadSpooler
//////////////////////////////////////////////////////////////////////////
class ThreadSpooler {
private:
	std::vector<std::thread> workerThreads;

	struct WorkEntry {
		std::function<void(void)> func;
		std::promise<void> prom_completion;
	};
	std::queue<WorkEntry> workQueue;
	std::mutex mtx_workQueue;
	std::condition_variable cv_workAvailable;

	std::atomic<bool> shouldWorkerThreadsBeRunning;

	void workerThreadLoop(uint32 id);

public:
	ThreadSpooler();
	~ThreadSpooler();

	// creates worker threads equal to the number of detected processing cores minus the number of explicit threads already in existence
	// if the number of processing cores cannot be determined the fall back number will be used instead.
	uint32 initialize(uint32 minimumNumberOfThreads = 1);
	void uninitialize();

	uint32 getOptiomalNumConcurrentTasks() const;

	std::future<void> enqueueWork(std::function<void(void)>&& workFunction);
};