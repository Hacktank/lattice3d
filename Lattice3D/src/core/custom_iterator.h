#pragma once

// code from: http://www.drdobbs.com/the-standard-librarian-defining-iterato/184401331

template<bool flag,class IsTrue,class IsFalse>
struct choose;

template<class IsTrue,class IsFalse>
struct choose<true,IsTrue,IsFalse> {
	typedef IsTrue type;
};

template<class IsTrue,class IsFalse>
struct choose<false,IsTrue,IsFalse> {
	typedef IsFalse type;
};

template<class T,bool isconst = false>
class custom_iterator {
public:
	typedef std::forward_iterator_tag                  iterator_category;
	typedef T                                          value_type;
	typedef std::ptrdiff_t                             difference_type;
	typedef typename choose<isconst,const T&,T&>::type reference;
	typedef typename choose<isconst,const T*,T*>::type pointer;

	// all methods must be implemented in the derived class
	/*
	custom_iterator() {}
	custom_iterator(const custom_iterator<T,false> &other) {}

	reference operator*() const {}
	pointer operator->() const {}
	custom_iterator& operator++() {
		// xxx
		return *this;
	}

	custom_iterator& operator++(int) {
		// xxx
		return *this;
	}

	friend bool operator==(const custom_iterator &l,
						   const custom_iterator &r) {
		// xxx
	}

	friend bool operator!=(const custom_iterator &l,
						   const custom_iterator &r) {
		return !(l==r);
	}
	*/
};