#include "Actor.h"
#include "ActorComponent.h"
#include "physics/RigidBodyComponent.h"
#include "World.h"
#include <algorithm>

Actor::Actor(const ObjectConstructionData& ocd) : LatObject(ocd) {
	rigidBody = nullptr;
	_defaultRoot = constructObject<ActorComponent>(this,STR("DefaultRoot"));
	setDefaultRoot();
}

Actor::~Actor() {
}

void Actor::onSubObjectRegistering(LatObject *obj,uint32 depth) {
	__super::onSubObjectRegistering(obj,depth);

	// maintain the cache of actor components
	ActorComponent *obj_as_ActorComponent = dynamic_cast<ActorComponent*>(obj);
	if(obj_as_ActorComponent != nullptr) {
		_components.insert(obj_as_ActorComponent);
	}

	RigidBodyComponent* obj_as_RigidBodyComponent = dynamic_cast<RigidBodyComponent*>(obj);
	if(obj_as_RigidBodyComponent != nullptr) {
		assert(rigidBody == nullptr); // cannot have multiple rigid bodies in a single actor
		rigidBody = obj_as_RigidBodyComponent;
		rootComponent = rigidBody;
	}
}

void Actor::onSubObjectUnregistering(LatObject *obj,uint32 depth) {
	__super::onSubObjectUnregistering(obj,depth);

	// maintain the cache of actor components
	ActorComponent *obj_as_ActorComponent = dynamic_cast<ActorComponent*>(obj);
	if(obj_as_ActorComponent != nullptr) {
		_components.remove(obj_as_ActorComponent);
	}

	RigidBodyComponent* obj_as_RigidBodyComponent = dynamic_cast<RigidBodyComponent*>(obj);
	if(obj_as_RigidBodyComponent != nullptr) {
		rigidBody = nullptr;
		setDefaultRoot();
	}
}

void Actor::setDefaultRoot() {
	rootComponent = _defaultRoot;
}

World* Actor::getWorld() {
	LatObject* curObject = this->getOwner();
	while(curObject) {
		World* curObjectAsWorld = dynamic_cast<World*>(curObject);
		if(curObjectAsWorld != nullptr) return curObjectAsWorld;
		curObject = curObject->getOwner();
	}
	assert(false);
	return nullptr;
}