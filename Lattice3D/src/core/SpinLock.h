#pragma once

#include <atomic>

class ht_spinlock {
private:
	std::atomic_flag lck;

public:
	ht_spinlock() { lck.clear(); }

	inline void lock() {
		while(lck.test_and_set(std::memory_order_acquire)) {
		}
	}

	inline void unlock() {
		lck.clear(std::memory_order_release);
	}
};

class ht_spinlock_guard {
private:
	ht_spinlock& myInstance;

public:
	inline ht_spinlock_guard(ht_spinlock& instance) : myInstance(instance) {
		myInstance.lock();
	}

	inline ~ht_spinlock_guard() {
		myInstance.unlock();
	}

	ht_spinlock_guard(const ht_spinlock&) = delete;
};