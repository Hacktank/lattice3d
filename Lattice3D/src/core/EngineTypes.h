#pragma once

//#define USEWSTRING

#include "Utility.h"

#include <locale>
#include <codecvt>
#include <string>
typedef char* cstring_normal;
typedef std::string string_normal;
typedef wchar_t* cstring_wide;
typedef std::wstring string_wide;
extern std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> string_converter;
inline string_normal string_normal_from_wide(const string_wide &str) { return string_converter.to_bytes(str); }
inline string_wide string_normal_to_wide(const string_normal &str) { return string_converter.from_bytes(str); }
inline string_wide string_wide_from_normal(const string_normal &str) { return string_normal_to_wide(str); }
inline string_normal string_wide_to_normal(const string_wide &str) { return string_normal_from_wide(str); }
#ifdef USEWSTRING
typedef wchar_t engine_char;
typedef string_wide engine_string;
typedef cstring_wide engine_cstring;
FUNCTION_ALIAS(std::to_wstring,to_string);
FUNCTION_ALIAS(wcslen,string_len);
#define STR(s) (L##s)
inline string_normal string_to_normal(const engine_string &str) { return string_wide_to_normal(str); }
inline engine_string string_from_normal(const string_normal &str) { return string_wide_from_normal(str); }
inline string_wide string_to_wide(const engine_string &str) { return str; }
inline engine_string string_from_wide(const string_wide &str) { return str; }
#else
typedef char engine_char;
typedef string_normal engine_string;
typedef cstring_normal engine_cstring;
FUNCTION_ALIAS(std::to_string,to_string);
FUNCTION_ALIAS(strlen,string_len);
#define STR(s) (s)
inline string_normal string_to_normal(const engine_string &str) { return str; }
inline engine_string string_from_normal(const string_normal &str) { return str; }
inline string_wide string_to_wide(const engine_string &str) { return string_normal_to_wide(str); }
inline engine_string string_from_wide(const string_wide &str) { return string_normal_from_wide(str); }
#endif

#include <cstdint>
using uint8 = uint8_t;
using int8 = int8_t;
using uint16 = uint16_t;
using int16 = int16_t;
using uint32 = uint32_t;
using int32 = int32_t;
using uint64 = uint64_t;
using int64 = int64_t;

#include <chrono>
typedef std::chrono::high_resolution_clock                    engine_clock;
typedef std::chrono::duration<double>                         engine_duration;
typedef std::chrono::time_point<engine_clock,engine_duration> engine_time_point;

#include "EngineMath.h"
