#pragma once

class BoolVector3 {
	//////////////////////////////////////////////////////////////////////////
	// STATIC
public:
	static const BoolVector3 FFF;
	static const BoolVector3 TTT;

	//////////////////////////////////////////////////////////////////////////
	// NON - STATIC
public:
	union {
		struct {
			bool x,y,z;
		};
		bool data[3];
	};

	inline BoolVector3() {
		x = y = z = false;
	}

	inline BoolVector3(bool x,bool y,bool z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	inline BoolVector3(const BoolVector3 &other) {
		x = other.x;
		y = other.y;
		z = other.z;
	}

	inline BoolVector3& operator=(const BoolVector3 &l) {
		x = l.x;
		y = l.y;
		z = l.z;
		return *this;
	}

	bool operator==(const BoolVector3 &l) const {
		return x==l.x && y==l.y && z==l.z;
	}

	bool operator!=(const BoolVector3 &l) const {
		return !(*this == l);
	}
};
