#pragma once

#define _USE_MATH_DEFINES
#include <cmath>

#include <algorithm>

#include <SimpleMath.h>
#include <DirectXMath.h>
using Vector2 = DirectX::SimpleMath::Vector2;
using Vector3 = DirectX::SimpleMath::Vector3;
using Vector4 = DirectX::SimpleMath::Vector4;
using Matrix = DirectX::SimpleMath::Matrix;
using Quaternion = DirectX::SimpleMath::Quaternion;
using DXColor = DirectX::SimpleMath::Color;

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

#define PI 3.14159265359f

#define RAD_TO_DEG (180.0f / PI)
#define DEG_TO_RAD (PI / 180.0f)

// OPERATORS

// DOT PRODUCT
inline float operator|(const Vector3 &l,const Vector3 &r) { return l.Dot(r); }
inline float operator|(const Vector2 &l,const Vector2 &r) { return l.Dot(r); }

// CROSS PRODUCT
inline Vector3 operator%(const Vector3 &l,const Vector3 &r) { return r.Cross(l); }

// TRANSFORM VECTOR BY MATRIX
inline Vector4 operator*(const Vector4 &l,const Matrix &r) { return Vector4::Transform(l,r); }
inline Vector3 operator*(const Vector3 &l,const Matrix &r) { return Vector3::Transform(l,r); }
inline Vector2 operator*(const Vector2 &l,const Matrix &r) { return Vector2::Transform(l,r); }

namespace MathUtilities {
// FUNCTIONS
inline float lerp(float a,float b,float t) { return a*(1.0f - t) + b*(t); }

inline Vector3 minVec3(const Vector3& a,const Vector3& b) {
	return Vector3(std::min(a.x,b.x),
				   std::min(a.y,b.y),
				   std::min(a.z,b.z));
}

inline Vector3 maxVec3(const Vector3& a,const Vector3& b) {
	return Vector3(std::max(a.x,b.x),
				   std::max(a.y,b.y),
				   std::max(a.z,b.z));
}

struct Matrix_Packed_3x3 {
	union {
		struct {
			float
				_11,_12,_13,
				_21,_22,_23,
				_31,_32,_33;
		};
		float data[9];
	};

	Matrix_Packed_3x3(const Matrix& mat) :
		_11(mat._11),_12(mat._12),_13(mat._13),
		_21(mat._21),_22(mat._22),_23(mat._23),
		_31(mat._31),_32(mat._32),_33(mat._33) {
	}

	Matrix_Packed_3x3(const Matrix_Packed_3x3&) = default;
	Matrix_Packed_3x3(Matrix_Packed_3x3&&) = default;

	Matrix_Packed_3x3& operator=(const Matrix_Packed_3x3&) = default;
	Matrix_Packed_3x3& operator=(Matrix_Packed_3x3&&) = default;
};

struct Vector_Packed_4x1 {
	union {
		struct {
			float x,y,z,w;
		};
		float data[4];
	};

	Vector_Packed_4x1(float x,float y,float z,float w) :
		x(x),y(y),z(z),w(w) {
	}

	Vector_Packed_4x1(const Vector2& vec,float z,float w) :
		x(vec.x),y(vec.y),z(z),w(w) {
	}

	Vector_Packed_4x1(const Vector3& vec,float w) :
		x(vec.x),y(vec.y),z(vec.z),w(w) {
	}

	Vector_Packed_4x1(const Vector4& vec) :
		x(vec.x),y(vec.y),z(vec.z),w(vec.w) {
	}

	Vector_Packed_4x1(const Vector_Packed_4x1&) = default;
	Vector_Packed_4x1(Vector_Packed_4x1&&) = default;

	Vector_Packed_4x1& operator=(const Vector_Packed_4x1&) = default;
	Vector_Packed_4x1& operator=(Vector_Packed_4x1&&) = default;
};

Vector3 getNormal(const Vector3 &v);
void makeOrthoNormalBasis(Vector3 *x,Vector3 *y,Vector3 *z);
void orthoNormalize(Matrix *m);
Quaternion quatFromEuler(const Vector3 &euler);
//Quaternion quatAddEuler(const Quaternion &quat,const Vector3 &euler);

Vector3 perpendicular(const Vector3 &perpendicularTo);

Quaternion rotateTo(const Vector3 &q,const Vector3 &p);

Quaternion nullifyRotationOnAxis(const Quaternion &quat,const Vector3 &axis);
float getAngleBetween(const Vector3 &dir1,const Vector3 &dir2,const Vector3 &norm = Vector3::Zero,bool signedAngle = false);

Matrix skewSymmetric(const Vector3 &vector);
Matrix lookDirection(const Vector3 &localforward,const Vector3 &targetdir,const Vector3 &localup = Vector3::Up,const Vector3 &worldup = Vector3::Up);

Matrix removeScale(const Matrix &mat,Vector3* outScale = nullptr);

Quaternion quatAddAngularOffset(const Quaternion &quat,const Vector3 &offset);

Vector3 quatToEuler(const Quaternion &quat);
Quaternion quatFromEuler(const Vector3 &euler);

void barycentric(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c,float *u,float *v,float *w);
float pointDistanceFromLine(const Vector3 &p,const Vector3 &a,const Vector3 &b);
float pointDistanceFromTriangle(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c);
float pointDistanceFromPlane(const Vector3 &p,const Vector3 &a,const Vector3 &b,const Vector3 &c);
float pointDistanceFromPlane(const Vector3 &p,const Vector3 &n,const Vector3 &a);
float pointDistanceFromPlane(const Vector3 &p,const Vector3 &n,float d);

void aabbGetTransformed(const Vector3& in_min,
						const Vector3& in_max,
						const Matrix& transform,
						Vector3* out_min,
						Vector3* out_max);
}

using namespace MathUtilities;
