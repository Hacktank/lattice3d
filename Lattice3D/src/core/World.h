#pragma once

#include "ThreadSpooler.h"

#include "LatObject.h"

#include "physics/Contact.h"

#include "graphics/Renderable.h"

#include <vector>
#include "pool_vector.h"

class Actor;
class Transform;
class ThreadTask;

class World : public LatObject {
private:
	pool_vector<Actor*> _actors;
	engine_time_point _tpLastFrameTime;

	engine_duration _physicsTimeToSimulate;

	bool _isPaused;

	class CameraComponent* _activeCamera;
	class APlayer* _activePlayer;

	class PhysicsIntegrator* _physicsIntegrator;
	class ContactGenerator* _contactGenerator;
	class ContactResolver* _contactResolver;

	uint64 physicsFrameNumber;

protected:
	virtual void onSubObjectRegistering(LatObject *obj,uint32 depth) override;
	virtual void onSubObjectUnregistering(LatObject *obj,uint32 depth) override;

	virtual void updateAI();
	virtual void updateActors();
	virtual void updateSpacialPartitions();
	virtual void updatePhysics();

public:
	engine_duration gameFrameTimeDelta;
	engine_duration physicsFrameTimeDelta;
	engine_duration gameTime;
	engine_duration physicsTime;

	pool_vector<Contact> contacts;

	World(const ObjectConstructionData& ocd);
	virtual ~World();

	virtual void frame();

	virtual void setActiveCamera(CameraComponent* camera);
	virtual CameraComponent* getActiveCamera() const;

	virtual void setActivePlayer(APlayer* player);
	virtual APlayer* getActivePlayer() const;

	void setPaused(bool paused);
	bool isPaused() const;

	const pool_vector<Actor*>& getActors() const;

	PhysicsIntegrator* getPhysicsIntegrator() const;
	ContactGenerator* getContactGenerator() const;
	ContactResolver* getContactResolver() const;

	uint64 getPhysicsFrameNumber() const { return physicsFrameNumber; }

	template<typename T>
	void setPhysicsIntegrator();
	template<typename T>
	void setContactGenerator();
	template<typename T>
	void setContactResolver();
};

template<typename T>
void World::setPhysicsIntegrator() {
	static_assert(std::is_base_of<PhysicsIntegrator,T>::value,"T must derive from PhysicsIntegrator");
	if(_physicsIntegrator != nullptr) _physicsIntegrator->destroy();
	_physicsIntegrator = constructObjectNameless<T>(this);
}

template<typename T>
void World::setContactGenerator() {
	static_assert(std::is_base_of<ContactGenerator,T>::value,"T must derive from ContactGenerator");
	if(_contactGenerator != nullptr) _contactGenerator->destroy();
	_contactGenerator = constructObjectNameless<T>(this);
}

template<typename T>
void World::setContactResolver() {
	static_assert(std::is_base_of<ContactResolver,T>::value,"T must derive from ContactResolver");
	if(_contactResolver != nullptr) _contactResolver->destroy();
	_contactResolver = constructObjectNameless<T>(this);
}
