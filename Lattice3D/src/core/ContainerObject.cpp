#include "ContainerObject.h"

ContainerObject::ContainerObject(const ObjectConstructionData& ocd) : LatObject(ocd) {

}

ContainerObject::~ContainerObject() {

}

void ContainerObject::onSubObjectRegistering(LatObject *obj,uint32 depth) {
	__super::onSubObjectRegistering(obj,depth);
	if(getOwner()) getOwner()->onSubObjectRegistering(obj,depth+1);
}

void ContainerObject::onSubObjectUnregistering(LatObject *obj,uint32 depth) {
	__super::onSubObjectUnregistering(obj,depth);
	if(getOwner()) getOwner()->onSubObjectUnregistering(obj,depth+1);
}
