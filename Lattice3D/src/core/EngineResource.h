#pragma once

#include <type_traits>

#include "EngineTypes.h"

#include <string>
#include <unordered_map>
#include <memory>

class IEngineResource {
private:
	engine_string name;

protected:
	bool areInternalsValid;
	uint32 creationFlags;

	IEngineResource(const engine_string& name) :
		name(name),
		areInternalsValid(false),
		creationFlags(0) {
	}

	virtual ~IEngineResource() {
	}

public:
	enum class NameCollisionMode {
		ERNCM_RETURN_EXISTING = 0,
		ERNCM_REPLACE_IF_FLAGS_DIFFERENT
	};

	const engine_string& getName() const { return name; }
	uint32 getCreationFlags() const { return creationFlags; }

	virtual bool isValid() { return areInternalsValid; }
	virtual void invalidateInternals() { areInternalsValid = false; }

protected:
	// format uses printf rules, it should contain one %i for a unique number
	static engine_string generateAutoName(engine_cstring const format) {
		static uint32 _autoNameCounter = 1;

		engine_char buffer[256];
		sprintf_s(buffer,format,_autoNameCounter++);

		return engine_string(buffer);
	}
};

template<class DerivedType>
class EngineResource : public IEngineResource {
	//static_assert(std::is_base_of<EngineResource,DerivedType>::value,"DerivedType must derive from EngineResource");
private:
	static std::unordered_map<engine_string,std::weak_ptr<EngineResource>> _resourcesByName;

	static void __derivedDeleter(DerivedType* ptr) {
		ptr->invalidateInternals();
		delete ptr;
	}

protected:
	static void _registerInstance(const std::shared_ptr<EngineResource>& sptr) {
		assert(sptr);
		const auto name = sptr->getName();
		assert(!findByName(name));
		_resourcesByName.emplace(name,sptr);
	}

	static void _unregisterInstance(const engine_string& name) {
		auto findResult = _resourcesByName.find(name);
		if(findResult != _resourcesByName.end())
			_resourcesByName.erase(findResult);
	}

	static std::shared_ptr<DerivedType> factoryGetTargetResourceHandle(const engine_string& name,NameCollisionMode nameCollisionMode,uint32 flags) {
		auto resource = findByName(name);

		if(resource) {
			switch(nameCollisionMode) {
				case IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING:
					return resource;
					break;
				case IEngineResource::NameCollisionMode::ERNCM_REPLACE_IF_FLAGS_DIFFERENT:
					if(resource->getCreationFlags() == flags) return resource;
				default:
					resource->invalidateInternals();
			}
		} else {
			resource = std::shared_ptr<DerivedType>(new DerivedType(name),__derivedDeleter);
			_registerInstance(resource);
		}

		return resource;
	}

public:
	static std::shared_ptr<DerivedType> findByName(const engine_string& name) {
		auto findResult = _resourcesByName.find(name);
		if(findResult == _resourcesByName.end())
			return nullptr;
		return std::static_pointer_cast<DerivedType>(findResult->second.lock());
	}

protected:
	EngineResource(const engine_string& name) : IEngineResource(name) {
	}

	virtual ~EngineResource() {
		_unregisterInstance(getName());
	}
};

template<class DerivedType>
std::unordered_map<engine_string,std::weak_ptr<EngineResource<DerivedType>>> EngineResource<DerivedType>::_resourcesByName;
