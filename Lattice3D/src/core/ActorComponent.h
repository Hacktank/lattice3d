#pragma once

#include "LatObject.h"
#include "Transform.h"

#include "pool_vector.h"
#include <vector>

#include "BoolVector3.h"

class Actor;
class ColliderComponent;

class ActorComponent : public LatObject {
protected:
	ActorComponent *componentAttachedTo;
	pool_vector<ActorComponent*> componentsAttachedToMe;

	virtual void onComponentAttached(ActorComponent* comp);
	virtual void onComponentDetached(ActorComponent* comp);

public:
	Transform transform;
	BoolVector3 absolutePosition;
	BoolVector3 absoluteRotation;
	BoolVector3 absoluteScale;

	ActorComponent(const ObjectConstructionData& ocd);
	virtual ~ActorComponent();

	Actor* getActor() const;

	// is this component the root component of the actor it is attached to
	bool isRoot() const;

	virtual Matrix getComponentToWorld() const;

	void attachTo(ActorComponent *targetComponent);
	void detach();

	// CONTACT CALLBACKS ARE CURRENTLY UNIMPLEMENTED
	virtual void onContactBegin(ColliderComponent *myCollider,ColliderComponent *otherCollider);
	virtual void onContactSustained(ColliderComponent *myCollider,ColliderComponent *otherCollider);
	virtual void onContactEnd(ColliderComponent *myCollider,ColliderComponent *otherCollider);
};