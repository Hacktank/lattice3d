#pragma once

#include "LatObject.h"

class ContainerObject : public LatObject {
protected:
	virtual void onSubObjectRegistering(LatObject *obj,uint32 depth) override;
	virtual void onSubObjectUnregistering(LatObject *obj,uint32 depth) override;

public:
	ContainerObject(const ObjectConstructionData& ocd);
	virtual ~ContainerObject();
};
