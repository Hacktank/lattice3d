#pragma once

#include "EngineTypes.h"

#include <functional>
#include "pool_vector.h"

namespace TickGroup {
enum _ : uint32 {
	NONE = 1<<0,

	ENTITY = 1<<1,

	UI = 1<<2,

	PRE_PHYSICS = 1<<3,
	POST_PHYSICS = 1<<4,

	PRE_GRAPHICS = 1<<5,
	POST_GRAPHICS = 1<<6,
};
}

class Tickable {
public:
	class DiscreetTickerHandle {
		friend class Tickable;
	public:
		typedef std::function<void(uint32 groups,engine_duration deltaTime)> CallbackType;
	private:
		uint32 tickGroup;
		CallbackType func;

	public:
		void unregister();

	private:
		DiscreetTickerHandle(uint32 tickGroup,CallbackType&& func);
		~DiscreetTickerHandle();
	};

	//////////////////////////////////////////////////////////////////////////
	// STATIC
private:
	static pool_vector<Tickable*> __tickableRegistry;

	static void __registerTickableObject(Tickable* object);
	static void __unregisterTickableObject(Tickable* object);

	static pool_vector<DiscreetTickerHandle*> __discreetTickerRegistry;

public:
	static void tickGroups(uint32 groups,engine_duration deltaTime);

	static DiscreetTickerHandle* createDiscreetTicker(uint32 tickGroup,DiscreetTickerHandle::CallbackType&& func);

	//////////////////////////////////////////////////////////////////////////
	// NON - STATIC
public:
	uint32 tickGroup;

	Tickable();
	virtual ~Tickable();

	virtual void onTick(uint32 groups,engine_duration deltaTime) = 0;
};
