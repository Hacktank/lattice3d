#include "LatObject.h"

#include <algorithm>

//////////////////////////////////////////////////////////////////////////
// STATIC
std::atomic<uint32> LatObject::__nextUniqueID = 0;

pool_vector<LatObject*> LatObject::__objectRegistry;

std::queue<LatObject*> LatObject::__gcQueue;

void LatObject::__registerObject(LatObject *obj) {
	__objectRegistry.insert(obj);
}

void LatObject::__unregisterObject(LatObject *obj) {
	__objectRegistry.remove(obj);
}

bool LatObject::_isNameValid(const engine_string& name,const LatObject* parent) {
	return
		name.find(STR('/')) == engine_string::npos &&
		(!parent || parent->findSubObjectByName(name) == nullptr);
}

LatObject* LatObject::_impl_findObjectByName(const LatObject* root,const engine_string& name) {
	const auto slashPosition = name.find(STR('/'));

	auto lam_findByName = [&](const engine_string& name)->LatObject* {
		if(root == nullptr) {
			for(auto &obj : __objectRegistry) if(obj != nullptr) {
				if(obj->isValid() && obj->getName() == name) return obj;
			}
		} else {
			for(auto &obj : root->_subObjects) if(obj != nullptr) {
				if(obj->isValid() && obj->getName() == name) return obj;
			}
		}
		return nullptr;
	};

	if(slashPosition == engine_string::npos) {
		return lam_findByName(name);
	} else {
		const engine_string nameToFind = name.substr(0,slashPosition);
		const engine_string nameToPassOn = name.substr(slashPosition+1,engine_string::npos);

		LatObject* foundObject = lam_findByName(nameToFind);
		if(foundObject) return foundObject->findSubObjectByName(nameToPassOn);
	}

	return nullptr;
}

void LatObject::doGarbageCollection() {
	// obtain the list of objects to destroy and clear the static queue
	decltype(__gcQueue) objectsToDestroy;
	__gcQueue.swap(objectsToDestroy);

	while(!objectsToDestroy.empty()) {
		objectsToDestroy.front()->_deleteThis();
		objectsToDestroy.pop();
	}
}

std::vector<LatObject*> LatObject::filterObjectsByPredicate(std::function<bool(const LatObject *obj)> &&pred) {
	std::vector<LatObject*> result;
	for(auto &obj : __objectRegistry) if(obj != nullptr) {
		if(obj->isValid() && pred(obj)) result.push_back(obj);
	}
	return std::move(result);
}

LatObject* LatObject::findObjectByName(const engine_string& name) {
	return _impl_findObjectByName(nullptr,name);
}

LatObject* LatObject::findObjectByID(uint32 id) {
	for(auto &obj : __objectRegistry) if(obj != nullptr) {
		if(obj->isValid() && obj->getUniqueID() == id) return obj;
	}
	return nullptr;
}

//////////////////////////////////////////////////////////////////////////
// NON - STATIC
void LatObject::_deleteThis() {
	assert(isReadyForDestruction());
	if(_owner != nullptr) _owner->_unregisterSubObject(this);
	delete this;
}

void LatObject::_registerSubObject(LatObject *obj) {
	assert(obj != nullptr);
	assert(isValid());
	assert(obj->isValid());
	onSubObjectRegistering(obj,0);
	obj->_owner = this;
	_subObjects.insert(obj);
}

void LatObject::_unregisterSubObject(LatObject *obj) {
	assert(obj != nullptr);
	onSubObjectUnregistering(obj,0);
	_subObjects.remove(obj);
	obj->_owner = nullptr;
}

bool LatObject::isReadyForDestruction() const {
	// only destroy this object if all of its sub-objects have already been destroyed
	return _subObjects.size() == 0;
}

void LatObject::onSubObjectRegistering(LatObject *obj,uint32 depth) {
}

void LatObject::onSubObjectUnregistering(LatObject *obj,uint32 depth) {
}

LatObject::LatObject(const ObjectConstructionData& ocd) :
	_uniqueID(ocd.uniqueID),
	_name(ocd.name),
	_owner(ocd.owner) {
	__registerObject(this);

	_isPendingDestruction = false;
}

LatObject::~LatObject() {
	assert(_isPendingDestruction && isReadyForDestruction());
	__unregisterObject(this);
}

uint32 LatObject::getUniqueID() const {
	return _uniqueID;
}

LatObject* LatObject::getOwner() const {
	return _owner;
}

const engine_string& LatObject::getName() const {
	return _name;
}

engine_string LatObject::getFullName() const {
	engine_string fullName = getName();

	LatObject *curOwner = getOwner();
	while(curOwner != nullptr) {
		fullName = curOwner->getName() + STR("/") + fullName;
		curOwner = curOwner->_owner;
	}

	return fullName;
}

void LatObject::setName(const engine_string &name) {
	if(_name != name) {
		assert(_isNameValid(name,this));
		_name = name;
	}
}

bool LatObject::isPendingDestruction() const {
	return _isPendingDestruction;
}

void LatObject::destroy() {
	if(!_isPendingDestruction) {
		_isPendingDestruction = true;
		onInvalidate();

		// sub-objects must be destroyed first, so recursively add them to the destruction queue
		for(auto &subObj : _subObjects) {
			subObj->destroy();
		}

		; {
			__gcQueue.push(this);
		}
	}
}

bool LatObject::isValid() const {
	return !isPendingDestruction();
}

std::vector<LatObject*> LatObject::filterSubObjectsByPredicate(std::function<bool(const LatObject *obj)> &&pred) {
	std::vector<LatObject*> result;
	for(auto &subObject : _subObjects) if(subObject != nullptr) {
		if(subObject->isValid() && pred(subObject)) result.push_back(subObject);
	}
	return std::move(result);
}

LatObject* LatObject::findSubObjectByName(const engine_string &name) const {
	assert(isValid());
	return _impl_findObjectByName(this,name);
}

LatObject* LatObject::findSubObjectByID(uint32 id) const {
	assert(isValid());
	for(auto subObject : _subObjects) {
		if(subObject->isValid() && subObject->getUniqueID() == id) return subObject;
	}
	return nullptr;
}