#include "ConvexHull.h"

#include <vector>
#include <list>
#include <set>
#include <unordered_map>

//////////////////////////////////////////////////////////////////////////
// ConvexHull
ConvexHull::ConvexHull() {
	volumeData.centroid = {0,0,0};
	volumeData.volume = 0;
	aabb.min = aabb.max = {0,0,0};
}

ConvexHull::ConvexHull(const ConvexHull& other) :
	verticies(other.verticies),
	triangles(other.triangles),
	adjacency(other.adjacency),
	aabb(other.aabb),
	volumeData(other.volumeData) {
}

ConvexHull::ConvexHull(ConvexHull&& other) :
	verticies(std::move(other.verticies)),
	triangles(std::move(other.triangles)),
	adjacency(std::move(other.adjacency)),
	aabb(std::move(other.aabb)),
	volumeData(std::move(other.volumeData)) {
}

ConvexHull::~ConvexHull() {
}

void ConvexHull::offset(const Vector3& amount) {
	for(auto &v : verticies) {
		v += amount;
	}
	volumeData.centroid += amount;

	recalculateAABB();
}

void ConvexHull::recalculateAllDerivedData() {
	recalculateAdjacency();
	recalculateVolumeData();
	recalculateAABB();
}

void ConvexHull::recalculateAdjacency() {
	// build adjacency information
	adjacency.swap(decltype(adjacency)());
	adjacency.resize(verticies.size());
	for(auto& triangle : triangles) {
		for(uint8 i = 0; i < 3; i++) {
			adjacency[triangle[i]].push_back(triangle[(i+1)%3]);
			adjacency[triangle[i]].push_back(triangle[(i+2)%3]);
		}
	}

	for(auto& adj : adjacency) {
		std::sort(adj.begin(),adj.end());
		adj.erase(std::unique(adj.begin(),adj.end()),adj.end());
	}
}

void ConvexHull::recalculateVolumeData() {
	// adapted from http://clb.demon.fi/MathGeoLib/nightly/docs/Polyhedron.cpp_code.html#235

	Vector3 arbitraryCenter = {0,0,0};
	for(const auto& vert : verticies) {
		arbitraryCenter += vert;
	}
	arbitraryCenter /= (float)verticies.size();

	volumeData.centroid = {0,0,0};
	volumeData.volume = 0;

	// decompose the polyhedron into tetrahedrons
	// calculate the volume and centroid of each
	// the overall centroid is the weighted average of all tetrahedrons
	for(auto& tri : triangles) {
		const Vector3& a = verticies[tri[0]];
		const Vector3& b = verticies[tri[1]];
		const Vector3& c = verticies[tri[2]];
		const Vector3& d = arbitraryCenter;

		const Vector3 center = (a + b + c + d) * 0.25f;

		const float volume = fabs((a-d) | ((b-d) % (c-d))) / 6.0f;
		volumeData.volume += volume;
		volumeData.centroid += volume * center;
	}

	volumeData.centroid /= volumeData.volume;
}

void ConvexHull::recalculateAABB() {
	const auto numVerticies = verticies.size();
	if(numVerticies > 0) {
		aabb.min = aabb.max = verticies[1];
		for(uint32 i = 1; i < numVerticies; i++) {
			aabb.min = minVec3(aabb.min,verticies[i]);
			aabb.max = maxVec3(aabb.max,verticies[i]);
		}
	} else {
		aabb.min = aabb.max = {0,0,0};
	}
}

ConvexHull ConvexHull::createFromHVCADConvexHull(const VHACD::IVHACD::ConvexHull& hvcdHull) {
	ConvexHull hull;
	hull.verticies.resize(hvcdHull.m_nPoints);
	hull.triangles.resize(hvcdHull.m_nTriangles);

	// vertices
	for(uint32 vert = 0; vert < hvcdHull.m_nPoints; vert++) {
		for(uint8 i = 0; i < 3; i++) {
			hull.verticies[vert][i] = (float)hvcdHull.m_points[vert*3 + i];
		}
	}

	// indices
	for(uint32 face = 0; face < hvcdHull.m_nTriangles; face++) {
		for(uint8 faceVert = 0; faceVert < 3; faceVert++) {
			// NOTE: twiddling the winding direction!
			hull.triangles[face][faceVert] = hvcdHull.m_triangles[face*3 + (2 - faceVert)];
		}
	}

	hull.recalculateAllDerivedData();

	return std::move(hull);
}

//////////////////////////////////////////////////////////////////////////
// MathUtilities
namespace MathUtilities {
ConvexHull quickHull(Vector3* pointCloud,uint32 numPoints) {
	assert(numPoints>=4); //must have at least 4 points or the algorithm will not work

	struct Triangle {
		Vector3 *points[3];
		Vector3 n;
		std::vector<uint32> pointstocheck;

		Triangle(Vector3 *a,Vector3 *b,Vector3 *c) {
			points[0] = a;
			points[1] = b;
			points[2] = c;
			n = ((*b-*a) % (*c-*a)).GetNormal();
		}
	};
	struct Edge {
		Vector3 *points[2];

		Edge(Vector3 *a,Vector3 *b) {
			points[0] = a;
			points[1] = b;
		}
	};

	std::list<Triangle> lst_triangles;
	std::list<Edge> lst_edges;
	std::list<Triangle*> lst_facestocheck;
	//point list, second is true if point is available for use
	std::vector<std::pair<Vector3*,bool>> lst_points;

	auto lam_addEdge = [&](Vector3 *a,Vector3 *b)->void {
		for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
			if(it->points[0]==b && it->points[1]==a) {
				//opposite edge found, remove it and do not add new one
				lst_edges.erase(it);
				return;
			}
		}
		lst_edges.emplace_back(a,b);
	};

	auto lam_processPoints = [&]()->void {
		for(auto it = lst_facestocheck.begin(); it != lst_facestocheck.end(); it++) {
			(*it)->pointstocheck.clear();
		}

		for(unsigned i = 0; i < numPoints; i++) {
			if(!lst_points[i].second) continue;

			bool assigned = false;
			for(auto it = lst_facestocheck.begin(); it != lst_facestocheck.end(); it++) {
				if(((*it)->n | (*lst_points[i].first - *(*it)->points[0])) > 0.0f) {
					assigned = true;
					(*it)->pointstocheck.push_back(i);
					break;
				}
			}

			if(!assigned) lst_points[i].second = false;
		}
	};

	lst_points.reserve(numPoints);
	for(unsigned i = 0; i < numPoints; i++) lst_points.emplace_back(&pointCloud[i],true);

	//build the initial polytope
	; {
		std::set<int> ep;

		//find up to 6 extreme points (no duplicates)
		for(int e = 0; e < 3; e++) {
			for(int c = 0; c < 2; c++) {
				unsigned best = -1;
				for(unsigned i = 0; i < numPoints; i++) {
					if(!lst_points[i].second) continue;

					if(c==0) {
						if(best==-1 || (*lst_points[i].first)[e] < (*lst_points[best].first)[e]) best = i;
					} else {
						if(best==-1 || (*lst_points[i].first)[e] > (*lst_points[best].first)[e]) best = i;
					}
				}
				if(best==-1) continue;

				lst_points[best].second = false;
				ep.insert(best);
			}
		}

		assert(ep.size() >= 3);

		//find the most distant pair of points in ep
		int baseline[2];
		; {
			auto it = ep.begin();
			baseline[0] = *it++;
			baseline[1] = *it++;
			float baseline_dst = (*lst_points[baseline[0]].first-*lst_points[baseline[1]].first).LengthSquared();
			for(auto ita = it; ita != ep.end(); ita++) {
				for(auto itb = std::next(ita); itb != ep.end(); itb++) {
					const float dst = (*lst_points[*itb].first-*lst_points[*ita].first).LengthSquared();
					if(dst > baseline_dst) {
						baseline[0] = *ita;
						baseline[1] = *itb;
						baseline_dst = dst;
					}
				}
			}
		}

		//find the most distant point from the baseline in ep
		int basetip;
		; {
			basetip = 0;
			float basetip_dst = FLT_MIN;
			for(auto it = ep.begin(); it != ep.end(); it++) {
				if(*it == baseline[0] || *it == baseline[1]) continue;
				float dst = pointDistanceFromLine(*lst_points[*it].first,*lst_points[baseline[0]].first,*lst_points[baseline[1]].first);
				if(dst > basetip_dst) {
					basetip = *it;
					basetip_dst = dst;
				}
			}
		}

		//add the unused points from ep back into lst_points
		ep.erase(baseline[0]);
		ep.erase(baseline[1]);
		ep.erase(basetip);
		for(auto it = ep.begin(); it != ep.end(); it++) {
			lst_points[*it].second = true;
		}

		//find the most distant point from the triangle in the pointcloud
		int apex;
		; {
			unsigned best = -1;
			float apex_dst = FLT_MIN;
			for(unsigned i = 0; i < numPoints; i++) {
				if(!lst_points[i].second) continue;
				float dst = fabs(pointDistanceFromPlane(*lst_points[i].first,*lst_points[baseline[0]].first,*lst_points[baseline[1]].first,*lst_points[basetip].first));
				if(dst > apex_dst) {
					best = i;
					apex_dst = dst;
				}
			}
			assert(best!=-1);

			lst_points[best].second = false;
			apex = best;
		}

		//build the tetrahedron with proper winding
		; {
			Vector3 *a,*b,*c,*d;

			a = lst_points[apex].first;
			b = lst_points[baseline[0]].first;
			c = lst_points[basetip].first;
			d = lst_points[baseline[1]].first;

			if(pointDistanceFromPlane(*a,*b,*d,*c) > 0) {
				//apex is in front of the base with current winding, rewind
				std::swap(c,d);
			}

			lst_triangles.emplace_back(a,b,c);
			lst_triangles.emplace_back(a,c,d);
			lst_triangles.emplace_back(a,d,b);
			lst_triangles.emplace_back(b,d,c);
		}
	}

	for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
		lst_facestocheck.emplace_back(&*it);
	}

	lam_processPoints();

	while(true) {
		if(lst_facestocheck.size()==0) break;

		auto cur_triangle = lst_facestocheck.back(); lst_facestocheck.pop_back();

		Vector3 *newpoint = 0;

		; {
			unsigned best = -1;
			float dst_best = FLT_MIN;
			for(unsigned i = 0; i < cur_triangle->pointstocheck.size(); i++) {
				if(!lst_points[cur_triangle->pointstocheck[i]].second) continue;
				float dst = pointDistanceFromPlane(*lst_points[cur_triangle->pointstocheck[i]].first,*cur_triangle->points[0],*cur_triangle->points[1],*cur_triangle->points[2]);
				if(dst > dst_best) {
					best = cur_triangle->pointstocheck[i];
					dst_best = dst;
				}
			}
			if(best != -1) {
				lst_points[best].second = false;
				newpoint = lst_points[best].first;
			}
		}

		if(newpoint == 0) continue;

		for(auto it = lst_triangles.begin(); it != lst_triangles.end();) {
			//can this face be 'seen' by newpoint?
			if((it->n | (*newpoint - *it->points[0])) > 0) {
				lam_addEdge(it->points[0],it->points[1]);
				lam_addEdge(it->points[1],it->points[2]);
				lam_addEdge(it->points[2],it->points[0]);
				lst_facestocheck.remove(&*it);
				it = lst_triangles.erase(it);
				continue;
			}
			it++;
		}

		//create new triangles from the edges in the edge list
		for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
			lst_triangles.emplace_back(newpoint,it->points[0],it->points[1]);
			lst_facestocheck.emplace_back(&lst_triangles.back());
		}

		lst_edges.clear();

		lam_processPoints();
	}

	//build and return the convex hull structure
	ConvexHull ret;

	//map all of the hull's verts to a unique id while adding the triangles to the hull container
	//and generate adjacency information
	std::unordered_map<Vector3*,std::pair<unsigned,std::set<unsigned>>> map_vertinfo;
	unsigned curid = 0;
	for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
		ret.triangles.emplace_back();
		for(unsigned i = 0; i < 3; i++) {
			auto ins = map_vertinfo.insert(std::make_pair(it->points[i],std::make_pair(curid,std::set<unsigned>())));
			ret.triangles.back()[i] = ins.first->second.first;
			if(ins.second) curid++;
		}
		for(int i = 0; i < 3; i++) {
			for(int j = 0; j < 3; j++) {
				if(i==j) continue;
				map_vertinfo[it->points[i]].second.insert(map_vertinfo[it->points[j]].first);
			}
		}
	}

	//copy all of the vert data into the hull while calculating the centroid
	ret.verticies.resize(map_vertinfo.size());
	ret.adjacency.resize(map_vertinfo.size());

	for(auto it = map_vertinfo.begin(); it != map_vertinfo.end(); it++) {
		ret.verticies[it->second.first] = *it->first;
		std::copy(it->second.second.begin(),it->second.second.end(),std::back_inserter(ret.adjacency[it->second.first]));
	}

	ret.recalculateVolumeData();
	ret.recalculateAABB();

	return std::move(ret);
}
}