#include "ThreadSpooler.h"

#include <chrono>
#include <algorithm>

#ifdef _CONCURRENCY_VISUALIZER
#include <cvmarkersobj.h>
#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

Concurrency::diagnostic::marker_series markerSeries;
#endif


void ThreadSpooler::workerThreadLoop(uint32 id) {
#ifdef _CONCURRENCY_VISUALIZER
	markerSeries.write_flag("LatWorker %i",id);
#endif

	while(true) {
		std::unique_lock<decltype(mtx_workQueue)> lck(mtx_workQueue);
		while(!cv_workAvailable.wait_for(lck,
										 std::chrono::milliseconds(50),
										 [this]() { return !shouldWorkerThreadsBeRunning || workQueue.size() > 0; }));
		if(!shouldWorkerThreadsBeRunning) break;

		auto myWorkEntry = std::move(workQueue.front());
		workQueue.pop();

		lck.unlock();

		myWorkEntry.func();
		myWorkEntry.prom_completion.set_value();
	}
}

ThreadSpooler::ThreadSpooler() :
shouldWorkerThreadsBeRunning(false) {

}

ThreadSpooler::~ThreadSpooler() {
	uninitialize();
}

uint32 ThreadSpooler::initialize(uint32 minimumNumberOfThreads /*= 1*/) {
	shouldWorkerThreadsBeRunning = true;

	// std::thread::hardware_concurrency() will return zero if it cannot determine the actual number of cores
	// in this case, fall back to the minimum number provided
	const uint32 processorsAvailable = std::max(minimumNumberOfThreads,std::thread::hardware_concurrency());
	const uint32 threadsToMake = std::max(0,int32(processorsAvailable) - int32(workerThreads.size()));

	for(uint32 i = 0; i < threadsToMake; i++) {
		workerThreads.emplace_back([this,i]() { workerThreadLoop(i); });
	}

	return threadsToMake;
}

void ThreadSpooler::uninitialize() {
	shouldWorkerThreadsBeRunning = false;
	for(auto& worker : workerThreads) {
		worker.join();
	}
}

uint32 ThreadSpooler::getOptiomalNumConcurrentTasks() const {
	return (uint32)std::ceil(workerThreads.size() * 1.0f);
}

std::future<void> ThreadSpooler::enqueueWork(std::function<void(void)>&& workFunction) {
	std::unique_lock<decltype(mtx_workQueue)> lck(mtx_workQueue);
	workQueue.emplace();
	WorkEntry& newEntry = workQueue.back();
	newEntry.func = std::move(workFunction);
	cv_workAvailable.notify_one();
	return newEntry.prom_completion.get_future();
}
