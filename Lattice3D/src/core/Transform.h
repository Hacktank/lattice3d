#pragma once

#include "EngineMath.h"

class Transform {
public:
	Vector3 position;
	Quaternion rotation;
	Vector3 scale;

	Transform();
	Transform(const Transform &other);
	Transform(Matrix &matrix);
	Transform(Transform &&other);

	Transform& operator=(const Transform &other);
	Transform& operator=(Transform &&other);

	Matrix getMatrix() const;
	Matrix getMatrixNoScale() const;
};