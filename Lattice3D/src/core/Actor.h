#pragma once

#include "LatObject.h"
#include "Transform.h"

#include <vector>

class ActorComponent;

class Actor : public LatObject {
	friend class ActorComponent;
	friend class World;

private:
	pool_vector<ActorComponent*> _components;
	ActorComponent* _defaultRoot;

protected:
	virtual void onSubObjectRegistering(LatObject *obj,uint32 depth) override;
	virtual void onSubObjectUnregistering(LatObject *obj,uint32 depth) override;

public:
	ActorComponent *rootComponent;
	class RigidBodyComponent *rigidBody;

	Actor(const ObjectConstructionData& ocd);
	virtual ~Actor();

	World* getWorld();

	void setDefaultRoot();
};
