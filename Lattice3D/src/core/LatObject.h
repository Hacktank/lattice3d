#pragma once

#include <cassert>
#include <typeinfo>
#include <type_traits>
#include <functional>
#include <string>
#include <vector>
#include <queue>
#include <atomic>

#include "pool_vector.h"

#include "EngineTypes.h"

class LatObject;

template<typename T,typename ...Args>
T* constructObject(LatObject *owner,const engine_string &name,Args&&... args);
template<typename T,typename ...Args>
T* constructObjectNameless(LatObject *owner,Args&&... args);

class LatObject {
	friend class ContainerObject;

	template<typename T,typename ...Args>
	friend T* constructObject(LatObject *owner,const engine_string &name,Args&&... args);
	template<typename T,typename ...Args>
	friend T* constructObjectNameless(LatObject *owner,Args&&...args);

public:
	struct ObjectConstructionData {
		template<typename T,typename ...Args>
		friend T* constructObject(LatObject *owner,const engine_string &name,Args&&... args);

	public:
		uint32 uniqueID;
		engine_string name;
		LatObject* owner;

	private:
		ObjectConstructionData(uint32 uniqueID,
							   const engine_string& name,
							   LatObject* owner) :
			uniqueID(uniqueID),
			name(name),
			owner(owner) {
		};
	};

	//////////////////////////////////////////////////////////////////////////
	// STATIC
private:
	static pool_vector<LatObject*> __objectRegistry;
	static std::queue<LatObject*> __gcQueue;
	static std::atomic<uint32> __nextUniqueID;

	static void __registerObject(LatObject *obj);
	static void __unregisterObject(LatObject *obj);

	static bool _isNameValid(const engine_string& name,const LatObject* parent);
	static LatObject* _impl_findObjectByName(const LatObject* root,const engine_string& name);

public:
	static void doGarbageCollection();
	static std::vector<LatObject*> filterObjectsByPredicate(std::function<bool(const LatObject *obj)> &&pred);
	static LatObject* findObjectByName(const engine_string& name);
	static LatObject* findObjectByID(uint32 id);

	static auto getObjectRegistryIteratorBegin() { return static_cast<const decltype(__objectRegistry)*>(&__objectRegistry)->begin(); }
	static auto getObjectRegistryIteratorEnd() { return static_cast<const decltype(__objectRegistry)*>(&__objectRegistry)->end(); }

	//////////////////////////////////////////////////////////////////////////
	// NON - STATIC
private:
	uint32 _uniqueID;
	LatObject* _owner;
	engine_string _name;
	pool_vector<LatObject*> _subObjects;
	const std::type_info* _typeInfo;
	std::atomic<bool> _isPendingDestruction;

	void _deleteThis();

	void _registerSubObject(LatObject *obj);
	void _unregisterSubObject(LatObject *obj);

protected:
	LatObject(const ObjectConstructionData& ocd);
	virtual ~LatObject();

	virtual bool isReadyForDestruction() const;

	// events are fired BEFORE the sub objects are registered and unregistered.
	virtual void onSubObjectRegistering(LatObject *obj,uint32 depth);
	virtual void onSubObjectUnregistering(LatObject *obj,uint32 depth);

	virtual void onInvalidate() {};

public:
	const std::type_info* getTypeInfo() const { return _typeInfo; }

	uint32 getUniqueID() const;
	LatObject* getOwner() const;
	const engine_string& getName() const;
	engine_string getFullName() const;
	void setName(const engine_string &name);

	bool isPendingDestruction() const;
	void destroy();

	bool isValid() const;

	std::vector<LatObject*> filterSubObjectsByPredicate(std::function<bool(const LatObject *obj)> &&pred);
	LatObject* findSubObjectByName(const engine_string &name) const;
	LatObject* findSubObjectByID(uint32 id) const;

	template<typename T>
	T* findFirstSubObjectByType() const;
};

template<typename T>
T* LatObject::findFirstSubObjectByType() const {
	static_assert(std::is_base_of<LatObject,T>::value,"T must derive from LatObject");

	for(auto& subObject : _subObjects) {
		T* asT = dynamic_cast<T*>(subObject);
		if(asT) return asT;
	}

	return nullptr;
}

template<typename T,typename ...Args>
T* constructObject(LatObject* owner,const engine_string& name,Args&&... args) {
	static_assert(std::is_base_of<LatObject,T>::value,"T must derive from LatObject");
	//assert(std::is_base_of<class World,T>::value || owner != nullptr); // only World doesn't require a parent
	assert(LatObject::_isNameValid(name,owner));

	T *newObject = new T({LatObject::__nextUniqueID++,name,owner},std::forward<Args>(args)...);

	newObject->_typeInfo = &typeid(T);

	if(owner != nullptr) {
		owner->_registerSubObject(newObject);
	}

	return newObject;
}

template<typename T,typename ...Args>
T* constructObjectNameless(LatObject* owner,Args&&... args) {
	T *newObject = constructObject<T,Args...>(owner,STR("_") + to_string(LatObject::__nextUniqueID),std::forward<Args>(args)...);

	return newObject;
}
