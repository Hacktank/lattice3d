#include "World.h"
#include "Actor.h"
#include "Engine.h"

#include "physics/integrator/EulerIntegrator.h"
#include "physics/integrator/RK4Integrator.h"
#include "physics/contact_generator/GJKEPAGenerator.h"
#include "physics/contact_resolver/ImpulseContactResolver.h"

#include "physics/collider/ColliderComponent.h"

#include "graphics/camera/CameraComponent.h"
#include "actors/APlayer.h"

#include <algorithm>
#include <functional>

#include <bgfx/framework/debugdraw/debugdraw.h>

World::World(const ObjectConstructionData& ocd) : LatObject(ocd) {
	contacts.reserve(1000);

	_activeCamera = nullptr;
	_activePlayer = nullptr;

	_physicsIntegrator = nullptr;
	_contactGenerator = nullptr;
	_contactResolver = nullptr;

	physicsFrameNumber = 0;

	_physicsTimeToSimulate = engine_duration(0);

	setPhysicsIntegrator<RK4Integrator>();
	setContactGenerator<GJKEPAGenerator>();
	setContactResolver<ImpulseContactResolver>();

	// force un-pausing to reset time counters
	_isPaused = true;
	setPaused(false);
}

World::~World() {
}

void World::onSubObjectRegistering(LatObject *obj,uint32 depth) {
	__super::onSubObjectRegistering(obj,depth);

	// maintain the cache of actors
	Actor *obj_as_Actor = dynamic_cast<Actor*>(obj);
	if(obj_as_Actor != nullptr) {
		_actors.insert(obj_as_Actor);
	}
}

void World::onSubObjectUnregistering(LatObject *obj,uint32 depth) {
	__super::onSubObjectUnregistering(obj,depth);

	// maintain the cache of actors
	Actor *obj_as_Actor = dynamic_cast<Actor*>(obj);
	if(obj_as_Actor != nullptr) {
		_actors.remove(obj_as_Actor);
	}
}

void World::setPaused(bool paused) {
	if(isPaused() && !paused) {
		_tpLastFrameTime = engine_clock::now();
	}
	_isPaused = paused;
}

bool World::isPaused() const {
	return _isPaused;
}

void World::updateAI() {
	// TODO
}

void World::updateActors() {
	engine_time_point tpCurrentTime = engine_clock::now();

	float gameFrameTimeDeltaSeconds = (float)(tpCurrentTime - _tpLastFrameTime).count() * lat_time.timeScale;
	gameFrameTimeDeltaSeconds = (std::min)(gameFrameTimeDeltaSeconds,lat_time.maximumTimeStep);
	gameFrameTimeDelta = engine_duration(gameFrameTimeDeltaSeconds);
	gameTime += gameFrameTimeDelta;
	_tpLastFrameTime = tpCurrentTime;

	_physicsTimeToSimulate += gameFrameTimeDelta;

	Tickable::tickGroups(TickGroup::ENTITY,gameFrameTimeDelta);
}

void World::updateSpacialPartitions() {
	// TODO
}

void World::updatePhysics() {
	// perform enough physics updates to maintain a 1 to 1 ratio between actual time passed and the time passed within
	// the physics engine, in fixed time steps

	physicsFrameTimeDelta = engine_duration(lat_physics.fixedTimeStep * std::min(lat_time.timeScale,1.0f));

	const float timeToStepThisFrame = (float)_physicsTimeToSimulate.count();
	const uint8 numPhysicsUpdatesThisFrame = (uint8)(timeToStepThisFrame / physicsFrameTimeDelta.count());

	// perform
	for(uint8 i = 0; i < numPhysicsUpdatesThisFrame; i++) {
		physicsFrameNumber++;

		// update frame time
		physicsTime += physicsFrameTimeDelta;

		// pre-update tick actors
		Tickable::tickGroups(TickGroup::PRE_PHYSICS,physicsFrameTimeDelta);

		// integrate bodies
		// contact involvement manifolds MUST be valid!
		if(_physicsIntegrator != nullptr) {
			_physicsIntegrator->frame(this);
		}

		// generate new contacts, update persistent ones, and remove those that are invalid
		if(_contactGenerator != nullptr) {
			_contactGenerator->frame(this);
		}

		// remove invalid contacts
		for(auto it = contacts.begin(); it != contacts.end(); it++) {
			if(!it->isValid()) {
				contacts.remove(it);
			}
		}

		// resolve contacts
		if(_contactResolver != nullptr) {
			_contactResolver->frame(this);
		}

		// post-update tick actors
		Tickable::tickGroups(TickGroup::POST_PHYSICS,physicsFrameTimeDelta);
	}

	_physicsTimeToSimulate -= engine_duration(numPhysicsUpdatesThisFrame * physicsFrameTimeDelta);
}

void World::frame() {
	if(!isPaused()) {
		updateAI();
		updateActors();
		updateSpacialPartitions();
		updatePhysics();
	}

	if(lat_physics.debug.debugDrawEnabledGlobal && lat_physics.debug.debugDrawContacts) {
		for(auto& contact : contacts) {
			ddPush();
			; {
				static const uint32 _color_touching = 0xffff0066;
				static const uint32 _color_nottouching = 0xff333399;
				static const uint32 _color_points = 0xffffff00;
				static const float _totalLength_touching = 10;
				static const float _totalLength_nottouching = 5;
				static const float _pointRadius = 1.0f;

				const bool isThisContactTouching = contact.penetration > -0.01f;

				const float totalLength = isThisContactTouching ? _totalLength_touching : _totalLength_nottouching;
				const float lineToConeRatio = 0.9f;
				const float lineLength = totalLength * lineToConeRatio;
				const float lineRadius = lineLength * 0.01f;
				const float coneLength = totalLength * (1.0f-lineToConeRatio);
				const float coneRadius = coneLength * 1.0f;

				const Vector3& lineA = contact.point;
				const Vector3& lineB = lineA + contact.normal * lineLength;
				const Vector3& coneA = lineB;
				const Vector3& coneB = coneA + contact.normal * coneLength;

				ddSetLod(1);

				ddSetState(false,false,true);

				ddSetColor(isThisContactTouching ? _color_touching : _color_nottouching);

				ddDrawCylinder(&lineA,&lineB,lineRadius);
				ddDrawCone(&coneA,&coneB,coneRadius);

				ddDrawGrid(&contact.normal,&lineA,5,0.25f);

				ddSetColor(_color_points);

				// line c connects the contact point on the surface of object a, which the fancy arrow
				// was just drawn from, to the contact point on the surface of object b
				const Vector3& line2A = lineA;
				const Vector3& line2B = contact.point + contact.normal * contact.penetration;

				ddMoveTo(&line2A);
				ddLineTo(&line2B);
			}
			ddPop();
		}
	}
}

const pool_vector<Actor*>& World::getActors() const {
	return _actors;
}

void World::setActiveCamera(CameraComponent* camera) {
	_activeCamera = camera;
}

CameraComponent* World::getActiveCamera() const {
	if(_activeCamera == nullptr) {
		// try to find an existing camera
		auto potentialCameras = LatObject::filterObjectsByPredicate([](const LatObject* obj)->bool {
			return dynamic_cast<const CameraComponent*>(obj) != nullptr;
		});

		if(potentialCameras.size() > 0) return (CameraComponent*)potentialCameras[0];
	}
	return _activeCamera;
}

void World::setActivePlayer(APlayer* player) {
	_activePlayer = player;
}

APlayer* World::getActivePlayer() const {
	if(_activePlayer == nullptr) {
		// try to find an existing player
		auto potentialPlayers = LatObject::filterObjectsByPredicate([](const LatObject* obj)->bool {
			return dynamic_cast<const APlayer*>(obj) != nullptr;
		});

		if(potentialPlayers.size() > 0) return (APlayer*)potentialPlayers[0];
	}
	return _activePlayer;
}

PhysicsIntegrator* World::getPhysicsIntegrator() const {
	return _physicsIntegrator;
}

ContactGenerator* World::getContactGenerator() const {
	return _contactGenerator;
}

ContactResolver* World::getContactResolver() const {
	return _contactResolver;
}