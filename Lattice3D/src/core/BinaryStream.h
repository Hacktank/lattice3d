#pragma once

/*
Coded by Jacob Tyndall (Hacktank) (jacob.n.tyndall@gmail.com)
Use as you wish, give credit where credit is due.
*/

#include <string>
#include <math.h>
#include <cfloat>
#include <type_traits>

#define BSBUFFSIZE 64
#define FC(f) (*((char*)&f))

class BinStream {
protected:
	struct node {
		char *data;
		node *next;
		unsigned int datalen;
		unsigned int bitsused;
	};

	int io_flags;
	char io_sp_mantbits;
	char io_dp_mantbits;

	node *first,*last;

	char *c_data;
	unsigned int c_datalen;
	bool c_changed;

	char *ibfr,*obfr;
	unsigned int icurbit,ocurbit,nodes,numbits;
	node *obufnode,*ibufnode;

public:
	struct ios {
	public:
		static const int beg = 0x0;
		static const int cur = 0x1;
		static const int end = 0x2;

		static const int compression = 0x1;
	};

	class setflags {
	public:
		int newflags;
		setflags(int pnewflags) {
			newflags = pnewflags;
		}
		void apply(BinStream &bs) {
			bs.io_flags = (bs.io_flags & ((~0x0)^newflags)) | newflags;
		}
	};
	friend class setflags;

	class setspprecision {
	public:
		char precision;
		setspprecision(char pprecision) {
			precision = pprecision;
		}
		void apply(BinStream &bs) {
			bs.io_sp_mantbits = precision;
		}
	};
	friend class setspprecision;

	class setdpprecision {
	public:
		char precision;
		setdpprecision(char pprecision) {
			precision = pprecision;
		}
		void apply(BinStream &bs) {
			bs.io_dp_mantbits = precision;
		}
	};
	friend class setdpprecision;

	BinStream(int flags = 0,int sp_mantbits = FLT_MANT_DIG,int dpmantbits = DBL_MANT_DIG) {
		c_data = 0;
		first = 0; // prevent dirty deletion

		io_flags = flags;
		io_sp_mantbits = sp_mantbits;
		io_dp_mantbits = dpmantbits;
		clear();
	};

	~BinStream() {
		clear();
	}

	BinStream& operator=(BinStream &l) {
		io_flags = l.io_flags;
		io_sp_mantbits = l.io_sp_mantbits;
		io_dp_mantbits = l.io_dp_mantbits;
		setData(l.c_str(),l.size());
		numbits = l.numbits;
		seekpb(l.tellpb(),ios::beg);
		seekgb(l.tellgb(),ios::beg);
		return (*this);
	}

	void clear() {
		if(c_data) delete[] c_data;
		while(first) {
			node *tmp = first->next;
			delete[] first->data;
			delete first;
			first = tmp;
		}
		icurbit = 0;
		ocurbit = 0;
		obfr = 0;
		ibfr = 0;
		nodes = 0;
		obufnode = 0;
		ibufnode = 0;
		first = 0;
		last = 0;
		c_data = 0;
		c_datalen = 0;
		numbits = 0;
		c_changed = true;
	}

	void setData(const char *data,unsigned int len) {
		clear();
		addNode(len);
		memcpy(last->data,data,len);
		icurbit = len*8;
		last->bitsused = icurbit;
		numbits = icurbit;
	}

	const char* c_str() {
		compile();
		return c_data;
	}

	unsigned int size() {
		return (int)ceil(numbits/8.0f);
	}

	unsigned int sizeb() {
		return numbits;
	}

	void seekp(unsigned int pos,char dir) {
		seekpb(pos*8,dir);
	}

	void seekg(unsigned int pos,char dir) {
		seekgb(pos*8,dir);
	}

	unsigned int tellp() {
		return tellpb()/8;
	}

	unsigned int tellg() {
		return tellgb()/8;
	}

	void seekpb(int pos,int dir) {
		switch(dir) {
			case ios::beg:
				//
				break;
			case ios::cur:
				pos += tellpb();
				break;
			case ios::end:
				pos = numbits - pos;
				break;
		}
		if(pos > (int)numbits) pos = numbits;
		if(pos < 0) pos = 0;

		int p = 0;
		int clen;
		node *cur = first;
		while(cur) {
			clen = cur->bitsused;
			if(p<=pos && p+clen>pos) {
				ibufnode = cur;
				icurbit = pos-p;
				return;
			}
			p += clen;
			cur = cur->next;
		}

		icurbit = pos;
	}

	void seekgb(int pos,int dir) {
		switch(dir) {
			case ios::beg:
				//
				break;
			case ios::cur:
				pos += tellgb();
				break;
			case ios::end:
				pos = numbits - pos;
				break;
		}
		if(pos > (int)numbits) pos = numbits;
		if(pos < 0) pos = 0;

		int p = 0;
		int clen;
		node *cur = first;
		while(cur) {
			clen = cur->bitsused;
			if(p<=pos && p+clen>pos) {
				obufnode = cur;
				ocurbit = pos-p;
				return;
			}
			p += clen;
			cur = cur->next;
		}
	}

	unsigned int tellpb() {
		unsigned int p = 0;
		unsigned int clen;
		node *cur = first;
		while(cur) {
			clen = cur->datalen*8;
			if(cur == last) clen = icurbit;
			if(cur == ibufnode) return p+clen;
			p += clen;
			cur = cur->next;
		}
		return 0;
	}

	unsigned int tellgb() {
		unsigned int p = 0;
		unsigned int clen;
		node *cur = first;
		while(cur) {
			clen = cur->datalen*8;
			if(cur == obufnode) return p+ocurbit;
			p += clen;
			cur = cur->next;
		}
		return 0;
	}

	int getio_flags() {
		return io_flags;
	}

	void setioflags(int newflags) {
		io_flags = newflags;
	}

	template<class c> BinStream& operator>>(c &n) { read<c>(n); return (*this); };
	template<class c> BinStream& operator<<(const c &n) { write<c>(n); return (*this); };

	template<> BinStream& operator>>(setflags &n) { n.apply(*this); return (*this); };
	template<> BinStream& operator<<(const setflags &n) { ((setflags&)n).apply(*this); return (*this); };
	template<> BinStream& operator>>(setspprecision &n) { n.apply(*this); return (*this); };
	template<> BinStream& operator<<(const setspprecision &n) { ((setspprecision&)n).apply(*this); return (*this); };
	template<> BinStream& operator>>(setdpprecision &n) { n.apply(*this); return (*this); };
	template<> BinStream& operator<<(const setdpprecision &n) { ((setdpprecision&)n).apply(*this); return (*this); };

	char& operator[](unsigned int right) {
		//char error = 0;
		//if(right>=nodes || right<0) return error;
		unsigned int pos = 0;
		unsigned int clen;
		node *cur = first;
		while(cur) {
			clen = cur->datalen;
			if(pos<=right && pos+clen > right) return cur->data[right-pos];
			pos += clen;
			cur = cur->next;
		}
		//return error;
	}

	unsigned char getneededbits(unsigned long int maxvalues) {
		return (unsigned char)ceil(log(abs((long double)maxvalues))/log(2.0));
	}

	//getMostSignificantBit
	template<class c> char getMSB(c var) {
		unsigned int num = sizeof(c)*8;
		for(unsigned int i = 0; i < num; i++) {
			if(((char*)&var)[(num-i-1)/8] & (0x1 << ((num-i-1)%8))) return num-i;
		}
		return 0;
	}

	template<class c> c peek() {
		unsigned int tmp = tellgb();
		c ret = 0;
		read<c>(ret);
		seekgb(tmp,ios::beg);
		return ret;
	}

	template<class c> c peekfs(unsigned long int maxsize) {
		unsigned int tmp = tellgb();
		c ret = 0;
		ret = creadfs<c>(maxsize);
		seekgb(tmp,ios::beg);
		return ret;
	}

	// fs == fixed size

	template<class c> c readfs(unsigned long int maxsize) {
		c n = 0;
		creadfs<c>(n,maxsize);
		return n;
	}

	template<class c> void readfs(c &n,unsigned long int maxsize) {
		memset(&n,0,sizeof(n));
		readbits((char*)&n,getneededbits(maxsize));
	}

	template<class c> void writefs(c n,unsigned long int maxsize) {
		writebits((char*)&n,getneededbits(maxsize));
	}

	template<class c> c read() {
		c n = 0;
		read<c>(n);
		return n;
	}

	template<class c> void read(c &n) {
		if(!(io_flags&ios::compression) || !std::is_fundamental<c>::value) {
			readbits((char*)&n,sizeof(n)*8);
		} else {
			memset((char*)&n,0,sizeof(n));

			char msb = 0;
			char sign = 0;

			readbits((char*)&sign,1);
			readbits((char*)&msb,5);
			readbits((char*)&n,(char)(msb+1));

			if(sign) {
				n = ~n;
			}
		}
	}

	template<> void read(bool &n) {
		if(!(io_flags&ios::compression)) {
			readbits((char*)&n,sizeof(n)*8);
		} else {
			n = false;
			readbits((char*)&n,1);
		}
	}

	template<> void read(char &n) {
		if(!(io_flags&ios::compression)) {
			readbits((char*)&n,sizeof(n)*8);
		} else {
			readbits((char*)&n,8);
		}
	}

	template<> void read(unsigned char &n) {
		if(!(io_flags&ios::compression)) {
			readbits((char*)&n,sizeof(n)*8);
		} else {
			readbits((char*)&n,8);
		}
	}

	template<> void read(float &n) {
		if(!(io_flags&ios::compression)) {
			readbits((char*)&n,sizeof(n)*8);
		} else {
			readbits((char*)&n,9+io_sp_mantbits);
			*((unsigned int*)&n) = (*((unsigned int*)&n)) << (23-io_sp_mantbits);
		}
	}

	template<> void read(double &n) {
		if(!(io_flags&ios::compression)) {
			readbits((char*)&n,sizeof(n)*8);
		} else {
			readbits((char*)&n,12+io_dp_mantbits);
			*((unsigned int*)&n) = (*((unsigned int*)&n)) << (52-io_dp_mantbits);
		}
	}

	template<> void read(char* &n) {
		if(!(io_flags&ios::compression)) {
			int len = read<int>();
			n[len] = 0;
			readbytes(n,len);
		} else {
			int len = read<int>();
			n[len] = 0;
			readbytes(n,len);
		}
	}

	template<> void read(std::string &n) {
		char *buffer = new char[peek<int>()];
		read(buffer);
		n = buffer;
	}

	void readbytes(char *dest,unsigned int len) {
		if(!(io_flags&ios::compression)) {
			for(unsigned int i = 0; i < len; i++) (*this) >> dest[i];
		} else {
			for(unsigned int i = 0; i < len; i++) (*this) >> dest[i];
		}
	}

	template<class c> void write(const c &n) {
		if(!(io_flags&ios::compression) || !std::is_arithmetic<c>::value) {
			writebits((char*)&n,sizeof(n)*8);
		} else {
			c nn = n;
			char msb;

			bool sign = !!(nn & (0x1 << (sizeof(nn)*8-1)));
			if(sign) {
				nn = ~nn;
			}

			msb = getMSB(nn);
			if(msb != 0) msb--;
			writebits((char*)&sign,1);
			writebits((char*)&msb,5);
			writebits((char*)&nn,(char)(msb+1));
		}
	}

	template<> void write(const bool &n) {
		if(!(io_flags&ios::compression)) {
			writebits((char*)&n,sizeof(n)*8);
		} else {
			char rb = n ? 0x1 : 0x0;
			writebits(&rb,1);
		}
	}

	template<> void write(const char &n) {
		if(!(io_flags&ios::compression)) {
			writebits((char*)&n,sizeof(n)*8);
		} else {
			writebits((char*)&n,sizeof(n)*8);
		}
	}

	template<> void write(const unsigned char &n) {
		if(!(io_flags&ios::compression)) {
			writebits((char*)&n,sizeof(n)*8);
		} else {
			writebits((char*)&n,sizeof(n)*8);
		}
	}

	template<> void write(const float &n) {
		if(!(io_flags&ios::compression)) {
			writebits((char*)&n,sizeof(n)*8);
		} else {
			*((unsigned int*)&n) = (*((unsigned int*)&n)) >> (23-io_sp_mantbits);
			writebits((char*)&n,9+io_sp_mantbits);
		}
	}

	template<> void write(const double &n) {
		if(!(io_flags&ios::compression)) {
			writebits((char*)&n,sizeof(n)*8);
		} else {
			*((unsigned int*)&n) = (*((unsigned int*)&n)) >> (52-io_dp_mantbits);
			writebits((char*)&n,12+io_dp_mantbits);
		}
	}

	template<> void write(char* const &n) { //STRING
		if(!(io_flags&ios::compression)) {
			int len = strlen(n);
			write<int>(len);
			writebytes(n,len);
		} else {
			int len = strlen(n);
			write<int>(len);
			writebytes(n,len);
		}
	}

	template<> void write(const std::string &n) {
		write((char*)n.c_str());
	}

	void writebytes(char *data,unsigned int len) {
		if(!(io_flags&ios::compression)) {
			for(unsigned int i = 0; i < len; i++) (*this) << data[i];
		} else {
			for(unsigned int i = 0; i < len; i++) (*this) << data[i];
		}
	}

	void writebits(char *b,int num) {
		c_changed = true;
		if(!ibufnode) addNode(BSBUFFSIZE);
		for(int i = 0; i < num; i++) {
			if(icurbit == ibufnode->datalen*8) {
				ibufnode = ibufnode->next;
				if(!ibufnode) addNode(BSBUFFSIZE);
				icurbit = 0;
			}
			ibfr = ibufnode->data;
			int bi,ii;
			bi = (int)(i/8.0f);
			ii = (int)(icurbit/8.0f);
			ibfr[ii] = (ibfr[ii] & ~(0x1 << (icurbit%8))) | (((b[bi] & (0x1<<(i%8))) ? 0x1 : 0x0) << (icurbit%8));
			if(icurbit == ibufnode->bitsused) {
				numbits++;
				ibufnode->bitsused++;
			}
			icurbit++;
		}
	}

	void readbits(char *b,int num) {
		for(int i = 0; i < num; i++) {
			if(ocurbit == obufnode->datalen*8) {
				obufnode = obufnode->next;
				if(!obufnode) return;
				obfr = obufnode->data;
				ocurbit = 0;
			}
			if(!obfr) return;
			int bi,ii;
			bi = (int)(i/8.0f);
			ii = (int)(ocurbit/8.0f);
			b[bi] = (b[bi] & ~(0x1 << (i%8))) | (((obfr[ii] & (0x1<<(ocurbit%8))) ? 0x1 : 0x0) << (i%8));
			ocurbit++;
		}
	}

protected:
	void addNode(unsigned int datalen) {
		node *newnode = new node;
		if(last) last->next = newnode;
		last = newnode;
		newnode->next = 0;
		newnode->datalen = datalen;
		newnode->data = new char[datalen];
		newnode->bitsused = 0;
		memset(newnode->data,0,datalen);

		if(!first) first = newnode;

		if(!obufnode) {
			obufnode = newnode;
			obfr = newnode->data;
			ocurbit = 0;
		}

		if(!ibufnode) {
			ibufnode = newnode;
			ibfr = newnode->data;
			icurbit = 0;
		}

		nodes++;
	}

	void compile() {
		if(c_changed) {
			c_changed = false;
			unsigned int dlen = 0;
			node *cur = first;
			while(cur) {
				if(cur->bitsused == 0) break;
				dlen += (int)ceil(cur->bitsused/8.0f);
				cur = cur->next;
			}
			if(c_data) delete[] c_data;
			c_data = new char[dlen];
			c_datalen = dlen;
			unsigned int pos = 0;
			cur = first;
			while(cur) {
				if(cur->bitsused == 0) break;
				unsigned int clenbytes = (int)ceil(cur->bitsused/8.0f);
				memcpy(c_data+pos,cur->data,clenbytes);
				pos += clenbytes;
				cur = cur->next;
			}
		}
	}
};
