#pragma once

#include <stdexcept>
#ifdef max // annoying compiler warning
#undef max
#endif
#include "jsoncons/json.hpp"

using jsoncons::json;
using jsoncons::json_exception;
using jsoncons::json_deserializer;
using jsoncons::json_reader;
using jsoncons::pretty_print;
using jsoncons::output_format;