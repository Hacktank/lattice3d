#pragma once

#include "core/EngineTypes.h"

#include "core/EngineResource.h"
#include "core/JSON.h"

#include "core/ConvexHull.h"

class ConvexHullResource : public EngineResource<ConvexHullResource> {
	friend class EngineResource<ConvexHullResource>;

private:
	static constexpr const engine_cstring _autoNameFormat = STR("Unnamed ConvexHull %i");

	ConvexHullResource(const engine_string& name);
	~ConvexHullResource();

public:
	ConvexHull data;

	static std::shared_ptr<ConvexHullResource> createFromData(const engine_string& name,
															  const ConvexHull& hull,
															  NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);
};
