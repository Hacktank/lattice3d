#pragma once

#include "core/Actor.h"

class APlayer : public Actor {
public:
	APlayer(const ObjectConstructionData& ocd);
	virtual ~APlayer();
};
