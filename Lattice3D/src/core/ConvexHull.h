#pragma once

#include "EngineTypes.h"

#include <vector>
#include <array>

#include <VHACD.h>

class ConvexHull {
public:
	std::vector<Vector3> verticies;
	std::vector<std::array<uint32,3>> triangles;

	// derived data
	std::vector<std::vector<uint32>> adjacency;
	struct { Vector3 min,max; } aabb;
	struct {
		Vector3 centroid;
		float volume;
	} volumeData;

	ConvexHull();
	ConvexHull(const ConvexHull& other);
	ConvexHull(ConvexHull&& other);
	~ConvexHull();

	ConvexHull& operator=(const ConvexHull& l) = default;
	ConvexHull& operator=(ConvexHull&& l) = default;

	void offset(const Vector3& amount);

	void recalculateAllDerivedData();
	void recalculateAdjacency();
	void recalculateVolumeData();
	void recalculateAABB();

	static ConvexHull createFromHVCADConvexHull(const VHACD::IVHACD::ConvexHull& hvcdHull);
};

namespace MathUtilities {
ConvexHull quickHull(Vector3* pointCloud,uint32 numPoints);
}
