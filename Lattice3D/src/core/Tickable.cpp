#include "Tickable.h"

#include "LatObject.h"

//////////////////////////////////////////////////////////////////////////
// STATIC
pool_vector<Tickable*> Tickable::__tickableRegistry;
pool_vector<Tickable::DiscreetTickerHandle*> Tickable::__discreetTickerRegistry;

void Tickable::__registerTickableObject(Tickable* object) {
	__tickableRegistry.emplace(object);
}

void Tickable::__unregisterTickableObject(Tickable* object) {
	__tickableRegistry.remove(object);
}

void Tickable::tickGroups(uint32 groups,engine_duration deltaTime) {
	; {
		// as a result of actually calling the tick function, __tickableRegistry can be forced to grow, invalidating iterators
		struct ItemToTickEntry {
			Tickable* tickable;
			uint32 matchingGroups;
		};
		static std::vector<ItemToTickEntry> s_itemsToTickBuffer;
		s_itemsToTickBuffer.clear();

		// fill tickable cache
		for(auto& tickable : __tickableRegistry) {
			if(tickable->tickGroup != TickGroup::NONE) {
				const uint32 matchingGroups = tickable->tickGroup & groups;

				if(matchingGroups != 0) {
					LatObject* tickable_as_Object = dynamic_cast<LatObject*>(tickable);
					if(tickable_as_Object == nullptr || tickable_as_Object->isValid()) s_itemsToTickBuffer.push_back({tickable,matchingGroups});
				}
			}
		}

		// call tickable cache
		for(auto& entry : s_itemsToTickBuffer) {
			entry.tickable->onTick(entry.matchingGroups,deltaTime);
		}
	}

	; {
		for(auto &tickable : __discreetTickerRegistry) {
			if(tickable->tickGroup != TickGroup::NONE) {
				const uint32 matchingGroups = tickable->tickGroup & groups;
				if(matchingGroups == tickable->tickGroup) {
					tickable->func(matchingGroups,deltaTime);
				}
			}
		}
	}
}

Tickable::DiscreetTickerHandle* Tickable::createDiscreetTicker(uint32 tickGroup,DiscreetTickerHandle::CallbackType&& func) {
	auto newTickerIterator = __discreetTickerRegistry.emplace(new DiscreetTickerHandle(tickGroup,std::move(func)));
	return *newTickerIterator;
}

//////////////////////////////////////////////////////////////////////////
// NON - STATIC

void Tickable::DiscreetTickerHandle::unregister() {
	__discreetTickerRegistry.remove(this);

	delete this;
}

Tickable::DiscreetTickerHandle::DiscreetTickerHandle(uint32 tickGroup,CallbackType&& func) :
	tickGroup(tickGroup),
	func(func) {
}

Tickable::DiscreetTickerHandle::~DiscreetTickerHandle() {
}

Tickable::Tickable() {
	tickGroup = TickGroup::NONE;

	__registerTickableObject(this);
}

Tickable::~Tickable() {
	__unregisterTickableObject(this);
}