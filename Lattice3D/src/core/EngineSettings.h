#pragma once

#include "EngineMath.h"

#include "Singleton.h"

#include <vector>
#include <array>
#include <queue>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <functional>
#include <random>

#include "CollisionMatrix.h"
#include "ThreadSpooler.h"
#include "input/Input.h"

#ifdef max
#undef max
#endif
#ifdef min
#undef min
#endif

#include <VHACD.h>

namespace ThreadFlags {
enum _ : uint32 {
	ENTITY_UPDATE = 0,
	COM_GRAPHICS = 1<<1,
	COM_WINDOW = 1<<2,
};
}

#define lat_system (EngineSettings::Instance().system)
#define lat_rand (lat_system.random)
#define lat_physics (EngineSettings::Instance().physics)
#define lat_time (EngineSettings::Instance().time)
#define lat_graphics (EngineSettings::Instance().graphics)
#define lat_resources (EngineSettings::Instance().resources)

class EngineSettings : public singleton::Singleton<EngineSettings> {
public:
	struct {
		friend EngineSettings;

		std::thread::id mainThreadID;
		ThreadSpooler threadSpooler;
		InputManager input;
		class Application* application;
		class World* activeWorld;

		struct {
			VHACD::IVHACD::Parameters par_defaultParameters;
		} mesh_decomposition;

		struct {
			typedef std::mt19937 RNG_t;

			RNG_t rng;

			RNG_t::result_type getInteger() { return rng(); }
			RNG_t::result_type getInteger(RNG_t::result_type min,RNG_t::result_type max) { return (rng() % (max+1-min)) + min; }

			float getFloat() { return float(rng())/float(rng.max()); }
			float getFloat(float min,float max) { return (getFloat()*(min-max))+max; }
		} random;

	public:
		bool isCalledFromMainThread() const { return std::this_thread::get_id() == mainThreadID; }
	} system;

	struct _physics {
		friend EngineSettings;

		float fixedTimeStep;
		CollisionMatrix collisionMatrix;
		float collisionSkinWidth;
		float minimumVelocityForRestitution;
		Vector3 gravity;

		float maximumBodyMass;

		struct {
			float minPenetration;
			float maxPointDistanceFromInitialSq;
		} contact_persistance;

		struct {
			bool automaticSleepingEnabled;
			float epsilon_motion;
			float motionBiasCoef;
			float maximumPhysicsDistanceFromOriginSq;
		} sleep;

		struct {
			float linear;
			float angular;
		} damping;

		struct {
			uint32 maximumVelocityIterationsBase;
			float maximumVelocityIterationsPerContactInCluster;
			uint32 maximumPenetrationIterationsBase;
			float maximumPenetrationIterationsPerContactInCluster;

			float epsilon_penetration;
			float epsilon_velocity;
		} solver;

		struct {
			bool debugDrawEnabledGlobal;
			bool debugDrawEnabledStatic;
			bool debugDrawEnabledDynamic;
			bool debugDrawContacts;
		} debug;
	} physics;

	struct {
		float timeScale;
		float maximumTimeStep;

		engine_time_point tpLastFrameTime;
		engine_duration timeDeltaLastFrame;
	} time;

	struct _graphics {
		struct {
			Matrix mtxView;
			Matrix mtxProjection;
			Matrix mtxModel;
			Vector3 vecEyePosition;
		} runtime;

		bool globalWireframe;
	} graphics;

	struct _resources {
		std::vector<std::shared_ptr<class IEngineResource>> engineResources;

		void registerAsSystemResource(const std::shared_ptr<class IEngineResource>& resource);
		void unregisterAsSystemResource(const std::shared_ptr<class IEngineResource>& resource);
	} resources;

	EngineSettings();
	~EngineSettings();

	void initialize(Application* application,std::thread::id mainThreadID,uint32 randSeed) {
		assert(application != nullptr);

		system.application = application;
		system.mainThreadID = mainThreadID;
		system.random.rng.seed(randSeed);

		system.mesh_decomposition.par_defaultParameters.m_resolution = 1000;

		physics.fixedTimeStep = 1.0f / 50.0f;

		physics.collisionMatrix.setCollisionLayerName(0,STR("default"));
		physics.collisionMatrix.setCollisionLayerName(1,STR("terrain"));
		physics.collisionMatrix.setCollisionLayerName(2,STR("player"));
		physics.collisionMatrix.setCollisionLayerName(3,STR("enemy"));

		physics.minimumVelocityForRestitution = 2.5f;
		physics.collisionSkinWidth = 0.0f;
		//physics.collisionSkinWidth = 0.1f; disabled for now, before re-enabling fix or dismiss possible issues with the collision point being off
		physics.gravity = Vector3::Down * 60.0f;

		physics.maximumBodyMass = 1e6;

		physics.contact_persistance.minPenetration = -8.0f;
		physics.contact_persistance.maxPointDistanceFromInitialSq = SQ(5.5f);

		physics.sleep.automaticSleepingEnabled = true;
		physics.sleep.epsilon_motion = 2.0f;
		physics.sleep.motionBiasCoef = 0.5f;
		physics.sleep.maximumPhysicsDistanceFromOriginSq = SQ(10000);

		physics.damping.linear = 0.985f;
		physics.damping.angular = 0.96f;

		physics.solver.maximumPenetrationIterationsBase = 5;
		physics.solver.maximumPenetrationIterationsPerContactInCluster = 1.5f;
		physics.solver.maximumVelocityIterationsBase = 5;
		physics.solver.maximumVelocityIterationsPerContactInCluster = 1.5f;

		physics.solver.epsilon_penetration = 0.01f;
		physics.solver.epsilon_velocity = 0.025f;

		physics.debug.debugDrawEnabledGlobal = true;
		physics.debug.debugDrawEnabledStatic = false;
		physics.debug.debugDrawEnabledDynamic = true;
		physics.debug.debugDrawContacts = true;

		time.timeScale = 1.0f;
		time.maximumTimeStep = 0.2f;

		graphics.globalWireframe = false;

		_initializeEngineResources();
	}

	void uninitialize();

private:
	bool needsUninitialization;

	void _initializeEngineResources();
};
