#include "ConvexHullResource.h"

ConvexHullResource::ConvexHullResource(const engine_string& name) : EngineResource(name) {

}

ConvexHullResource::~ConvexHullResource() {

}

std::shared_ptr<ConvexHullResource> ConvexHullResource::createFromData(const engine_string& name,
																	   const ConvexHull& hull,
																	   NameCollisionMode nameCollisionMode /*= IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING*/) {
	auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,0);
	if(newResource->areInternalsValid) return newResource;

	newResource->data = hull;

	newResource->areInternalsValid = true;
	return newResource;
}
