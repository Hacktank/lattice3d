#include "EngineSettings.h"
#include "EngineTypes.h"

#include "JSON.h"

#include "../physics/material/PhysicalMaterial.h"

#include "graphics/Vertex.h"
#include "graphics/MeshResource.h"
#include "graphics/ShaderResource.h"

#include "EngineMath.h"

#include <algorithm>

void EngineSettings::_resources::registerAsSystemResource(const std::shared_ptr<class IEngineResource>& resource) {
	engineResources.emplace_back(resource);
}

void EngineSettings::_resources::unregisterAsSystemResource(const std::shared_ptr<class IEngineResource>& resource) {
	auto findIterator = std::find(engineResources.begin(),engineResources.end(),resource);
	if(findIterator != engineResources.end()) {
		engineResources.erase(findIterator);
	}
}

void EngineSettings::uninitialize() {
	assert(needsUninitialization);

	resources.engineResources.empty();

	needsUninitialization = false;
}

void EngineSettings::_initializeEngineResources() {
	assert(system.isCalledFromMainThread());

	// PhysicalMaterial //////////////////////////
	try {
		json materialFile = json::parse_file("data/assets/json/physicalmaterials.json");

		for(uint32 i = 0; i < materialFile.size(); i++) {
			resources.registerAsSystemResource(PhysicalMaterial::createFromJSON(materialFile[i]));
		}
	} catch(const std::exception &e) {
		std::cerr << e.what() << '\n';
	}

	// ShaderResource //////////////////////////

	// MeshResource //////////////////////////

	// cube
	; {
		// code modified from the DirectX ToolKit

		static const float s_cubeSize = 1.0f;
		static const int FaceCount = 6;

		static const Vector3 faceNormals[FaceCount] = {
			{0,  0,  1},
			{0,  0, -1},
			{1,  0,  0},
			{-1, 0,  0},
			{0,  1,  0},
			{0, -1,  0},
		};

		static const Vector2 textureCoordinates[4] = {
			{1, 0},
			{1, 1},
			{0, 1},
			{0, 0},
		};

		std::vector<VertexPosNormalTexCoord> vertices;
		std::vector<uint32> indices;

		float size = s_cubeSize / 2.0f;

		// Create each face in turn.
		for(int i = 0; i < FaceCount; i++) {
			const Vector3& normal = faceNormals[i];

			// Get two vectors perpendicular both to the face normal and to each other.
			const Vector3& basis = (i >= 4) ? Vector3::UnitX : Vector3::UnitY;

			const Vector3 side1 = normal % basis;
			const Vector3 side2 = normal % side1;

			// Six indices (two triangles) per face.
			uint32 vbase = uint32(vertices.size());
			indices.push_back(vbase + 2);
			indices.push_back(vbase + 1);
			indices.push_back(vbase + 0);

			indices.push_back(vbase + 3);
			indices.push_back(vbase + 2);
			indices.push_back(vbase + 0);

			// Four vertices per face.
			vertices.push_back({(normal - side1 - side2) * size,normal,textureCoordinates[0]});
			vertices.push_back({(normal - side1 + side2) * size,normal,textureCoordinates[1]});
			vertices.push_back({(normal + side1 + side2) * size,normal,textureCoordinates[2]});
			vertices.push_back({(normal + side1 - side2) * size,normal,textureCoordinates[3]});
		}

		resources.registerAsSystemResource(
			MeshResource::createFromData(STR("Cube"),
										 VertexPosNormalTexCoord::s_decl,
										 vertices.data(),
										 vertices.size(),
										 indices.data(),
										 indices.size(),
										 false)
		);
	}

	// sphere
	; {
		// code modified from the DirectX ToolKit

		static const ColorRGBA colorAtZero = 0xffffffff;
		static const ColorRGBA colorAtPI = 0xff202020;

		// original algorithm from the DirectX Tool Kit (DirectXTK)
		static constexpr const uint32 tessellation = 20;
		static constexpr const float diameter = 1.0f;

		std::vector<VertexPosNormalTexCoord> vertices;
		std::vector<uint32> indices;

		if(tessellation < 3)
			throw std::out_of_range("tessellation parameter out of range");

		size_t verticalSegments = tessellation;
		size_t horizontalSegments = tessellation * 2;

		float radius = diameter / 2;

		// Create rings of vertices at progressively higher latitudes.
		for(size_t i = 0; i <= verticalSegments; i++) {
			float v = 1 - (float)i / verticalSegments;

			const float vertColorLERPProgress = pow(sin(float(i) * DirectX::XM_PI / verticalSegments),2.5f);
			const ColorRGBA vertColor = colorLERP(colorAtZero,colorAtPI,vertColorLERPProgress);

			float latitude = (i * DirectX::XM_PI / verticalSegments) - DirectX::XM_PIDIV2;
			float dy,dxz;

			DirectX::XMScalarSinCos(&dy,&dxz,latitude);

			// Create a single ring of vertices at this latitude.
			for(size_t j = 0; j <= horizontalSegments; j++) {
				float u = (float)j / horizontalSegments;

				float longitude = j * DirectX::XM_2PI / horizontalSegments;
				float dx,dz;

				DirectX::XMScalarSinCos(&dx,&dz,longitude);

				dx *= dxz;
				dz *= dxz;

				Vector3 normal(dx,dy,dz);
				Vector2 textureCoordinate(u,v);

				vertices.push_back({normal * radius,
								   normal,
								   textureCoordinate});
			}
		}

		// Fill the index buffer with triangles joining each pair of latitude rings.
		size_t stride = horizontalSegments + 1;

		for(size_t i = 0; i < verticalSegments; i++) {
			for(size_t j = 0; j <= horizontalSegments; j++) {
				size_t nextI = i + 1;
				size_t nextJ = (j + 1) % stride;

				indices.push_back(static_cast<uint32>(i * stride + j));
				indices.push_back(static_cast<uint32>(nextI * stride + j));
				indices.push_back(static_cast<uint32>(i * stride + nextJ));

				indices.push_back(static_cast<uint32>(i * stride + nextJ));
				indices.push_back(static_cast<uint32>(nextI * stride + j));
				indices.push_back(static_cast<uint32>(nextI * stride + nextJ));
			}
		}

		resources.registerAsSystemResource(
			MeshResource::createFromData(STR("Sphere"),
										 VertexPosNormalTexCoord::s_decl,
										 vertices.data(),
										 vertices.size(),
										 indices.data(),
										 indices.size(),
										 false)
		);
	}

	// TextureResource //////////////////////////

	// SoundResource //////////////////////////
}

EngineSettings::EngineSettings() {
	needsUninitialization = false;
}

EngineSettings::~EngineSettings() {
	if(needsUninitialization) uninitialize();
}
