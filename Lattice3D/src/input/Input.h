#pragma once

#include <chrono>

#include <array>

#include "core/EngineTypes.h"

#include <windows.h>

namespace entry {
struct MouseState;
}

class MousePoint {
public:// STATIC CONSTNATS
	static MousePoint Zero;

public:
	int32 x,y,z;

	inline MousePoint() : x(0),y(0),z(0) {}
	inline MousePoint(int32 x,int32 y,int32 z = 0) : x(x),y(y),z(z) {}
	inline MousePoint(const MousePoint &point) : x(point.x),y(point.y),z(point.z) {}
	inline MousePoint(const POINT &point) : x(point.x),y(point.y),z(0) {}

	inline operator POINT() const { POINT ret; ret.x = x; ret.y = y; return ret; }
	inline operator Vector2() const { return Vector2(static_cast<float>(x),static_cast<float>(y)); }
	inline operator Vector3() const { return Vector3(static_cast<float>(x),static_cast<float>(y),static_cast<float>(z)); }

	inline bool operator==(const MousePoint &r) const { return x==r.x && y==r.y && z==r.z; }
	inline bool operator!=(const MousePoint &r) const { return !(*this == r); }

	inline MousePoint& operator+=(const MousePoint &r) { x += r.x; y += r.y; z += r.z; return *this; }
	inline MousePoint& operator-=(const MousePoint &r) { x -= r.x; y -= r.y; z -= r.z; return *this; }
	inline MousePoint& operator*=(uint32 r) { x *= r; y *= r; z *= r; return *this; }
	inline MousePoint& operator/=(uint32 r) { x /= r; y /= r; z /= r; return *this; }

	inline MousePoint operator+(const MousePoint &r) const { MousePoint ret(*this); ret += r; return ret; }
	inline MousePoint operator-(const MousePoint &r) const { MousePoint ret(*this); ret -= r; return ret; }
	inline MousePoint operator*(uint32 r) const { MousePoint ret(*this); ret *= r; return ret; }
	inline MousePoint operator/(uint32 r) const { MousePoint ret(*this); ret /= r; return ret; }
};

class InputManager {
private:
	struct _KeyData {
		bool pressedThisFrame;
		bool isPressed;
		bool releasedThisFrame;
		engine_time_point tpLastPressed;
		engine_time_point tpLastReleased;
	};

	std::array<_KeyData,256> keyData;

	struct {
		MousePoint previousPosition;
		MousePoint currentPosition;

		bool isFpsMode;
		MousePoint fpsModePosition;
	} mouse;

	static MousePoint _systemGetMouseClientPosition();
	static MousePoint _systemGetMouseScreenPosition();
	static void _systemSetMouseClientPosition(const MousePoint& clientPosition);
	static void _systemSetMouseScreenPosition(const MousePoint& screenPosition);

public:
	void updateInput(const entry::MouseState* mouseState);

	InputManager();
	~InputManager();

	//////////////////////////////////////////////////////////////////////////
	// GENEREAL
	//////////////////////////////////////////////////////////////////////////

	// FPS mode keeps the mouse cursor within the game window and hides it
	void setMouseFPSMode(bool fpsmode);
	bool getMouseFPSMode() const;

	//////////////////////////////////////////////////////////////////////////
	// KEYS
	//////////////////////////////////////////////////////////////////////////

	// gets whether or not the given key is currently pressed
	bool getKeyIsPressed(uint32 key) const;

	// gets whether or not the given key was pressed during this frame
	bool getKeyDown(uint32 key) const;

	// gets whether or not the given key was held down during this frame, DOES NOT FIRE ON THE FIRST FRAME THAT A KEY IS PRESSED
	bool getKeyHeld(uint32 key) const;

	// gets the duration that the given key has been held
	engine_duration getKeyHeldTime(uint32 key) const;

	// gets whether or not the given key was released during this frame
	bool getKeyUp(uint32 key,engine_duration* outDurationHeld = nullptr) const;

	//////////////////////////////////////////////////////////////////////////
	// MOUSE
	//////////////////////////////////////////////////////////////////////////

	// gets the screen-space coordinates of the mouse cursor
	MousePoint getMousePostion() const;

	// gets the mouse movement over the last frame
	MousePoint getMouseMotion() const;

	// sets the screen-space coordinates of the mouse
	void setMousePosition(const MousePoint &clientPosition);
};
