#include "Input.h"

#include "core/EngineSettings.h"

#include <bgfx/framework/common.h>

MousePoint MousePoint::Zero(0,0);

MousePoint InputManager::_systemGetMouseClientPosition() {
	POINT tmp;
	GetCursorPos(&tmp);
	int32_t x = tmp.x;
	int32_t y = tmp.y;
	entry::mouseScreenToClient({0},&x,&y);
	return MousePoint(x,y);
}

MousePoint InputManager::_systemGetMouseScreenPosition() {
	POINT tmp;
	GetCursorPos(&tmp);
	int32_t x = tmp.x;
	int32_t y = tmp.y;
	return MousePoint(x,y);
}

void InputManager::_systemSetMouseClientPosition(const MousePoint& clientPosition) {
	int32_t x = clientPosition.x;
	int32_t y = clientPosition.y;
	entry::mouseClientToScreen({0},&x,&y);
	SetCursorPos(x,y);
}

void InputManager::_systemSetMouseScreenPosition(const MousePoint& screenPosition) {
	SetCursorPos(screenPosition.x,screenPosition.y);
}

void InputManager::updateInput(const entry::MouseState* mouseState) {
	if(!entry::doesWindowHaveFocus({0})) return;

	mouse.previousPosition = mouse.currentPosition;
	mouse.currentPosition = {mouseState->m_mx, mouseState->m_my, mouseState->m_mz};

	if(mouse.currentPosition != mouse.previousPosition) {
		if(mouse.isFpsMode) {
			//setMousePosition((MousePoint)EngineSettings::Instance().system.application->GetWindowSize() / 2);
			setMousePosition(mouse.fpsModePosition);
		}
	}

	for(uint32 i = 0; i < keyData.size(); i++) {
		auto &key = keyData[i];

		key.pressedThisFrame = key.releasedThisFrame = false;

		const bool isKeyPressed = (GetAsyncKeyState(i)&0x8000) != 0;
		if(isKeyPressed) {
			if(!key.isPressed) {
				key.tpLastPressed = engine_clock::now();
				key.pressedThisFrame = true;
			}
		} else {
			if(key.isPressed) {
				key.tpLastReleased = engine_clock::now();
				key.releasedThisFrame = true;
			}
		}
		key.isPressed = isKeyPressed;
	}
}

InputManager::InputManager() {
	mouse.isFpsMode = false;

	// init key data
	for(auto &key : keyData) {
		key.pressedThisFrame = key.isPressed = key.releasedThisFrame = false;
	}
}

InputManager::~InputManager() {
}

bool InputManager::getMouseFPSMode() const {
	return mouse.isFpsMode;
}

void InputManager::setMouseFPSMode(bool fpsmode) {
	if(mouse.isFpsMode != fpsmode) {
		if(fpsmode) {
			entry::setMouseHide({0},true);
			mouse.fpsModePosition = getMousePostion();
			//setMousePosition((MousePoint)EngineSettings::Instance().system.application->GetWindowSize() / 2);
		} else {
			entry::setMouseHide({0},false);
			setMousePosition(mouse.fpsModePosition);
		}
	}
	mouse.isFpsMode = fpsmode;
}

bool InputManager::getKeyIsPressed(uint32 key) const {
	assert(key >= 0 && key < keyData.size());
	return keyData[key].isPressed;
}

bool InputManager::getKeyDown(uint32 key) const {
	assert(key >= 0 && key < keyData.size());
	return keyData[key].pressedThisFrame;
}

bool InputManager::getKeyHeld(uint32 key) const {
	assert(key >= 0 && key < keyData.size());
	return !keyData[key].pressedThisFrame && keyData[key].isPressed;
}

engine_duration InputManager::getKeyHeldTime(uint32 key) const {
	assert(key >= 0 && key < keyData.size());
	if(!keyData[key].isPressed) return keyData[key].tpLastReleased - keyData[key].tpLastPressed;
	return engine_clock::now() - keyData[key].tpLastPressed;
}

bool InputManager::getKeyUp(uint32 key,engine_duration* outDurationHeld) const {
	assert(key >= 0 && key < keyData.size());
	if(outDurationHeld) *outDurationHeld = getKeyHeldTime(key);
	return keyData[key].releasedThisFrame;
}

MousePoint InputManager::getMousePostion() const {
	return mouse.currentPosition;
}

MousePoint InputManager::getMouseMotion() const {
	return mouse.currentPosition - mouse.previousPosition;
}

void InputManager::setMousePosition(const MousePoint &clientPosition) {
	const MousePoint previousMotion = getMouseMotion();

	_systemSetMouseClientPosition(clientPosition);

	mouse.currentPosition = _systemGetMouseClientPosition();
	mouse.previousPosition = mouse.currentPosition - previousMotion;
}