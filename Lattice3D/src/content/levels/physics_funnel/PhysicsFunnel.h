#pragma once

#include "core/EngineTypes.h"

#include "core/World.h"
#include "core/Tickable.h"
#include "core/ContainerObject.h"

#include "graphics/Color.h"

class ConvexHull;

class WPhysicsFunnel : public World {
public:
	Actor* floorActor;
	ContainerObject* fodderContainer;
	ContainerObject* environmentContainer;

	WPhysicsFunnel(const ObjectConstructionData& ocd);
	virtual ~WPhysicsFunnel();

	virtual void frame() override;

	void uniformScaleFitWithinByAABB(Actor* actor,float size);

	Actor* makePhysGfxMesh(ContainerObject* container,float density,const engine_string& fileName,const Vector3& pos,const Quaternion& ori,std::function<ColorRGBA(const ConvexHull*)> getSubmeshColor);
	Actor* makePhysGfxConvexHull(ContainerObject* container,float density,const ConvexHull& hull,const Vector3& pos,const Quaternion& ori,ColorRGBA color);
	Actor* makePhysGfxBox(ContainerObject* container,float density,const Vector3& halfSize,const Vector3& pos,const Quaternion& ori,ColorRGBA color);
	Actor* makePhysGfxEllipsoid(ContainerObject* container,float density,const Vector3& halfSize,const Vector3& pos,const Quaternion& ori,ColorRGBA color);

	void putAboveFunnel(Actor* actor,const Vector3& posoffset);
	Actor* generateFodderDecomposition(const engine_string& fileName);
	Actor* generateFodderConvexHull();
	Actor* generateFodderBox();
	Actor* generateFodderEllipsoid(bool forceSphere);
	Actor* generateFodderRandom();

	void destroyAllFodder();

	void makeFunnel(ContainerObject* container,float height,float thickness,float radius_bottom,float radius_top,int resolution,const Vector3 &pos,const Quaternion &ori);
};
