#include "PhysicsFunnel.h"

#include "content/common/SleepWakeColorActuatorComponent.h"
#include "content/common/ScaleOscillatorComponent.h"

#include "core/Engine.h"
#include "core/ConvexHull.h"
#include "core/Utility.h"

#include "physics/Physics.h"

#include "graphics/Color.h"
#include "graphics/Vertex.h"
#include "graphics/MeshResource.h"
#include "graphics/shaders/simplecolor/MatInst_SimpleColor.h"
#include "graphics/shaders/simplelighting/MatInst_SimpleLighting.h"

#include "graphics/camera/MouseLookCameraComponent.h"
#include "graphics/MeshComponent.h"

#include "core/ConvexHullResource.h"

#include <bgfx/framework/imgui/imgui.h>
#include "system/Application.h"

#include <bgfx/framework/debugdraw/debugdraw.h>

//////////////////////////////////////////////////////////////////////////
// CONSTANTS
const float _FUNNEL_HEIGHT = 160.0f;
const float _FUNNEL_THICKNESS = 2.0f;
const float _FUNNEL_RAD_BOTTOM = 40.0f;
const float _FUNNEL_RAD_TOP = 160.0f;
const int _FUNNEL_RESOLUTION = 15;
const Vector3 _FUNNEL_POSITION(0,-50,0);

const float _FLOOR_THICKNESS = 10.0f;
const Vector3 _FLOOR_POSITION = _FUNNEL_POSITION + (Vector3(0,-1,0) * ((_FUNNEL_HEIGHT + _FLOOR_THICKNESS) * 0.5f + 150.0f));

const float _FODDER_DENSITY = 0.005f;
const int _FODDER_NUMPERWAVE = 20;

const float _BULLET_DENSITY = _FODDER_DENSITY * 3;

const Vector3 _LIGHT_POSITION = _FUNNEL_POSITION + (Vector3(0,1,0) * (_FUNNEL_HEIGHT * 0.5f + 250.0f));
const bool _LIGHT_IS_DIRECTIONAL = false;

#define FUNNEL_MATERIAL_CONSTANTS \
	Vector4(_LIGHT_POSITION.x,_LIGHT_POSITION.y,_LIGHT_POSITION.z,_LIGHT_IS_DIRECTIONAL? 0:1),/*light position*/\
	0.4f,/*light ambient coef*/\
	0.0000015f,/*light attenuation coef*/\
	0xffffffff/*light color*/

namespace LoadedMeshes {
enum {
	Chair,
	Cow,
	Screwdriver
};
}
static const engine_string MeshesToLoad[] = {
	STR("data/assets/meshes/chair.off"),
	STR("data/assets/meshes/cow.off"),
	STR("data/assets/meshes/screwdriver.off"),
};

namespace BulletType {
enum {
	Sphere,
	Ellipsoid,
	Box,
	ConvexHull,
	ChairMesh,
	CowMesh,
	ScrewdriverMesh
};
}
struct {
	struct {
		int scrollArea = 0;
		bool pause = false;
		bool bulletWireframe = false;
		uint32 bulletType = BulletType::ConvexHull;
		bool bulletScaleOscillating = false;
	} settings;
} guiData;

WPhysicsFunnel::WPhysicsFunnel(const ObjectConstructionData& ocd) : World(ocd) {
	//////////////////////////////////////////////////////////////////////////
	// CAMERA ENTITY
	; {
		auto cameraEntity = constructObjectNameless<Actor>(this);
		auto cameraComponent = constructObjectNameless<MouseLookCameraComponent>(cameraEntity);
		// set movement speed, buttons, and vertical range
		cameraComponent->initialize(0.0025f, // x sensitivity
									0.0025f, // y sensitivity
									90.0f, // vertical range
									75.0f, // movement speed
									'W', // key forward
									'A', // key right
									'S', // key back
									'D', // key left
									VK_SPACE, // key up
									VK_MENU, // key down
									VK_CONTROL, // key slow
									VK_SHIFT, // key fast
									VK_RBUTTON); // key mouse look
		cameraEntity->rootComponent = cameraComponent;
		cameraEntity->rootComponent->transform.position = Vector3(0.0f,250.0f,-300.0f);
		cameraComponent->setLookAt(Vector3(0.0f,0.0f,0.0f));

		this->setActiveCamera(cameraComponent);

		//cameraComponent->setInputEnabled(false);
	}

	lat_system.mesh_decomposition.par_defaultParameters.m_resolution *= 100;

	for(auto& fileName : MeshesToLoad) {
		lat_resources.registerAsSystemResource(MeshResource::createFromFile(fileName,
																			VertexPosNormalTexCoord::s_decl,
																			fileName,
																			VertexPosNormalTexCoord::initMeshDefault,
																			MeshResource::flg_compute_compute_convex_decomposition,
																			IEngineResource::NameCollisionMode::ERNCM_REPLACE_IF_FLAGS_DIFFERENT));
	}

	environmentContainer = constructObject<ContainerObject>(this,"Funnel");
	fodderContainer = constructObject<ContainerObject>(this,"Fodder");

	makeFunnel(environmentContainer,_FUNNEL_HEIGHT,_FUNNEL_THICKNESS,_FUNNEL_RAD_BOTTOM,_FUNNEL_RAD_TOP,_FUNNEL_RESOLUTION,_FUNNEL_POSITION,Quaternion::Identity);
	floorActor = makePhysGfxBox(environmentContainer,0,{1000,_FLOOR_THICKNESS,1000},_FLOOR_POSITION,Quaternion::Identity,0xff50aa50);

	// WIP pick-up-sticks test
	/*; {
		Vector3 pileBasePosition = _FLOOR_POSITION + Vector3::Up * 10.0f;
		const float pileGirth = 10.0f;
		const float pileLength = 100.0f;
		const float pileInnerSize = pileLength / 2.0f;
		const uint8 pilePieces = 3;

		for(uint8 i = 0; i < pilePieces; i++) {
			Quaternion rotation = Quaternion::CreateFromAxisAngle(Vector3::Up,float(M_PI)/4.0f * float(i));
			Vector3 position = Vector3::Transform(Vector3(pileInnerSize/2.0f,0,0),rotation) + pileBasePosition;

			makePhysGfxBox(fodderContainer,1,{pileGirth/2.0f,pileGirth/2.0f,pileLength/2.0f},position,rotation,0xff0000ff);

			if(i>0 && i%2==0) pileBasePosition.y += pileGirth;
		}
	}*/
}

WPhysicsFunnel::~WPhysicsFunnel() {
}

void WPhysicsFunnel::frame() {
	__super::frame();

	//lat_graphics.debugRenderer->addLine({0,0,0},{100,0,0},0xff0000ff,0xff0000ff);
	//lat_graphics.debugRenderer->addAABB({0,0,0},{100,0,0},0xff00ff00);
	ddPush();
	; {
		ddDrawAxis(0.0f,0.0f,0.0f,20.0f);
	}
	ddPop();

	auto lam_spawnFodderWave_spheres = [&]() {
		Vector3 posoff(0,0,0);
		for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
			putAboveFunnel(generateFodderEllipsoid(true),posoff);
			posoff.y += 5;
		}
	};

	auto lam_spawnFodderWave_ellipsoids = [&]() {
		Vector3 posoff(0,0,0);
		for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
			putAboveFunnel(generateFodderEllipsoid(false),posoff);
			posoff.y += 5;
		}
	};

	auto lam_spawnFodderWave_boxes = [&]() {
		Vector3 posoff(0,0,0);
		for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
			putAboveFunnel(generateFodderBox(),posoff);
			posoff.y += 5;
		}
	};

	auto lam_spawnFodderWave_convexHulls = [&]() {
		Vector3 posoff(0,0,0);
		for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
			putAboveFunnel(generateFodderConvexHull(),posoff);
			posoff.y += 5;
		}
	};

	auto lam_spawnFodderWave_random = [&]() {
		Vector3 posoff(0,0,0);
		for(int i = 0; i < _FODDER_NUMPERWAVE; i++) {
			putAboveFunnel(generateFodderRandom(),posoff);
			posoff.y += 5;
		}
	};

	auto lam_spawnChair = [&]() {
		auto actor = generateFodderDecomposition(MeshesToLoad[LoadedMeshes::Chair]);
		putAboveFunnel(actor,{0,0,0});
	};

	auto lam_spawnCow = [&]() {
		auto actor = generateFodderDecomposition(MeshesToLoad[LoadedMeshes::Cow]);
		putAboveFunnel(actor,{0,0,0});
	};

	auto lam_spawnScrewdriver = [&]() {
		auto actor = generateFodderDecomposition(MeshesToLoad[LoadedMeshes::Screwdriver]);
		putAboveFunnel(actor,{0,0,0});
	};

	// gui
	; {
		struct {
			const uint32& width = lat_system.application->windowWidth;
			const uint32& height = lat_system.application->windowHeight;
		} const window;

		const MousePoint mousePoint = lat_system.input.getMousePostion();
		const MousePoint mouseMotion = lat_system.input.getMouseMotion();
		imguiBeginFrame(mousePoint.x,
						mousePoint.y,
						lat_system.input.getKeyIsPressed(VK_LBUTTON) ? IMGUI_MBUT_LEFT : 0 |
						lat_system.input.getKeyIsPressed(VK_RBUTTON) ? IMGUI_MBUT_RIGHT : 0 |
						lat_system.input.getKeyIsPressed(VK_MBUTTON) ? IMGUI_MBUT_MIDDLE : 0,
						mouseMotion.z,
						lat_system.application->windowWidth,
						lat_system.application->windowHeight);

		// SETTINGS
		; {
			imguiBeginScrollArea("Settings",
								 window.width - window.width/5 - 10,
								 10,
								 int(window.width / 4.5f),
								 int(window.height / 1.1f),
								 &guiData.settings.scrollArea);

			imguiBool("Global Wireframe",lat_graphics.globalWireframe);

			imguiSeparatorLine(1); imguiIndent(8); imguiBool("Draw Physics Debug",lat_physics.debug.debugDrawEnabledGlobal); imguiUnindent(8); imguiSeparatorLine(1);
			; {
				imguiIndent();
				imguiBool("Bounds: Static Objects",lat_physics.debug.debugDrawEnabledStatic,lat_physics.debug.debugDrawEnabledGlobal);
				imguiBool("Bounds: Dynamic Objects",lat_physics.debug.debugDrawEnabledDynamic,lat_physics.debug.debugDrawEnabledGlobal);
				imguiBool("Contacts",lat_physics.debug.debugDrawContacts,lat_physics.debug.debugDrawEnabledGlobal);
				imguiUnindent();
			}

			guiData.settings.pause = isPaused(); imguiBool("Paused",guiData.settings.pause); setPaused(guiData.settings.pause);
			imguiSlider("Time Scale",lat_time.timeScale,0.0f,2.0f,0.001f,!isPaused());
			//imguiSlider("Collision Skin Width",lat_physics.collisionSkinWidth,0.0f,50.0f,0.001f);

			imguiSeparatorLine(1); imguiIndent(8); imguiLabel("Bullet (fire with 'R'):"); imguiUnindent(8); imguiSeparatorLine(1);
			; {
				imguiIndent();

				imguiBool("Wireframe",guiData.settings.bulletWireframe);

				imguiIndent(8); imguiLabel("Type:"); imguiUnindent(8);

				; {
					imguiIndent();
					guiData.settings.bulletType = imguiChoose(guiData.settings.bulletType,
															  "Sphere",
															  "Ellipsoid",
															  "Box",
															  "ConvexHull",
															  "Chair Mesh",
															  "Cow Mesh",
															  "Screwdriver Mesh");
					imguiUnindent();
				}

				imguiBool("Scale Oscillating",guiData.settings.bulletScaleOscillating);

				imguiUnindent();
			}

			imguiSeparatorLine(1); imguiIndent(8); imguiLabel("Funnel Fodder:"); imguiUnindent(8); imguiSeparatorLine(1);
			; {
				imguiIndent();
				if(imguiButton("Clear All Fodder")) destroyAllFodder();
				if(imguiButton("Spawn Spheres")) lam_spawnFodderWave_spheres();
				if(imguiButton("Spawn Ellipsoids")) lam_spawnFodderWave_ellipsoids();
				if(imguiButton("Spawn Boxes")) lam_spawnFodderWave_boxes();
				if(imguiButton("Spawn Convex Hulls")) lam_spawnFodderWave_convexHulls();
				if(imguiButton("Spawn Random Assortment")) lam_spawnFodderWave_random();
				if(imguiButton("Spawn Chair Mesh")) lam_spawnChair();
				if(imguiButton("Spawn Cow Mesh")) lam_spawnCow();
				if(imguiButton("Spawn Screwdriver Mesh")) lam_spawnScrewdriver();
				imguiUnindent();
			}

			imguiEndScrollArea();
		}

		imguiEndFrame();
	}

	if(lat_system.input.getKeyUp('2')) lam_spawnFodderWave_spheres();
	if(lat_system.input.getKeyUp('3')) lam_spawnFodderWave_ellipsoids();
	if(lat_system.input.getKeyUp('4')) lam_spawnFodderWave_boxes();
	if(lat_system.input.getKeyUp('5')) lam_spawnFodderWave_convexHulls();
	if(lat_system.input.getKeyUp('6')) lam_spawnFodderWave_random();
	if(lat_system.input.getKeyIsPressed('9')) destroyAllFodder();

	if(lat_system.input.getKeyUp('R')) { // fire a bullet
		Actor* bullet = nullptr;

		switch(guiData.settings.bulletType) {
			case BulletType::Sphere:
				bullet = generateFodderEllipsoid(true);
				break;
			case BulletType::Ellipsoid:
				bullet = generateFodderEllipsoid(false);
				break;
			case BulletType::Box:
				bullet = generateFodderBox();
				break;
			case BulletType::ConvexHull:
				bullet = generateFodderConvexHull();
				break;
			case BulletType::ChairMesh:
				bullet = generateFodderDecomposition(MeshesToLoad[LoadedMeshes::Chair]);
				break;
			case BulletType::CowMesh:
				bullet = generateFodderDecomposition(MeshesToLoad[LoadedMeshes::Cow]);
				break;
			case BulletType::ScrewdriverMesh:
				bullet = generateFodderDecomposition(MeshesToLoad[LoadedMeshes::Screwdriver]);
				break;
			default:
				assert(false);
		}

		bullet->rigidBody->density = _BULLET_DENSITY;

		for(auto& bulletMeshComponent : bullet->filterSubObjectsByPredicate([](const LatObject* obj)->bool { return dynamic_cast<const MeshComponent*>(obj) != nullptr; })) {
			reinterpret_cast<MeshComponent*>(bulletMeshComponent)->wireframe = guiData.settings.bulletWireframe;
		}

		// use only if a predictable sphere projectile is needed
		//CVEllipsoid* collisionVolume = (CVEllipsoid*)bullet->rigidBody->getCollider()->getChildren()[0];
		//collisionVolume->radii = {5,5,5};
		//bullet->findFirstSubObjectByType<MeshComponent>()->transform.scale = {5,5,5};
		//collisionVolume->useMassOverride = true;
		//collisionVolume->massOverride = 1;

		bullet->rootComponent->transform.position = getActiveCamera()->transform.position;
		const Matrix cameraMatrix = getActiveCamera()->getComponentToWorld();

		bullet->rootComponent->transform.rotation = Quaternion::CreateFromRotationMatrix(cameraMatrix);
		bullet->rootComponent->transform.rotation = quatAddAngularOffset(bullet->rootComponent->transform.rotation,{0,M_PI,M_PI});

		if(guiData.settings.bulletScaleOscillating) {
			const Vector3 thetaOffset = {
				lat_rand.getFloat(-float(M_PI),float(M_PI)),
				lat_rand.getFloat(-float(M_PI),float(M_PI)),
				lat_rand.getFloat(-float(M_PI),float(M_PI))
			};
			const Vector3 thetaCoef = {
				lat_rand.getFloat(0.25f,5.0f),
				lat_rand.getFloat(0.25f,5.0f),
				lat_rand.getFloat(0.25f,5.0f)
			};
			constructObjectNameless<ScaleOscillatorComponent>(bullet,
															  thetaOffset,
															  thetaCoef,
															  Vector3(0.5f,0.5f,0.5f),  // scale change min
															  Vector3(1.5f,1.5f,1.5f)); // scale change max
		}

		bullet->rigidBody->velocity = 200.0f * cameraMatrix.Forward();
		bullet->rigidBody->angularVelocity = {
			lat_rand.getFloat(-(float)M_PI/2,(float)M_PI/2),
			0,
			0
		};
	}

	if(lat_system.input.getKeyUp('P')) { // toggle pause
		setPaused(!isPaused());
	}

	//////////////////////////////////////////////////////////////////////////
	// Change Timescale
	if(lat_system.input.getKeyIsPressed('Z')) {
		lat_time.timeScale = std::max(0.0f,lat_time.timeScale - 0.25f * (float)lat_time.timeDeltaLastFrame.count());
	}
	if(lat_system.input.getKeyIsPressed('X')) {
		lat_time.timeScale = std::max(0.0f,lat_time.timeScale + 0.25f * (float)lat_time.timeDeltaLastFrame.count());
	}

	//////////////////////////////////////////////////////////////////////////
	// Modify floor transform
	if(lat_system.input.getKeyIsPressed('C')) {
		auto& var = floorActor->rootComponent->transform.scale.y;
		var = std::max(0.25f,var - 0.5f * (float)lat_time.timeDeltaLastFrame.count());
	}
	if(lat_system.input.getKeyIsPressed('V')) {
		auto& var = floorActor->rootComponent->transform.scale.y;
		var = std::max(0.25f,var + 0.5f * (float)lat_time.timeDeltaLastFrame.count());
	}
	; {// rotation
		static const float _ROTATION_SPEED = float(M_PI)*2.0f / 6.0f;

		struct RotationInputData {
			uint8 key;
			Vector3 rotationAxis;
		};

		static const RotationInputData rotationInputData[] = {
			{VK_NUMPAD4,Vector3::UnitZ},
			{VK_NUMPAD6,-Vector3::UnitZ},
			{VK_NUMPAD8,Vector3::UnitX},
			{VK_NUMPAD2,-Vector3::UnitX},
			{VK_NUMPAD7,Vector3::UnitY},
			{VK_NUMPAD9,-Vector3::UnitY},
		};

		for(uint8 i = 0; i < 6; i++) {
			if(lat_system.input.getKeyIsPressed(rotationInputData[i].key)) {
				auto& var = floorActor->rootComponent->transform.rotation;
				var = var * Quaternion::CreateFromAxisAngle(rotationInputData[i].rotationAxis,_ROTATION_SPEED * (float)lat_time.timeDeltaLastFrame.count());
			}
		}

		if(lat_system.input.getKeyIsPressed(VK_NUMPAD5)) {// RESET TO IDENTITY
			auto& var = floorActor->rootComponent->transform.rotation;
			var = Quaternion::Identity;
		}
	}
}

void WPhysicsFunnel::uniformScaleFitWithinByAABB(Actor* actor,float size) {
	auto collider = actor->findFirstSubObjectByType<ColliderComponent>();
	if(collider) {
		// the AABB must be valid
		collider->recalculateDerivedData();

		const Vector3 aabbSize = collider->aabb_world.max - collider->aabb_world.min;

		if(aabbSize.x == 0 || aabbSize.y == 0 || aabbSize.z == 0) return;

		float bestScale = FLT_MAX;
		for(uint8 i = 0; i < 3; i++) {
			if(aabbSize[i] == 0) continue;

			const float cur = size/aabbSize[i];
			bestScale = std::min(bestScale,cur);
		}

		if(bestScale < FLT_MAX) {
			actor->rootComponent->transform.scale *= bestScale;
		}
	}
}

Actor* WPhysicsFunnel::makePhysGfxMesh(ContainerObject* container,float density,const engine_string& fileName,const Vector3& pos,const Quaternion& ori,std::function<ColorRGBA(const ConvexHull*)> getSubmeshColor) {
	auto actor = constructObjectNameless<Actor>(container);

	// load the specified mesh and ensure that the convex hull decomposition algorithm is run on it
	auto meshResource = MeshResource::createFromFile(fileName,
													 VertexPosNormalTexCoord::s_decl,
													 fileName,
													 VertexPosNormalTexCoord::initMeshDefault,
													 MeshResource::flg_compute_compute_convex_decomposition,
													 IEngineResource::NameCollisionMode::ERNCM_REPLACE_IF_FLAGS_DIFFERENT);

	if(!meshResource) {
		actor->destroy();
		return nullptr;
	}

	auto colliderComponent = constructObjectNameless<ColliderComponent>(actor);

	auto rigidBodyComponent = constructObjectNameless<RigidBodyComponent>(actor,density);
	rigidBodyComponent->setCollider(colliderComponent);

	// recursively build the collision volume tree
	std::function<void(CollisionVolumeNode*,MeshResource::VolumeTreeNode*)> lam_buildVolumeTree = [&](CollisionVolumeNode* cvRoot,MeshResource::VolumeTreeNode* meshVolumeRoot) {
		auto cvChild = cvRoot->createChild<CVConvexHullResource>(meshVolumeRoot->hull);

		// only want to render leaf volumes
		if(meshVolumeRoot->children.size() == 0) {
			auto subMeshResource = MeshResource::createFromConvexHull(STR(""),
																	  VertexPosNormalTexCoord::s_decl,
																	  meshVolumeRoot->hull->data,
																	  true,
																	  VertexPosNormalTexCoord::initMeshDefault);
			auto subMaterialInstance = MatInst_SimpleLighting::createInstance(colorRandomRGBA(0xff),FUNNEL_MATERIAL_CONSTANTS);
			auto subMeshComponent = constructObjectNameless<MeshComponent>(actor,subMeshResource,subMaterialInstance);

			const ColorRGBA color = getSubmeshColor(&meshVolumeRoot->hull->data);
			auto renderColorActuator = constructObjectNameless<SleepWakeColorActuatorComponent>(actor,
																								rigidBodyComponent,
																								&subMaterialInstance->uniforms.m_colorMult,
																								color,
																								colorScale(color,0.25f));
		} else {
			for(auto& meshVolumeChild : meshVolumeRoot->children) {
				lam_buildVolumeTree(cvChild,&meshVolumeChild);
			}
		}
	};

	lam_buildVolumeTree(colliderComponent,&meshResource->collisionVolumeTree);

	actor->rootComponent = rigidBodyComponent;

	actor->rootComponent->transform.position = pos;
	actor->rootComponent->transform.rotation = ori;

	return actor;
}

Actor* WPhysicsFunnel::makePhysGfxConvexHull(ContainerObject* container,float density,const ConvexHull& hull,const Vector3& pos,const Quaternion& ori,ColorRGBA color) {
	auto actor = constructObjectNameless<Actor>(container);

	auto colliderComponent = constructObjectNameless<ColliderComponent>(actor);
	colliderComponent->createChild<CVConvexHull>(hull);

	auto rigidBodyComponent = constructObjectNameless<RigidBodyComponent>(actor,density);
	rigidBodyComponent->setCollider(colliderComponent);

	auto materialInstance = MatInst_SimpleLighting::createInstance(color,FUNNEL_MATERIAL_CONSTANTS);
	auto meshResource = MeshResource::createFromConvexHull(STR(""),
														   VertexPosNormalTexCoord::s_decl,
														   hull,
														   true,
														   VertexPosNormalTexCoord::initMeshDefault);

	auto meshComponent = constructObjectNameless<MeshComponent>(actor,meshResource,materialInstance);

	auto renderColorActuator = constructObjectNameless<SleepWakeColorActuatorComponent>(actor,
																						rigidBodyComponent,
																						&materialInstance->uniforms.m_colorMult,
																						color,
																						colorScale(color,0.25f));
	actor->rootComponent = rigidBodyComponent;

	actor->rootComponent->transform.position = pos;
	actor->rootComponent->transform.rotation = ori;

	return actor;
}

Actor* WPhysicsFunnel::makePhysGfxBox(ContainerObject* container,float density,const Vector3& halfSize,const Vector3& pos,const Quaternion& ori,ColorRGBA color) {
	auto actor = constructObjectNameless<Actor>(container);

	auto colliderComponent = constructObjectNameless<ColliderComponent>(actor);
	colliderComponent->createChild<CVBox>(halfSize);

	auto rigidBodyComponent = constructObjectNameless<RigidBodyComponent>(actor,density);
	rigidBodyComponent->setCollider(colliderComponent);

	auto materialInstance = MatInst_SimpleLighting::createInstance(color,FUNNEL_MATERIAL_CONSTANTS);
	auto meshResource = MeshResource::findByName(STR("Cube")); assert(meshResource != nullptr);
	auto meshComponent = constructObjectNameless<MeshComponent>(actor,meshResource,materialInstance);
	meshComponent->transform.scale = halfSize*2.0f;
	auto renderColorActuator = constructObjectNameless<SleepWakeColorActuatorComponent>(actor,
																						rigidBodyComponent,
																						&materialInstance->uniforms.m_colorMult,
																						color,
																						colorScale(color,0.25f));
	actor->rootComponent = rigidBodyComponent;

	actor->rootComponent->transform.position = pos;
	actor->rootComponent->transform.rotation = ori;

	return actor;
}

Actor* WPhysicsFunnel::makePhysGfxEllipsoid(ContainerObject* container,float density,const Vector3& halfSize,const Vector3& pos,const Quaternion& ori,ColorRGBA color) {
	auto actor = constructObjectNameless<Actor>(container);

	auto colliderComponent = constructObjectNameless<ColliderComponent>(actor);
	colliderComponent->createChild<CVEllipsoid>(halfSize);

	auto rigidBodyComponent = constructObjectNameless<RigidBodyComponent>(actor,density);
	rigidBodyComponent->setCollider(colliderComponent);

	auto materialInstance = MatInst_SimpleLighting::createInstance(color,FUNNEL_MATERIAL_CONSTANTS);
	auto meshResource = MeshResource::findByName(STR("Sphere")); assert(meshResource != nullptr);
	auto meshComponent = constructObjectNameless<MeshComponent>(actor,meshResource,materialInstance);
	meshComponent->transform.scale = halfSize*2.0f;
	auto renderColorActuator = constructObjectNameless<SleepWakeColorActuatorComponent>(actor,
																						rigidBodyComponent,
																						&materialInstance->uniforms.m_colorMult,
																						color,
																						colorScale(color,0.25f));
	actor->rootComponent = rigidBodyComponent;

	actor->rootComponent->transform.position = pos;
	actor->rootComponent->transform.rotation = ori;

	return actor;
}

void WPhysicsFunnel::putAboveFunnel(Actor* actor,const Vector3& posoffset) {
	Vector3 pos;
	pos = _FUNNEL_POSITION + posoffset;
	pos.y += _FUNNEL_HEIGHT / 2.0f + lat_rand.getFloat(0,50);
	; {
		//uniform point-in-circle algorithm from: http://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
		const float R = _FUNNEL_RAD_TOP*0.8f;
		const float t = 2 * (float)M_PI * lat_rand.getFloat();
		const float u = lat_rand.getFloat() + lat_rand.getFloat();
		const float r = u > 1.0f ? 2.0f-u : u;
		pos.x = r*cos(t)*R;
		pos.z = r*sin(t)*R;
	}
	Quaternion ori = Quaternion::CreateFromYawPitchRoll(lat_rand.getFloat(0,(float)M_PI*2),
														lat_rand.getFloat(0,(float)M_PI*2),
														lat_rand.getFloat(0,(float)M_PI*2));

	actor->rootComponent->transform.position = pos;
	actor->rootComponent->transform.rotation = ori;

	actor->rigidBody->angularVelocity = {
		lat_rand.getFloat(-(float)M_PI/4,(float)M_PI/4),
		lat_rand.getFloat(-(float)M_PI/4,(float)M_PI/4),
		lat_rand.getFloat(-(float)M_PI/4,(float)M_PI/4)
	};
}

Actor* WPhysicsFunnel::generateFodderDecomposition(const engine_string& fileName) {
	auto actor = makePhysGfxMesh(fodderContainer,
												_FODDER_DENSITY*2,
												fileName,
												{0,0,0},
												Quaternion::Identity,
												[](const ConvexHull* hull) { return colorRandomRGBA(0xff); });
	if(actor) {
		uniformScaleFitWithinByAABB(actor,lat_rand.getFloat(30,100));
		putAboveFunnel(actor,{0,0,0});
	}

	return actor;
}

Actor* WPhysicsFunnel::generateFodderConvexHull() {
	std::vector<Vector3> pointCloud(lat_rand.getInteger(5,25));
	for(auto& point : pointCloud) {
		for(uint8 i = 0; i < 3; i++) {
			const float frand = lat_rand.getFloat(1.5f,3.5f);
			point[i] = frand*frand * (lat_rand.getInteger()%2==0 ? 1 : -1);

			// add some random spikeyness
			if(lat_rand.getInteger()%50==0) point[i] *= 3.0f;
		}
	}

	ConvexHull convexHull = MathUtilities::quickHull(pointCloud.data(),pointCloud.size());
	const unsigned color = colorScale(colorRandomRGBA(0xff),1.25f);
	return makePhysGfxConvexHull(fodderContainer,_FODDER_DENSITY,std::move(convexHull),{0,0,0},Quaternion::Identity,color);
}

Actor* WPhysicsFunnel::generateFodderBox() {
	const Vector3 hs = {
		lat_rand.getFloat(2.5f,12),
		lat_rand.getFloat(2.5f,12),
		lat_rand.getFloat(2.5f,12)
	};
	const unsigned color = colorScale(colorRandomRGBA(0xff),1.25f);
	return makePhysGfxBox(fodderContainer,_FODDER_DENSITY,hs,{0,0,0},Quaternion::Identity,color);
}

Actor* WPhysicsFunnel::generateFodderEllipsoid(bool forceSphere) {
	auto lam_generateHalfSizeElement = []()->float {
		return lat_rand.getFloat(2.5f,13);
	};

	Vector3 hs;

	if(forceSphere) {
		const float radius = lam_generateHalfSizeElement();
		hs = {
			radius,
			radius,
			radius
		};
	} else {
		hs = {
			lam_generateHalfSizeElement(),
			lam_generateHalfSizeElement(),
			lam_generateHalfSizeElement()
		};
	}

	const unsigned color = colorScale(colorRandomRGBA(0xff),1.25f);
	return makePhysGfxEllipsoid(fodderContainer,_FODDER_DENSITY,hs,{0,0,0},Quaternion::Identity,color);
}

Actor* WPhysicsFunnel::generateFodderRandom() {
	switch(lat_rand.getInteger()%4) {
		case 0: return generateFodderBox();
		case 1: return generateFodderEllipsoid(false); // ellipsoids
		case 2: return generateFodderEllipsoid(true); // spheres
		case 3: return generateFodderConvexHull();
	}
	return nullptr;
}

void WPhysicsFunnel::destroyAllFodder() {
	fodderContainer->destroy();
	fodderContainer = constructObject<ContainerObject>(this,"Fodder");
}

void WPhysicsFunnel::makeFunnel(ContainerObject* container,float height,float thickness,float radius_bottom,float radius_top,int resolution,const Vector3 &pos,const Quaternion &ori) {
	float inc = 2.0f * (float)M_PI / resolution;
	float theta = 0.0f;
	float boxwidth = 2*tan((float)M_PI/resolution)*std::max(radius_bottom,radius_top);
	float boxheight = sqrt(SQ(abs(radius_bottom-radius_top))+SQ(height));
	for(int i = 0; i < resolution; i++) {
		Vector3 point[2];
		Vector3 rotoff = Vector3::Transform({cos(theta),0.0f,sin(theta)},ori);
		point[0] = pos + radius_bottom * rotoff;
		point[1] = pos + radius_top * rotoff;
		point[0].y -= height/2;
		point[1].y += height/2;

		float rotonaxis[2] = {
			acos(height/boxheight),
			(float)M_PI/2.0f-theta
		};
		Quaternion rot = ori;
		{
			Quaternion qtmp = Quaternion::CreateFromAxisAngle({0,1,0},rotonaxis[1]);//rotate to face center
			rot = qtmp * rot;
			qtmp = Quaternion::CreateFromAxisAngle({1,0,0},rotonaxis[0]);//rotate to form slope
			rot = qtmp * rot;
		}

		Vector3 newpos = point[0] + Vector3::Transform({0,boxheight/2,thickness/2},rot);

		makePhysGfxBox(environmentContainer,0,Vector3(boxwidth/2,boxheight/2,thickness),newpos,rot,colorScale(colorRandomRGBA(0xff),1.5f));

		theta += inc;
	}
}