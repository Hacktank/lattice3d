#pragma once

#include "core/ActorComponent.h"
#include "core/Tickable.h"
#include "graphics/Color.h"

#include <memory>

class SleepWakeColorActuatorComponent : public ActorComponent,private Tickable {
public:
	class RigidBodyComponent* targetPhysicsBodyComponent;
	ColorRGBA_Packed_Float4* targetColorVariable;

	ColorRGBA colorAwake;
	ColorRGBA colorAsleep;

	SleepWakeColorActuatorComponent(const ObjectConstructionData& ocd);
	SleepWakeColorActuatorComponent(const ObjectConstructionData& ocd,
									RigidBodyComponent* targetPhysicsBodyComponent,
									ColorRGBA_Packed_Float4* targetColorVariable,
									ColorRGBA colorAwake,
									ColorRGBA colorAsleep);
	virtual ~SleepWakeColorActuatorComponent();

	void onTick(uint32 groups,engine_duration deltaTime) override;
};
