#pragma once

#include "core/ActorComponent.h"
#include "core/Tickable.h"

class ScaleOscillatorComponent : public ActorComponent,private Tickable {
public:
	Vector3 baseScale;

	Vector3 thetaOffsets;
	Vector3 thetaCoefs;
	Vector3 scaleMultMin;
	Vector3 scaleMultMax;

	ScaleOscillatorComponent(const ObjectConstructionData& ocd);
	ScaleOscillatorComponent(const ObjectConstructionData& ocd,
							 const Vector3& thetaOffsets,
							 const Vector3& thetaCoefs,
							 const Vector3& scaleMultMin,
							 const Vector3& scaleMultMax);
	virtual ~ScaleOscillatorComponent();

private:
	void onTick(uint32 groups,engine_duration deltaTime) override;
};
