#include "SleepWakeColorActuatorComponent.h"

#include "physics/RigidBodyComponent.h"
#include "graphics/Renderable.h"

#include "core/EngineSettings.h"
#include "core/Actor.h"
#include "graphics/MeshComponent.h"

SleepWakeColorActuatorComponent::SleepWakeColorActuatorComponent(const ObjectConstructionData& ocd) : ActorComponent(ocd) {
	targetPhysicsBodyComponent = nullptr;
	targetColorVariable = nullptr;
	colorAwake = ColorRGBA(255,255,255,255);
	colorAsleep = ColorRGBA(100,100,100,255);

	tickGroup = TickGroup::PRE_GRAPHICS;
}

SleepWakeColorActuatorComponent::SleepWakeColorActuatorComponent(const ObjectConstructionData& ocd,
																 RigidBodyComponent* targetPhysicsBodyComponent,
																 ColorRGBA_Packed_Float4* targetColorVariable,
																 ColorRGBA colorAwake,
																 ColorRGBA colorAsleep) : ActorComponent(ocd) {
	this->targetPhysicsBodyComponent = targetPhysicsBodyComponent;
	this->targetColorVariable = targetColorVariable;
	this->colorAwake = colorAwake;
	this->colorAsleep = colorAsleep;

	tickGroup = TickGroup::PRE_GRAPHICS;
}

SleepWakeColorActuatorComponent::~SleepWakeColorActuatorComponent() {

}

void SleepWakeColorActuatorComponent::onTick(uint32 groups,engine_duration deltaTime) {
	if(targetPhysicsBodyComponent != nullptr && targetColorVariable != nullptr) {
		bool useWakeColor = targetPhysicsBodyComponent->isAwake();

		auto actorMeshComponent = getActor()->findFirstSubObjectByType<MeshComponent>();
		if(actorMeshComponent->wireframe || lat_graphics.globalWireframe) useWakeColor = true;
		  
		*targetColorVariable = useWakeColor ? colorAwake : colorAsleep;
	}
}
