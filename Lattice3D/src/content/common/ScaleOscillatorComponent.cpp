#include "ScaleOscillatorComponent.h"

#include "core/Actor.h"
#include "core/EngineSettings.h"
#include "core/World.h"

ScaleOscillatorComponent::ScaleOscillatorComponent(const ObjectConstructionData& ocd) : ActorComponent(ocd) {

}

ScaleOscillatorComponent::ScaleOscillatorComponent(const ObjectConstructionData& ocd,
												   const Vector3& thetaOffsets,
												   const Vector3& thetaCoefs,
												   const Vector3& scaleMultMin,
												   const Vector3& scaleMultMax) : ActorComponent(ocd),
												   thetaOffsets(thetaOffsets),
												   thetaCoefs(thetaCoefs),
												   scaleMultMin(scaleMultMin),
												   scaleMultMax(scaleMultMax) {
	baseScale = getActor()->rootComponent->transform.scale;

	tickGroup = TickGroup::ENTITY;
}

ScaleOscillatorComponent::~ScaleOscillatorComponent() {

}

void ScaleOscillatorComponent::onTick(uint32 groups,engine_duration deltaTime) {
	const float thetaBase = (float)lat_system.activeWorld->gameTime.count();
	for(uint8 i = 0; i < 3; i++) {
		const float baseMult = sin(thetaBase*thetaCoefs[i] + thetaOffsets[i]) * 0.5f + 0.5f;
		const float finalMult = (baseMult*(scaleMultMin[i]-scaleMultMax[i]))+scaleMultMax[i];

		getActor()->rootComponent->transform.scale[i] = baseScale[i] * finalMult;
	}
}
