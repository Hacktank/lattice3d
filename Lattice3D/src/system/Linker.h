#pragma once

#ifndef _DEBUG
#pragma comment(lib,"bgfxRelease.lib")
#pragma comment(lib,"bgfxFrameworkRelease.lib")

#pragma comment(lib,"vhacdRelease.lib")
#else
#pragma comment(lib,"bgfxDebug.lib")
#pragma comment(lib,"bgfxFrameworkDebug.lib")

#pragma comment(lib,"vhacdDebug.lib")
#endif
