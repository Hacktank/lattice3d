#include "Application.h"

#include "core/Engine.h"
#include "graphics/Vertex.h"
#include "graphics/camera/CameraComponent.h"
#include "graphics/Renderable.h"

#include <bgfx/framework/bgfx_utils.h>
#include <bgfx/framework/imgui/imgui.h>

#include "content/levels/physics_funnel/PhysicsFunnel.h"

#include <bgfx/framework/debugdraw/debugdraw.h>

// link to third party libraries
#include "Linker.h"

void Application::init(int argc,char** argv) {
	static const uint32 RAND_SEED = static_cast<uint32>(std::chrono::system_clock::now().time_since_epoch().count());

	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	windowWidth = 1280;
	windowHeight = 720;
	bgfxDebug = BGFX_DEBUG_TEXT;
	bgfxReset = BGFX_RESET_NONE; //BGFX_RESET_VSYNC

	bgfx::init(bgfx::RendererType::Direct3D11,BGFX_PCI_ID_NONE);

	// enable debug text
	bgfx::setDebug(bgfxDebug);

	bgfx::setViewClear(0,
					   BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH,
					   0x222222ff,
					   1.0f,
					   0);

	// Create vertex stream declaration.
	VertexPosColor::init();
	VertexPosNormalTexCoord::init();
	VertexPosColorBarycentric::init();

	EngineSettings::Instance().initialize(this,std::this_thread::get_id(),RAND_SEED);

	// initialize the debug drawing engine
	ddInit(true);

	// initialize the gui engine
	imguiCreate();

	framesThisSecond = framesPerSecond = 0;
	tp_lastFrameRatePollTime = lat_time.tpLastFrameTime = engine_clock::now();

	//////// Initialize Threads //////////////
	// create enough implicit threads to occupy every logical core (at least one worker no matter what)
	lat_system.threadSpooler.initialize();
	///////////////////////////////////

	//////// Load Assets AND Initialize Level //////////////
	; {
		auto world = constructObjectNameless<WPhysicsFunnel>(nullptr);
		lat_system.activeWorld = world;
	}
}

int Application::shutdown() {
	// properly destroy all engine Objects
	; {
		// add all objects to the destruction queue
		for(auto it = LatObject::getObjectRegistryIteratorBegin(); it != LatObject::getObjectRegistryIteratorEnd(); it++) {
			(*it)->destroy();
		}

		// flush the destruction queue
		LatObject::doGarbageCollection();
	}

	ddShutdown();

	EngineSettings::Destroy();

	imguiDestroy();
	bgfx::shutdown();

	return 0;
}

bool Application::update() {
	bgfx::setDebug(bgfxDebug);

	if(!entry::processEvents(windowWidth,windowHeight,bgfxDebug,bgfxReset,&mouseState)) {
		engine_time_point tp_now = engine_clock::now();
		lat_time.timeDeltaLastFrame = tp_now - lat_time.tpLastFrameTime;
		lat_time.tpLastFrameTime = tp_now;

		// begin a debug draw frame before any user code so that it may do any debug drawing.
		// frame is ended in Application::render()
		ddBegin(0);

		World* activeWorld = lat_system.activeWorld;

		// GARBAGE COLLECTION
		//////////////////////////////////////////////////////////////////////////
		LatObject::doGarbageCollection();

		// INPUT
		//////////////////////////////////////////////////////////////////////////
		lat_system.input.updateInput(&mouseState);

		// SOUND
		//////////////////////////////////////////////////////////////////////////
		// TODO

		// UI
		//////////////////////////////////////////////////////////////////////////
		// UI is updated off real time, not connected to the world
		Tickable::tickGroups(TickGroup::UI,lat_time.timeDeltaLastFrame);

		// ACTIVE WORLD
		//////////////////////////////////////////////////////////////////////////
		if(activeWorld != nullptr) activeWorld->frame();

		// RENDERING
		//////////////////////////////////////////////////////////////////////////
		; {
			// pre-render tick
			if(activeWorld != nullptr) Tickable::tickGroups(TickGroup::PRE_GRAPHICS,activeWorld->gameFrameTimeDelta);

			// actually render
			render();

			// post-render tick
			if(activeWorld != nullptr) Tickable::tickGroups(TickGroup::POST_GRAPHICS,activeWorld->gameFrameTimeDelta);
		}

		// FRAME COUNTING
		//////////////////////////////////////////////////////////////////////////
		framesThisSecond++;
		tp_now = engine_clock::now();
		if((tp_now - tp_lastFrameRatePollTime).count() >= 1.0) {
			tp_lastFrameRatePollTime = tp_now;
			framesPerSecond = framesThisSecond;
			framesThisSecond = 0;
		}

		return true;
	}

	return false;
}

void Application::render() {
	// set up view and projection matrices
	World* activeWorld = lat_system.activeWorld;
	CameraComponent* activeCamera = activeWorld ? activeWorld->getActiveCamera() : nullptr;

	Matrix view,proj;
	if(activeCamera != nullptr) {
		view = activeCamera->getViewMatrix();
		proj = activeCamera->getProjectionMatrix();
		lat_graphics.runtime.vecEyePosition = activeCamera->transform.position;
	} else {
		Vector3 at = {0.0f, 0.0f,   0.0f};
		Vector3 eye = {0.0f, 0.0f, -35.0f};
		bx::mtxLookAt(&view.m[0][0],(float*)&eye,(float*)&at);
		bx::mtxProj(&proj.m[0][0],60.0f,float(windowWidth)/float(windowHeight),0.1f,100.0f);
		lat_graphics.runtime.vecEyePosition = eye;
	}

	bgfx::setViewTransform(0,&view.m[0][0],&proj.m[0][0]);

	// Set view 0 default viewport.
	bgfx::setViewRect(0,0,0,windowWidth,windowHeight);

	// This dummy draw call is here to make sure that view 0 is cleared
	// if no other draw calls are submitted to view 0.
	bgfx::touch(0);

	// Use debug font to print information about this example.
	bgfx::dbgTextClear();
	; {
		uint8 cury = 0;
		bgfx::dbgTextPrintf(0,cury++,0x4f,"FPS: %i",framesPerSecond);
		bgfx::dbgTextPrintf(0,cury++,0x4f,"Timescale: %.4f",lat_time.timeScale);

		if(activeWorld) {
			bgfx::dbgTextPrintf(0,cury++,0x4f,"Physics:");
			bgfx::dbgTextPrintf(0,cury++,0x4f,"  Time Step: %.4fs",activeWorld->physicsFrameTimeDelta.count());
			bgfx::dbgTextPrintf(0,cury++,0x4f,"  Time: %.4fs",activeWorld->physicsTime.count());
			bgfx::dbgTextPrintf(0,cury++,0x4f,"  Frame: %lli",activeWorld->getPhysicsFrameNumber());
			bgfx::dbgTextPrintf(0,cury++,0x4f,"  Contacts: %i",activeWorld->contacts.size());
		}
	}

	lat_graphics.runtime.mtxView = view;
	lat_graphics.runtime.mtxProjection = proj;

	// render all objects that inherit from Renderable
	Renderable::renderAll();

	ddEnd();

	// Advance to next frame. Rendering thread will be kicked to
	// process submitted rendering primitives.
	bgfx::frame();
}

ENTRY_IMPLEMENT_MAIN(Application);