#include "core/EngineTypes.h"

#include <bgfx/framework/common.h>

class Application : public entry::AppI {
private:
	entry::MouseState mouseState;

public:
	uint32 windowWidth;
	uint32 windowHeight;
	uint32 bgfxDebug;
	uint32 bgfxReset;

	engine_time_point tp_lastFrameRatePollTime;
	uint32 framesThisSecond;
	uint32 framesPerSecond;

private:
	void init(int argc,char** argv) override;

	int shutdown() override;

	bool update() override;

	void render();
};
