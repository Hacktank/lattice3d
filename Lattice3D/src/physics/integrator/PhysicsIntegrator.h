#pragma once

#include "core/ThreadSpooler.h"
#include "core/LatObject.h"

#include <vector>

class World;
class RigidBodyComponent;

class PhysicsIntegrator : public LatObject {
protected:
	World* myWorld;

	virtual void updateSleepWake(RigidBodyComponent* body,const Vector3& deltaPosition,const Vector3& deltaRotation,float deltaTime);

public:
	PhysicsIntegrator(const ObjectConstructionData& ocd);
	virtual ~PhysicsIntegrator();

	virtual void frame(World* world);
};
