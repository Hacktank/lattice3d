#pragma once

#include "PhysicsIntegrator.h"

class World;
class RigidBodyComponent;
class ColliderComponent;

class RK4Integrator : public PhysicsIntegrator {
private:
	typedef std::vector<RigidBodyComponent*> ProcItemsCont;
	ProcItemsCont procItems;

	void integrateBodies(ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd);

	struct Derivative {
		Vector3 linearVelocity;
		Vector3 linearAcceleration;

		Vector3 angularVelocity;
		Vector3 angularAcceleration;

		Derivative() {
			linearVelocity = linearAcceleration = angularVelocity = angularAcceleration = {0,0,0};
		}

		Derivative operator+(const Derivative& r) const {
			Derivative ret = *this;
			ret += r;
			return ret;
		}
		friend Derivative operator*(float r,const Derivative& l) {
			Derivative ret = l;
			ret *= r;
			return ret;
		}
		Derivative& operator+=(const Derivative& r) {
			linearVelocity += r.linearVelocity;
			linearAcceleration += r.linearAcceleration;
			angularVelocity += r.angularVelocity;
			angularAcceleration += r.angularAcceleration;
			return *this;
		}
		Derivative& operator*=(float r) {
			linearVelocity *= r;
			linearAcceleration *= r;
			angularVelocity *= r;
			angularAcceleration *= r;
			return *this;
		}
	};

	void evaluate(Derivative* output,const RigidBodyComponent* body,float time,float deltaTime,const Derivative& d);
	void integrate(RigidBodyComponent* body);

public:
	RK4Integrator(const ObjectConstructionData& ocd);
	virtual ~RK4Integrator();

	virtual void frame(World* world);
};