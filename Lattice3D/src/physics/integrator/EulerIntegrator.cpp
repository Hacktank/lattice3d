#include "EulerIntegrator.h"
#include "physics/RigidBodyComponent.h"

#include "physics/collider/ColliderComponent.h"
#include "core/Actor.h"
#include "core/World.h"

void EulerIntegrator::integrateBodies(ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd) {
	const float deltaTime = (float)myWorld->physicsFrameTimeDelta.count();
	for(auto it = itBegin; it != itEnd; it++) {
		RigidBodyComponent* body = *it;
		ColliderComponent* collider = body->getCollider();
		if(collider == nullptr) continue;

		if(collider->inverseMass != 0 && body->isAwake()) {
			// helper data for contact resolution
			body->lastFrameAcceleration = lat_physics.gravity * body->gravityMultiplier;
			body->lastFrameAcceleration += body->accumulatedForce * collider->inverseMass;

			body->lastFrameAcceleration *= deltaTime;

			// Euler integration
			const Vector3 accelerationThisFrame = body->lastFrameAcceleration + body->accumulatedForce * collider->inverseMass;
			const Vector3 angularAccelerationThisFrame = body->accumulatedTorque * collider->inertialTensor_inverse;

			body->velocity += accelerationThisFrame;
			body->angularVelocity += angularAccelerationThisFrame;

			// damping
			body->velocity *= pow(lat_physics.damping.linear,deltaTime);
			body->angularVelocity *= pow(lat_physics.damping.angular,deltaTime);

			// apply transform changes

			// do not create unnecessary penetration
			Vector3 adjustedVelocity = body->velocity;

			for(auto& contact : collider->collision_resolution.contactInvolvement) {
				if(!contact->isValid()) continue;

				const uint8 otherColliderInd = collider == contact->colliders[0] ? 0 : 1;
				const float sign = float(otherColliderInd==0 ? -1 : 1);

				if(contact->penetration >= 0) {
					adjustedVelocity -= sign * std::max(sign*(adjustedVelocity | contact->normal),0.0f) * contact->normal;
				}
			}

			body->transform.position += adjustedVelocity * deltaTime;
			body->transform.rotation = MathUtilities::quatAddAngularOffset(body->transform.rotation,body->angularVelocity * deltaTime);
			body->transform.rotation.Normalize();

			body->clearAccumulators();

			// sleep/wake
			updateSleepWake(body,adjustedVelocity,body->angularVelocity,deltaTime);

			// TODO: make this more efficient
			collider->recalculateDerivedData();
		}
	}
}

EulerIntegrator::EulerIntegrator(const ObjectConstructionData& ocd) : PhysicsIntegrator(ocd) {
}

EulerIntegrator::~EulerIntegrator() {
}

void EulerIntegrator::frame(World* world) {
	__super::frame(world);

	// find all rigid bodies in world
	procItems.clear();
	for(auto actor : world->getActors()) {
		if(actor->rigidBody != nullptr) procItems.push_back(actor->rigidBody);
	}

	const uint32 numItems = (uint32)procItems.size();
	const uint32 numWorkers = std::min(lat_system.threadSpooler.getOptiomalNumConcurrentTasks(),numItems);
	std::vector<std::future<void>> completionFutures; completionFutures.reserve(numWorkers);
	//assert(numWorkers > 0);

	if(numItems > 0) {
		const uint32 div = numItems/numWorkers;
		const uint32 mod = numItems%numWorkers;
		uint32 curIndex = 0;
		for(uint32 i = 0; i < numWorkers; i++) {
			const uint32 thisBlockSize = div + (i < mod ? 1 : 0);
			const uint32 indStart = curIndex;
			const uint32 indEnd = curIndex + thisBlockSize;
			curIndex += thisBlockSize;

			completionFutures.emplace_back(lat_system.threadSpooler.enqueueWork(std::bind(&EulerIntegrator::integrateBodies,this,procItems.begin()+indStart,procItems.begin()+indEnd)));
		}

		for(auto& future : completionFutures) future.wait();
	}
}