#include "RK4Integrator.h"

#include "physics/RigidBodyComponent.h"

#include "physics/collider/ColliderComponent.h"
#include "core/Actor.h"
#include "core/World.h"

void RK4Integrator::integrateBodies(ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd) {
	const float deltaTime = (float)myWorld->physicsFrameTimeDelta.count();
	for(auto it = itBegin; it != itEnd; it++) {
		RigidBodyComponent* body = *it;
		ColliderComponent* collider = body->getCollider();
		if(collider == nullptr) continue;

		if(collider->inverseMass != 0 && body->isAwake()) {
			integrate(body);

			collider->recalculateDerivedData();
		}
	}
}

void RK4Integrator::evaluate(Derivative* output,const RigidBodyComponent* body,float time,float deltaTime,const Derivative& d) {
	ColliderComponent* collider = body->getCollider();

	output->linearVelocity = body->velocity + d.linearAcceleration * deltaTime;
	output->linearAcceleration =
		lat_physics.gravity * body->gravityMultiplier +
		body->accumulatedForce * collider->inverseMass;

	output->angularVelocity = body->angularVelocity + d.angularAcceleration * deltaTime;
	output->angularAcceleration = body->accumulatedTorque * collider->inertialTensor_inverse;
}

void RK4Integrator::integrate(RigidBodyComponent* body) {
	ColliderComponent* collider = body->getCollider();

	float time = (float)myWorld->physicsTime.count();
	float deltaTime = (float)myWorld->physicsFrameTimeDelta.count();

	Derivative d[4];
	evaluate(&d[0],body,time,0,Derivative());
	evaluate(&d[1],body,time,deltaTime * 0.5f,d[0]);
	evaluate(&d[2],body,time,deltaTime * 0.5f,d[1]);
	evaluate(&d[3],body,time,deltaTime,d[2]);

	Derivative finalDerivative;

	finalDerivative = 1.0f/6.0f * (d[0] + 2.0f * (d[1] + d[2]) + d[3]);

	// apply damping to derivative
	finalDerivative.linearVelocity *= pow(lat_physics.damping.linear,deltaTime);
	finalDerivative.angularVelocity *= pow(lat_physics.damping.angular,deltaTime);

	body->velocity += finalDerivative.linearAcceleration * deltaTime;
	body->angularVelocity += finalDerivative.angularAcceleration * deltaTime;

	// do not apply any velocity to our position that will cause further penetration with any
	// contacts we are involved in. this does not affect the actual velocity of this body
	// so energy transfer between objects still functions as expected.
	// TODO: make this algorithm apply to angular velocity as well
	Vector3 velocityToApply = finalDerivative.linearVelocity;
	for(auto& contact : collider->collision_resolution.contactInvolvement) {
		if(!contact->isValid()) continue;

		const uint8 myColliderIndexInContact = collider == contact->colliders[0] ? 0 : 1;
		const float sign = float(myColliderIndexInContact==0 ? -1 : 1);

		if(contact->penetration >= 0) {
			velocityToApply -= sign * std::max(sign*(velocityToApply | contact->normal),0.0f) * contact->normal;
		}
	}

	body->transform.position += velocityToApply * deltaTime;
	body->transform.rotation = MathUtilities::quatAddAngularOffset(body->transform.rotation,finalDerivative.angularVelocity * deltaTime).GetNormal();

	updateSleepWake(body,velocityToApply,finalDerivative.angularVelocity,deltaTime);

	body->clearAccumulators();
}

RK4Integrator::RK4Integrator(const ObjectConstructionData& ocd) : PhysicsIntegrator(ocd) {
}

RK4Integrator::~RK4Integrator() {
}

void RK4Integrator::frame(World* world) {
	__super::frame(world);

	// find all rigid bodies in world
	procItems.clear();
	for(auto actor : world->getActors()) {
		if(actor->rigidBody != nullptr) procItems.push_back(actor->rigidBody);
	}

	const uint32 numItems = (uint32)procItems.size();
	const uint32 numWorkers = std::min(lat_system.threadSpooler.getOptiomalNumConcurrentTasks(),numItems);
	std::vector<std::future<void>> completionFutures; completionFutures.reserve(numWorkers);
	//assert(numWorkers > 0);

	if(numItems > 0) {
		const uint32 div = numItems/numWorkers;
		const uint32 mod = numItems%numWorkers;
		uint32 curIndex = 0;
		for(uint32 i = 0; i < numWorkers; i++) {
			const uint32 thisBlockSize = div + (i < mod ? 1 : 0);
			const uint32 indStart = curIndex;
			const uint32 indEnd = curIndex + thisBlockSize;
			curIndex += thisBlockSize;

			completionFutures.emplace_back(lat_system.threadSpooler.enqueueWork(std::bind(&RK4Integrator::integrateBodies,this,procItems.begin()+indStart,procItems.begin()+indEnd)));
		}

		for(auto& future : completionFutures) future.wait();
	}
}