#include "PhysicsIntegrator.h"

#include "core/EngineSettings.h"

#include "physics/RigidBodyComponent.h"
#include "physics/collider/ColliderComponent.h"

void PhysicsIntegrator::updateSleepWake(RigidBodyComponent* body,const Vector3& deltaPosition,const Vector3& deltaRotation,float deltaTime) {
	ColliderComponent* collider = body->getCollider();

	if(lat_physics.sleep.automaticSleepingEnabled &&
	   body->canSleep) {
		Contact* c;
		bool touchingSolidGround = false; // solid ground is any static or asleep object
		bool hasRestingContact = false; // has any contacts that have zero desiredDeltaVelocity, will be <0 for contacts that are moving together, and >0 for ones moving apart
		for(auto& contact : collider->collision_resolution.contactInvolvement) {
			if(!contact->isValid()) continue;

			if(!hasRestingContact) {
				hasRestingContact = fabs(contact->resolution_impulse.desiredDeltaVelocity) <= 0.00001f;
				c = contact;
			}

			if(!touchingSolidGround) {
				for(uint8 i = 0; i < 2; i++) {
					if(contact->colliders[i]->isStatic() || !contact->colliders[i]->rigidBody->isAwake()) {
						touchingSolidGround = true;
					}
				}
			}

			if(hasRestingContact && touchingSolidGround) break;
		}

		const float& epsilon = lat_physics.sleep.epsilon_motion;

		const Vector3 velocityToConcider = deltaPosition + deltaRotation*15.0f;
		const float currentMotion = velocityToConcider | velocityToConcider;
		const float motionBias = currentMotion > body->__motion ? 0 : pow(lat_physics.sleep.motionBiasCoef,deltaTime);

		body->__motion = clamp(motionBias*body->__motion + (1.0f-motionBias)*currentMotion,
							   0.0f,
							   epsilon*10.0f);

		if((body->transform.position.LengthSquared() > lat_physics.sleep.maximumPhysicsDistanceFromOriginSq) ||
			(touchingSolidGround && currentMotion < epsilon && body->__motion < epsilon)) {
			body->awake = false;
		} else {
			// TODO: add a list of rigid body involvement like the collider's contact involvement, and save it when they go to sleep
			//       so that an entire pile can be awakened at once
		}
	} else {
		body->awake = true;
	}
}

PhysicsIntegrator::PhysicsIntegrator(const ObjectConstructionData& ocd) : LatObject(ocd) {
	myWorld = nullptr;
}

PhysicsIntegrator::~PhysicsIntegrator() {
}

void PhysicsIntegrator::frame(World* world) {
	myWorld = world;
}