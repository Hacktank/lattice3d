#pragma once

#include "PhysicsIntegrator.h"

class World;
class RigidBodyComponent;

class EulerIntegrator : public PhysicsIntegrator {
private:
	typedef std::vector<RigidBodyComponent*> ProcItemsCont;
	ProcItemsCont procItems;

	void integrateBodies(ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd);

public:
	EulerIntegrator(const ObjectConstructionData& ocd);
	virtual ~EulerIntegrator();

	virtual void frame(World* world);
};