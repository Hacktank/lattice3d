#pragma once

#include "core/EngineTypes.h"

#include "core/pool_vector.h"

#include <vector>

class ColliderComponent;
class CollisionVolumeNode;

class ContactBasicData {
public:
	CollisionVolumeNode* collisionVolumeNodes[2];
	ColliderComponent* colliders[2];

	Vector3 point; // point is on the surface of A (inside of B if penetration is positive)
	Vector3 normal; // points from B to A
	float penetration;

	// the local support points of the minkowski difference triangle that was used to generate this contact originally
	// these points can be used to update the contact data even after the two objects have been arbitrarily changed
	Vector3 triangleSupports_local[2][3];
};

class Contact : public ContactBasicData {
private:
	bool invalidFlag;
	bool derivedDataCalculated;
	uint64 physicsFrameGenesis;
	pool_vector<Contact*>::iterator colliderInvolvementIterators[2];

public:
	Vector3 initialContactPoint;

	// DERIVED
	Matrix matContactToWorld;
	Matrix matWorldToContact;
	Vector3 closingVelocity_world;
	Vector3 closingVelocity_contact;
	Vector3 relativeContactPosition[2];
	float coefRestitution;
	float coefFriction;

	// USED IN IMPULSE CONTACT RESOLUTION
	struct {
		float desiredDeltaVelocity;
	} resolution_impulse;

	Contact(const ContactBasicData& data);

	Contact(Contact&& other);

	~Contact();

	uint32 getFrameAge() const;
	bool isValid() const;
	void invalidate();

	void invert();
	void calculateDerivedData();

	bool needsResolution() const;
};