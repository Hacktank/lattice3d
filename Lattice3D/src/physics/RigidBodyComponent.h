#pragma once

#include "core/ActorComponent.h"

class ColliderComponent;

class RigidBodyComponent : public ActorComponent {
private:
	ColliderComponent* myCollider;

public:
	float __motion; // for automatic sleep

	bool canSleep;
	bool awake;

	float density;
	float gravityMultiplier;
	Vector3 velocity;
	Vector3 angularVelocity;

	Vector3 lastFrameAcceleration;
	Vector3 accumulatedForce;
	Vector3 accumulatedTorque;

	RigidBodyComponent(const ObjectConstructionData& ocd,float density);
	virtual ~RigidBodyComponent();

	// accessors / mutators
	void setCollider(ColliderComponent* collider);
	ColliderComponent* getCollider() const;

	bool isAwake() const { return awake; }

	// physical interaction
	void addForce(const Vector3 &force);
	void addForceAtPointWorld(const Vector3 &force,const Vector3 &point);
	void addForceAtPointBody(const Vector3 &force,const Vector3 &point);
	void addTorque(const Vector3 &torque);

	const Vector3& getLastFrameAcceleration() const { return lastFrameAcceleration; }

	void clearAccumulators();
};
