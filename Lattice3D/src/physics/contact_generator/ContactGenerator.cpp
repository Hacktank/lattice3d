#include "ContactGenerator.h"

#include "physics/collider/ColliderComponent.h"

void ContactGenerator::refreshColliderList() {
	collidersToConsider.clear();

	for(auto it = LatObject::getObjectRegistryIteratorBegin(); it != LatObject::getObjectRegistryIteratorEnd(); it++) {
		ColliderComponent* objectAsCollider = dynamic_cast<ColliderComponent*>(*it);
		if(objectAsCollider != nullptr)
			collidersToConsider.push_back(objectAsCollider);
	}
}

ContactGenerator::ContactGenerator(const ObjectConstructionData& ocd) : LatObject(ocd) {
	myWorld = nullptr;
}

ContactGenerator::~ContactGenerator() {
}

void ContactGenerator::frame(World* world) {
	myWorld = world;

	refreshColliderList();
}