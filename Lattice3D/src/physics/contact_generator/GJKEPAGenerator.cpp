#include "GJKEPAGenerator.h"

#include "core/World.h"
#include "physics/Contact.h"
#include "physics/collider/ColliderComponent.h"
#include "physics/RigidBodyComponent.h"

#include <numeric>

void GJKEPAGenerator::processCollisionTests(ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd) {
	for(auto it = itBegin; it != itEnd; it++) {
		auto& pendingTest = *it;

		ContactBasicData contactData;

		// func() returns true if intersection between the two nodes is found
		typedef std::function<bool(CollisionVolumeNode*)> TravFunc_t;
		// lam_bredthFirst() returns false if func(root) returns false, true otherwise
		std::function<bool(CollisionVolumeNode*,TravFunc_t,bool)> lam_bredthFirstCollisionTestHelper = [&](CollisionVolumeNode* root,TravFunc_t& func,bool returnOnFirstLeafTrue)->bool {
			if(!func(root)) return false;

			for(auto& child : root->getChildren()) {
				const bool childRet = lam_bredthFirstCollisionTestHelper(child,func,returnOnFirstLeafTrue);
				if(returnOnFirstLeafTrue && child->getChildren().size()==0 && childRet) return true;
			}

			return true;
		};

		auto lam_travFuncB = [&](CollisionVolumeNode* nodeA,CollisionVolumeNode* nodeB)->bool {
			if(!performCoarseIntersectionTest(nodeA,nodeB)) return false;

			if(!(nodeA->shouldGenerateContacts() && nodeB->shouldGenerateContacts())) return true;

			Simplex testSimplex;
			uint32 supportPointNextUniqueId = 0;

			contactData.colliders[0] = nodeA->getCollider();
			contactData.collisionVolumeNodes[0] = nodeA;
			contactData.colliders[1] = nodeB->getCollider();
			contactData.collisionVolumeNodes[1] = nodeB;

			if(performGJKIntersectionTest(&testSimplex,&contactData,&supportPointNextUniqueId) &&
			   performEPAManifoldExtraction(&testSimplex,&contactData,&supportPointNextUniqueId)) {
				ht_spinlock_guard lck(spinlock_newContactsThisFrame);

				newContactsThisFrame.emplace_back(std::move(contactData));

				return true;
			}

			return false;
		};

		auto lam_travFuncA = [&](CollisionVolumeNode* nodeA,CollisionVolumeNode* nodeB)->bool {
			const bool nodeAIsLeaf = nodeA->getChildren().size() == 0;
			return lam_bredthFirstCollisionTestHelper(nodeB,
													  std::bind(lam_travFuncB,nodeA,std::placeholders::_1),
													  !nodeAIsLeaf);
		};

		lam_bredthFirstCollisionTestHelper(pendingTest.colliders[0],
										   std::bind(lam_travFuncA,std::placeholders::_1,pendingTest.colliders[1]),
										   false);
	}
}

bool GJKEPAGenerator::extrapolateContactInformation(ContactBasicData* contactData) {
	// build the necessary structure and calculate the necessary data to extrapolate contact information
	const Matrix& volumeA_toWorld = contactData->collisionVolumeNodes[0]->collision_detection.mtxColliderToWorld;
	const Matrix& volumeB_toWorld = contactData->collisionVolumeNodes[1]->collision_detection.mtxColliderToWorld;

	SupportPoint supports[3];

	for(uint8 i = 0; i < 3; i++) {
		supports[i].localSupports[0] = contactData->triangleSupports_local[0][i];
		supports[i].localSupports[1] = contactData->triangleSupports_local[1][i];
		supports[i].v =
			(supports[i].localSupports[0] * volumeA_toWorld) -
			(supports[i].localSupports[1] * volumeB_toWorld);
	}

	Triangle triangle(supports[0],supports[1],supports[2]);

	// actually extrapolate contact information
	return extrapolateContactInformation(&triangle,contactData);
}

bool GJKEPAGenerator::extrapolateContactInformation(const Triangle* triangle,
													ContactBasicData* contactData) {
	const Matrix& volumeA_toWorld = contactData->collisionVolumeNodes[0]->collision_detection.mtxColliderToWorld;
	const Matrix& volumeB_toWorld = contactData->collisionVolumeNodes[1]->collision_detection.mtxColliderToWorld;

	// project the origin onto the triangle
	const float distanceFromOrigin = triangle->n | triangle->points[0].v;

	// calculate the barycentric coordinates of the closest triangle with respect to
	// the projection of the origin onto the triangle
	float bary_u,bary_v,bary_w;
	barycentric(triangle->n * distanceFromOrigin,
				triangle->points[0].v,
				triangle->points[1].v,
				triangle->points[2].v,
				&bary_u,
				&bary_v,
				&bary_w);

	// barycentric can fail and generate invalid coordinates, if this happens return false
	if(!is_valid(bary_u) || !is_valid(bary_v) || !is_valid(bary_w)) return false;

	// if any of the barycentric coefficients have a magnitude greater than 1, then the origin is not within the triangular prism described by 'triangle'
	// thus, there is no collision here, return false
	if(fabs(bary_u)>1.0f || fabs(bary_v)>1.0f || fabs(bary_w)>1.0f) return false;

	// collision point on object a in world space
	const Vector3 point(
		(bary_u * (triangle->points[0].localSupports[0] * volumeA_toWorld)) +
		(bary_v * (triangle->points[1].localSupports[0] * volumeA_toWorld)) +
		(bary_w * (triangle->points[2].localSupports[0] * volumeA_toWorld)));

	// collision normal
	const Vector3 normal = -triangle->n;

	// penetration depth
	const float penetrationDepth = distanceFromOrigin;

	contactData->point = point - normal * lat_physics.collisionSkinWidth;
	contactData->normal = normal;
	contactData->penetration = penetrationDepth - lat_physics.collisionSkinWidth * 2;

	// copy data necessary for contact persistence into the contactData structure
	for(uint8 i = 0; i < 2; i++)
		for(uint8 j = 0; j < 3; j++)
			contactData->triangleSupports_local[i][j] = triangle->points[j].localSupports[i];

	return true;
}

GJKEPAGenerator::SupportPoint GJKEPAGenerator::generateSupport(Vector3 directionWorld,const CollisionVolumeNode* volumeA,const CollisionVolumeNode* volumeB,uint32* uniqueIdCounter) {
	const Matrix& volumeA_toWorld = volumeA->collision_detection.mtxColliderToWorld;
	const Matrix& volumeB_toWorld = volumeB->collision_detection.mtxColliderToWorld;

	directionWorld.Normalize();

	SupportPoint ret(uniqueIdCounter ? (*uniqueIdCounter)++ : 0);
	ret.localSupports[0] = volumeA->getSupportPointLocalSpace(directionWorld,true);
	ret.localSupports[1] = volumeB->getSupportPointLocalSpace(-directionWorld,true);

	const Vector3 skinInDirection = directionWorld * lat_physics.collisionSkinWidth;

	ret.v =
		(ret.localSupports[0] * volumeA_toWorld + skinInDirection) -
		(ret.localSupports[1] * volumeB_toWorld - skinInDirection);
	return ret;
}

bool GJKEPAGenerator::performCoarseIntersectionTest(const CollisionVolumeNode* volumeA,const CollisionVolumeNode* volumeB) {
	// axis-aligned bounding-box intersection test
	const Vector3& amin = volumeA->aabb_world.min;
	const Vector3& amax = volumeA->aabb_world.max;
	const Vector3& bmin = volumeB->aabb_world.min;
	const Vector3& bmax = volumeB->aabb_world.max;

	const float sk = lat_physics.collisionSkinWidth * 2.0f;

	return
		(amin.x <= bmax.x + sk) && (amin.y <= bmax.y + sk) && (amin.z <= bmax.z + sk) &&
		(amax.x >= bmin.x - sk) && (amax.y >= bmin.y - sk) && (amax.z >= bmin.z - sk);
}

#define gjk_simtest(v) (((v)|(ao)) > 0)
bool GJKEPAGenerator::performGJKIntersectionTest(Simplex* inout_simplex,ContactBasicData* in_contactData,uint32* uniqueIdCounter) {
	Simplex& sim = *inout_simplex;

	// Gilbert�Johnson�Keerthi (GJK) algorithm as described here: http://hacktank.net/blog/?p=93 (by me)
	// Inspired by: http://mollyrocket.com/849, and http://vec3.ca/gjk/implementation/
	/*
	********** Note **********
	There is an issue with with ellipsoids that have very non-uniform scaling.
	For some reason we fail to pass the origin on the first or second early out test.
	**************************/

	const unsigned _EXIT_ITERATION_LIMIT = 20;
	unsigned _EXIT_ITERATION_NUM = 0;

	// build the initial simplex, a single support point in an arbitrary position
	sim.clear();
	Vector3 dir = Vector3::UnitY;
	SupportPoint s = generateSupport(dir,in_contactData->collisionVolumeNodes[0],in_contactData->collisionVolumeNodes[1],uniqueIdCounter);
	if(fabs(dir|s.v)>=s.v.Length()*0.8f) {
		// the chosen initial direction is invalid, will produce (0,0,0) for a subsequent direction later, try a different arbitrary direction
		dir = Vector3::UnitX;
		s = generateSupport(dir,in_contactData->collisionVolumeNodes[0],in_contactData->collisionVolumeNodes[1],uniqueIdCounter);
	}
	sim.push(s);
	dir = -s.v;

	// iteratively attempt to build a simplex that encloses the origin
	while(true) {
		if(_EXIT_ITERATION_NUM++ >= _EXIT_ITERATION_LIMIT) return false;

		// error, for some reason the direction vector is broken
		if(dir.LengthSquared()<=0.0001f) return false;

		// get the next point in the direction of the origin
		SupportPoint a = generateSupport(dir,in_contactData->collisionVolumeNodes[0],in_contactData->collisionVolumeNodes[1],uniqueIdCounter);

		// early out: if a.v dot d is less than zero that means that the new point did not go past the origin
		// and thus there is no intersection
		const float proj = a.v | dir;
		if(proj < 0) return false;
		sim.push(a);

		const Vector3 ao = -sim.a.v;

		bool _triangleSimplex_needToTestAboveBelow = true; // used to skip a calculation in the tetrahedron case

		// simplex tests
		if(sim.num == 2) {
			// simplex is a line, being here means that the early out was passed, and thus
			// the origin must be between point a and b
			// search direction is perpendicular to ab and coplanar with ao

			const Vector3 ab = (sim.b.v-sim.a.v);
			dir = ab % ao % ab;
			continue;
		} else if(sim.num == 3) {
			jmp_triangleSimplex: // used used in the sim.num == 4 case

								 // simplex is a triangle
			const Vector3 ab = (sim.b.v-sim.a.v);
			const Vector3 ac = (sim.c.v-sim.a.v);
			const Vector3 abc = ab % ac;

			if(gjk_simtest(ab % abc)) {
				// origin is in front of segment ab
				// we already know that the origin is in front of segment bc and behind the projection of segment bc onto point a
				// this is because point a is the support in the direction of bc's normal

				// [a b] ab % ao % ab
				sim.set(sim.a,sim.b);
				dir = ab % ao % ab;
			} else {
				if(gjk_simtest(abc % ac)) {
					// origin is in front of segment ac
					// just as above, we know that the origin in in front of segment bc and behind ...

					// [a c] ac % ao % ac
					sim.set(sim.a,sim.c);
					dir = ac % ao % ac;
				} else {
					// if we have been redirected to the triangle case by the tetrahedron case this test is unnecessary
					// because we know the origin is above the triangle already
					if(!_triangleSimplex_needToTestAboveBelow || gjk_simtest(abc)) {
						// origin is above the triangle

						// [a b c] abc
						//sim.set(sim.a,sim.b,sim.a); it is already equal to this
						dir = abc;
					} else {
						// origin is below the triangle

						// [a c b] -abc
						sim.set(sim.a,sim.c,sim.b);
						dir = -abc;
					}
				}
			}
			continue;
		} else { // == 4
				 // the simplex is a tetrahedron, must check if it is outside any of the side triangles, (abc, acd, adb)
				 // if it is then set the simplex equal to that triangle and jump back up to the triangle case, otherwise we know
				 // there is an intersection and exit

				 // check the triangles (abc,acd,adb), scoped as the temporary variables used here
				 // will no longer be valid afterward
			; {
				const Vector3 ab = (sim.b.v-sim.a.v);
				const Vector3 ac = (sim.c.v-sim.a.v);

				if(gjk_simtest(ab % ac)) {
					// origin is in front of triangle abc, simplex is already what it needs to be
					// [a b c] goto jmp_triangleSimplex
					sim.num = 3;
					_triangleSimplex_needToTestAboveBelow = false;
					goto jmp_triangleSimplex;
				}

				const Vector3 ad = (sim.d.v-sim.a.v);

				if(gjk_simtest(ac % ad)) {
					// origin is in front of triangle acd, simplex is set to this triangle
					// [a c d] goto jmp_triangleSimplex
					sim.set(sim.a,sim.c,sim.d);
					_triangleSimplex_needToTestAboveBelow = false;
					goto jmp_triangleSimplex;
				}

				if(gjk_simtest(ad % ab)) {
					// origin is in front of triangle adb, simplex is set to this triangle
					// [a d b] goto jmp_triangleSimplex
					sim.set(sim.a,sim.d,sim.b);
					_triangleSimplex_needToTestAboveBelow = false;
					goto jmp_triangleSimplex;
				}

				// intersection confirmed, break from the loop
				break;
			}
		}
	}

	return true;
}
#undef gjk_simtest

bool GJKEPAGenerator::performEPAManifoldExtraction(const Simplex* in_initialSimplex,ContactBasicData* inout_contactData,uint32* uniqueIdCounter) {
	// Expanding Polytope Algorithm (EPA) as described here: http://hacktank.net/blog/?p=119 (by me)
	// Inspired by: http://allenchou.net/2013/12/game-physics-contact-generation-epa/

	// the algorithm will iteratively try to grow the polytope to find the face on the minkowski difference that is closest to the origin
	// it will continue to do this until if fails to push the polytope more than _EXIT_GROWTH_THRESHOLD

	const float _EXIT_GROWTH_THRESHOLD = 0.001f;
	const unsigned _EXIT_ITERATION_LIMIT = 30;
	unsigned _EXIT_ITERATION_CUR = 0;

	std::list<Triangle> lst_triangles;
	std::list<Edge> lst_edges;

	// process the specified edge, if another edge with the same points in the
	// opposite order exists then it is removed and the new point is also not added
	// this ensures only the outermost ring edges of a cluster of triangles remain
	// in the list
	auto lam_addEdge = [&](const SupportPoint &a,const SupportPoint &b)->void {
		for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
			if(it->points[0].uniqueId==b.uniqueId && it->points[1].uniqueId==a.uniqueId) {
				//opposite edge found, remove it and do not add new one
				lst_edges.erase(it);
				return;
			}
		}
		lst_edges.emplace_back(a,b);
	};

	// add the GJK simplex triangles to the list
	lst_triangles.emplace_back(in_initialSimplex->a,in_initialSimplex->b,in_initialSimplex->c);
	lst_triangles.emplace_back(in_initialSimplex->a,in_initialSimplex->c,in_initialSimplex->d);
	lst_triangles.emplace_back(in_initialSimplex->a,in_initialSimplex->d,in_initialSimplex->b);
	lst_triangles.emplace_back(in_initialSimplex->b,in_initialSimplex->d,in_initialSimplex->c);

	while(true) {
		if(_EXIT_ITERATION_CUR++ >= _EXIT_ITERATION_LIMIT) return false;

		// find closest triangle to origin
		std::list<Triangle>::iterator closestTriangle_it;
		float closestTriangle_dst = FLT_MAX;
		for(auto it = lst_triangles.begin(); it != lst_triangles.end(); it++) {
			const float dst = (it->n | it->points[0].v);
			if(dst < closestTriangle_dst) {
				closestTriangle_dst = dst;
				closestTriangle_it = it;
			}
		}

		// get the next support point in front of the current triangle, away from the origin
		const SupportPoint entry_new_support = generateSupport(closestTriangle_it->n,
															   inout_contactData->collisionVolumeNodes[0],
															   inout_contactData->collisionVolumeNodes[1],
															   uniqueIdCounter);

		// checks how much further this new point will take us from the origin
		// if it is not far enough then we assume we have found the closest triangle
		// on the hull from the origin
		const float newDst = closestTriangle_it->n|entry_new_support.v;
		const float growth = newDst - closestTriangle_dst;
		//if(thisItGrowth < 0) break;
		if((growth < _EXIT_GROWTH_THRESHOLD)) {
			//printf("EPA: exit growth: %f\n",growth);

			//////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////
			// GENERATE CONTACT INFO AND RETURN

			return extrapolateContactInformation(&*closestTriangle_it,
												 inout_contactData);

			break;
		}

		for(auto it = lst_triangles.begin(); it != lst_triangles.end();) {
			// can this face be 'seen' by entry_new_support?
			if((it->n | (entry_new_support.v - it->points[0].v)) > 0) {
				lam_addEdge(it->points[0],it->points[1]);
				lam_addEdge(it->points[1],it->points[2]);
				lam_addEdge(it->points[2],it->points[0]);
				it = lst_triangles.erase(it);
				continue;
			}
			it++;
		}

		// create new triangles from the edges in the edge list
		for(auto it = lst_edges.begin(); it != lst_edges.end(); it++) {
			lst_triangles.emplace_back(entry_new_support,it->points[0],it->points[1]);
		}

		lst_edges.clear();
	}

	return false;
}

void GJKEPAGenerator::updatePersistentContacts() {
	typedef decltype(World::contacts)::iterator ContactIterator_t;

	auto lam_workFunction = [](ContactIterator_t itBegin,ContactIterator_t itEnd)->void {
		for(auto it = itBegin; it != itEnd; it++) {
			if(it->isValid()) {
				// are both objects static/asleep?
				if((it->colliders[0]->isStatic() || it->colliders[0]->rigidBody == nullptr || !it->colliders[0]->rigidBody->isAwake()) &&
					(it->colliders[1]->isStatic() || it->colliders[1]->rigidBody == nullptr || !it->colliders[1]->rigidBody->isAwake())) {
					it->invalidate();
				} else {
					if(extrapolateContactInformation(&*it)) { // returns false if the given contact data is invalid
															  // is penetration too low (objects too far away)?
						if(it->penetration < lat_physics.contact_persistance.minPenetration) {
							it->invalidate();
						} else if((it->point - it->initialContactPoint).LengthSquared() > lat_physics.contact_persistance.maxPointDistanceFromInitialSq) {
							it->invalidate();
						}
					} else {
						it->invalidate();
					}
				}
			}
		}
	};

	const uint32 numItems = (uint32)myWorld->contacts.size();
	const uint32 numWorkers = std::min(lat_system.threadSpooler.getOptiomalNumConcurrentTasks(),numItems);
	std::vector<std::future<void>> completionFutures; completionFutures.reserve(numWorkers);

	// split the tests that are to be performed among all available worker threads
	if(numItems > 0) {
		const uint32 div = numItems/numWorkers;
		const uint32 mod = numItems%numWorkers;
		uint32 curIndex = 0;
		for(uint32 i = 0; i < numWorkers; i++) {
			const uint32 thisBlockSize = div + (i < mod ? 1 : 0);
			const uint32 indStart = curIndex;
			const uint32 indEnd = curIndex + thisBlockSize;
			curIndex += thisBlockSize;

			completionFutures.emplace_back(
				lat_system.threadSpooler.enqueueWork(
					std::bind(
						lam_workFunction,
						myWorld->contacts.begin()+indStart,
						myWorld->contacts.begin()+indEnd
					)
				)
			);
		}

		for(auto& future : completionFutures) future.wait();
	}
}

GJKEPAGenerator::GJKEPAGenerator(const ObjectConstructionData& ocd) : ContactGenerator(ocd) {
	procItems_SizeOverTime_insertInd = 0;
	for(auto& item : procItems_SizeOverTime) item = 0;

	newContactsThisFrame_SizeOverTime_insertInd = 0;
	for(auto& item : newContactsThisFrame_SizeOverTime) item = 0;
}

GJKEPAGenerator::~GJKEPAGenerator() {
}

void GJKEPAGenerator::frame(World* world) {
	__super::frame(world);

	updatePersistentContacts();

	//////////////////////////////////////////////////////////////////////////
	// GENERATE NEW CONTACTS

	// try to avoid unnecessary reallocation of the procItems vector
	procItems_SizeOverTime[procItems_SizeOverTime_insertInd++ % procItems_SizeOverTime.size()] = procItems.size();
	const uint32 averageNumProcItemsOverLastFewFrames = std::accumulate(procItems_SizeOverTime.begin(),procItems_SizeOverTime.end(),0,std::plus<uint32>()) / procItems_SizeOverTime.size();
	// build a list of tests to perform
	// TODO: implement octree culling
	procItems.clear();
	procItems.reserve((uint32)(averageNumProcItemsOverLastFewFrames * 1.25f));
	//procItems.reserve(N_CHOOSE_R(allColliderComponents.size(),2));

	newContactsThisFrame_SizeOverTime[newContactsThisFrame_SizeOverTime_insertInd++ % newContactsThisFrame_SizeOverTime.size()] = newContactsThisFrame_SizeOverTime.size();
	const uint32 averageNumNewContactsOverLastFewFrames = std::accumulate(newContactsThisFrame_SizeOverTime.begin(),newContactsThisFrame_SizeOverTime.end(),0,std::plus<uint32>()) / newContactsThisFrame_SizeOverTime.size();
	newContactsThisFrame.clear();
	newContactsThisFrame.reserve((uint32)(averageNumNewContactsOverLastFewFrames * 1.25f));

	// add all the root-to-root collision tests to the processing queue
	for(uint32 i = 0; i < collidersToConsider.size(); i++) {
		ColliderComponent* const colliderA = collidersToConsider[i];
		for(uint32 j = i+1; j < collidersToConsider.size(); j++) {
			ColliderComponent* const colliderB = collidersToConsider[j];

			// are both colliders dynamic, but asleep?
			if(!(colliderA->isDynamic() && colliderA->rigidBody->isAwake()) &&
			   !(colliderB->isDynamic() && colliderB->rigidBody->isAwake())) continue; // ignore collisions where neither party is dynamic and not asleep

			// is collision between the two collider's layers disabled?
			if(!lat_physics.collisionMatrix.isCollisionEnabled(colliderA->collisionLayer,colliderB->collisionLayer)) continue;

			procItems.emplace_back();
			auto& newItem = procItems.back();
			newItem.colliders[0] = colliderA;
			newItem.colliders[1] = colliderB;
		}
	}

	const uint32 numItems = (uint32)procItems.size();
	const uint32 numWorkers = std::min(lat_system.threadSpooler.getOptiomalNumConcurrentTasks(),numItems);
	std::vector<std::future<void>> completionFutures; completionFutures.reserve(numWorkers);

	// split the tests that are to be performed among all available worker threads
	if(numItems > 0) {
		const uint32 div = numItems/numWorkers;
		const uint32 mod = numItems%numWorkers;
		uint32 curIndex = 0;
		for(uint32 i = 0; i < numWorkers; i++) {
			const uint32 thisBlockSize = div + (i < mod ? 1 : 0);
			const uint32 indStart = curIndex;
			const uint32 indEnd = curIndex + thisBlockSize;
			curIndex += thisBlockSize;

			// add the tests between indStart and indEnd to ThreadSpooler's work queue
			// and store the std::future so we can wait for it to be completed along with the rest of the work segments
			completionFutures.emplace_back(
				lat_system.threadSpooler.enqueueWork(
					std::bind(
						&GJKEPAGenerator::processCollisionTests,
						this,procItems.begin()+indStart,
						procItems.begin()+indEnd
					)
				)
			);
		}
	}

	// wait for all collision testing threads to finish
	for(auto& future : completionFutures) future.wait();

	// once all the tests are performed, collate all of the found contacts and add them to the world's contact array
	for(const auto& testData : newContactsThisFrame) {
		// check to see if there is an existing contact that is already too similar to this new one
		// if so, remove the old one and keep the new one

		const auto* colliderA = testData.colliders[0];
		const auto* colliderB = testData.colliders[1];
		const auto* colliderToCheck =
			colliderA->collision_resolution.contactInvolvement.size() <
			colliderB->collision_resolution.contactInvolvement.size() ? colliderA : colliderB;

		for(auto& contact : colliderToCheck->collision_resolution.contactInvolvement) {
			if(!contact->isValid()) continue;

			if((contact->colliders[0] == colliderA && contact->colliders[1] == colliderB) ||
				(contact->colliders[0] == colliderB && contact->colliders[1] == colliderA)) {
				if((contact->normal | testData.normal) > 0.95f) {
					if((contact->point - testData.point).Length() < 5.0f) {
						// the new contact will replace this old one, remove it
						contact->invalidate();
						break;
					}
				}
			}
		}

		auto newit = myWorld->contacts.emplace(std::move(testData));
	}
}