#pragma once

#include "ContactGenerator.h"

#include "physics/Contact.h"

#include <vector>
#include <array>

#include "core/SpinLock.h"

class World;
class ColliderComponent;
class Contact;
class CollisionVolumeNode;

class GJKEPAGenerator : public ContactGenerator {
private:
	struct ProcItemData {
		ColliderComponent* colliders[2];
	};
	typedef std::vector<ProcItemData> ProcItemsCont;

	ProcItemsCont procItems;
	std::array<uint32,10> procItems_SizeOverTime;
	uint32 procItems_SizeOverTime_insertInd;

	std::vector<ContactBasicData> newContactsThisFrame;
	std::array<uint32,10> newContactsThisFrame_SizeOverTime;
	uint32 newContactsThisFrame_SizeOverTime_insertInd;
	
	ht_spinlock spinlock_newContactsThisFrame;

	void processCollisionTests(ProcItemsCont::iterator itBegin,ProcItemsCont::iterator itEnd);

	struct SupportPoint {
		Vector3 v; // WORLD SPACE
		Vector3 localSupports[2];

		uint32 uniqueId;

		// id is only used in the expanding polytope algorithm while generating edges
		inline SupportPoint(uint32 uniqueId = 0) : uniqueId(uniqueId) {}
	};

	struct Simplex {
	public:
		SupportPoint _simplex[4];
		int num;
		SupportPoint &a;
		SupportPoint &b;
		SupportPoint &c;
		SupportPoint &d;

		inline Simplex() : a(_simplex[0]),b(_simplex[1]),c(_simplex[2]),d(_simplex[3]) { clear(); }

		inline void clear() { num = 0; }

		inline void set(SupportPoint a,SupportPoint b,SupportPoint c,SupportPoint d) { num = 4; this->a = a; this->b = b; this->c = c; this->d = d; }
		inline void set(SupportPoint a,SupportPoint b,SupportPoint c) { num = 3; this->a = a; this->b = b; this->c = c; }
		inline void set(SupportPoint a,SupportPoint b) { num = 2; this->a = a; this->b = b; }
		inline void set(SupportPoint a) { num = 1; this->a = a; }

		inline void push(SupportPoint p) { num = (std::min)(num+1,4); for(int i = num-1; i > 0; i--) _simplex[i] = _simplex[i-1]; _simplex[0] = p; }
	};

	struct Triangle {
		SupportPoint points[3];
		Vector3 n;

		inline Triangle(const SupportPoint &a,const SupportPoint &b,const SupportPoint &c) {
			points[0] = a;
			points[1] = b;
			points[2] = c;
			n = getNormal((b.v-a.v) % (c.v-a.v));
		}
	};
	struct Edge {
		SupportPoint points[2];

		inline Edge(const SupportPoint &a,const SupportPoint &b) {
			points[0] = a;
			points[1] = b;
		}
	};

	static bool extrapolateContactInformation(ContactBasicData* contactData);
	static bool extrapolateContactInformation(const Triangle* triangle,
											  ContactBasicData* contactData);

	static SupportPoint generateSupport(Vector3 directionWorld,const CollisionVolumeNode* volumeA,const CollisionVolumeNode* volumeB,uint32* uniqueIdCounter = 0);

	static bool performCoarseIntersectionTest(const CollisionVolumeNode* volumeA,const CollisionVolumeNode* volumeB);
	static bool performGJKIntersectionTest(Simplex* inout_simplex,ContactBasicData* in_contactData,uint32* uniqueIdCounter);
	static bool performEPAManifoldExtraction(const Simplex* in_initialSimplex,ContactBasicData* inout_contactData,uint32* uniqueIdCounter);

	void updatePersistentContacts();

public:
	GJKEPAGenerator(const ObjectConstructionData& ocd);
	virtual ~GJKEPAGenerator();

	virtual void frame(World* world);
};