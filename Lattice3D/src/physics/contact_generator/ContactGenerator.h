#pragma once

#include "core/Engine.h"
#include "core/LatObject.h"

#include <vector>

class World;

class ContactGenerator : public LatObject {
protected:
	World* myWorld;
	std::vector<class ColliderComponent*> collidersToConsider;

	virtual void refreshColliderList();

public:
	ContactGenerator(const ObjectConstructionData& ocd);
	virtual ~ContactGenerator();

	virtual void frame(World* world);
};