#include "Contact.h"

#include "collider/ColliderComponent.h"
#include "material/PhysicalMaterial.h"
#include "RigidBodyComponent.h"

#include <algorithm>

Contact::Contact(const ContactBasicData& data) : ContactBasicData(data) {
	invalidFlag = false;
	derivedDataCalculated = false;

	physicsFrameGenesis = lat_system.activeWorld->getPhysicsFrameNumber();
	colliderInvolvementIterators[0] = colliders[0]->collision_resolution.contactInvolvement.insert(this);
	colliderInvolvementIterators[1] = colliders[1]->collision_resolution.contactInvolvement.insert(this);

	initialContactPoint = point;
}

Contact::Contact(Contact&& other) {
	invalidFlag = other.invalidFlag;
	other.invalidFlag = true;

	point = other.point;
	normal = other.normal;
	penetration = other.penetration;
	colliders[0] = other.colliders[0]; other.colliders[0] = nullptr;
	colliders[1] = other.colliders[1]; other.colliders[1] = nullptr;
	collisionVolumeNodes[0] = other.collisionVolumeNodes[0]; other.collisionVolumeNodes[0] = nullptr;
	collisionVolumeNodes[1] = other.collisionVolumeNodes[1]; other.collisionVolumeNodes[1] = nullptr;

	initialContactPoint = other.initialContactPoint;

	matContactToWorld = other.matContactToWorld;
	matWorldToContact = other.matWorldToContact;
	closingVelocity_world = other.closingVelocity_world;
	closingVelocity_contact = other.closingVelocity_contact;
	relativeContactPosition[0] = other.relativeContactPosition[0];
	relativeContactPosition[1] = other.relativeContactPosition[1];
	coefRestitution = other.coefRestitution;
	coefFriction = other.coefFriction;

	resolution_impulse = other.resolution_impulse;

	derivedDataCalculated = other.derivedDataCalculated;
	physicsFrameGenesis = other.physicsFrameGenesis;

	colliderInvolvementIterators[0] = other.colliderInvolvementIterators[0];
	colliderInvolvementIterators[1] = other.colliderInvolvementIterators[1];

	for(uint8 i = 0; i < 2; i++)
		for(uint8 j = 0; j < 3; j++)
			triangleSupports_local[i][j] = other.triangleSupports_local[i][j];

	for(uint8 i = 0; i < 2; i++) {
		if(colliders[i] != nullptr && colliders[i]->isValid()) {
			assert(*colliderInvolvementIterators[i] == &other);
			*colliderInvolvementIterators[i] = this;
		}
	}

	other.colliders[0] = other.colliders[1] = {};
	other.colliderInvolvementIterators[0] = other.colliderInvolvementIterators[1] = {};
}

Contact::~Contact() {
	for(uint8 i = 0; i < 2; i++) {
		if(colliders[i] != nullptr) {
			colliders[i]->collision_resolution.contactInvolvement.remove(colliderInvolvementIterators[i]);
		}
	}
}

uint32 Contact::getFrameAge() const {
	return uint32(lat_system.activeWorld->getPhysicsFrameNumber() - physicsFrameGenesis);
}

bool Contact::isValid() const {
	if(invalidFlag) return false;

	for(uint8 i = 0; i < 2; i++) {
		if(!(colliders[i] != nullptr &&
			 colliders[i]->isValid() &&
			 colliders[i]->physicalMaterial != nullptr &&
			 collisionVolumeNodes[i] != nullptr &&
			 collisionVolumeNodes[i]->getCollider() == colliders[i])) return false;
	}

	return true;
}

void Contact::invalidate() {
	invalidFlag = true;
}

void Contact::invert() {
	normal *= -1.0f;
	point += normal * penetration;
	std::swap(colliders[0],colliders[1]);
	std::swap(collisionVolumeNodes[0],collisionVolumeNodes[1]);
	for(uint8 i = 0; i < 3; i++) {
		std::swap(triangleSupports_local[0][i],triangleSupports_local[1][i]);
	}
	std::swap(colliderInvolvementIterators[0],colliderInvolvementIterators[1]);

	if(derivedDataCalculated) calculateDerivedData();
}

void Contact::calculateDerivedData() {
	assert(isValid());

	matContactToWorld = Matrix::Identity;
	matContactToWorld.Right(normal);
	MathUtilities::orthoNormalize(&matContactToWorld);
	matContactToWorld.Transpose(matWorldToContact);

	const float restitutionLow = (std::min)(colliders[0]->physicalMaterial->restitution,colliders[1]->physicalMaterial->restitution);
	const float restitutionHigh = (std::max)(colliders[0]->physicalMaterial->restitution,colliders[1]->physicalMaterial->restitution);

	const float frictionLow = (std::min)(colliders[0]->physicalMaterial->friction,colliders[1]->physicalMaterial->friction);
	const float frictionHigh = (std::max)(colliders[0]->physicalMaterial->friction,colliders[1]->physicalMaterial->friction);

	const float targetRestitution = lerp(restitutionLow,restitutionHigh,0.15f);
	const float targetFriction = lerp(frictionLow,frictionHigh,0.15f);

	// initialize values
	closingVelocity_world = Vector3::Zero;
	closingVelocity_contact = Vector3::Zero;

	// calculate derived data
	for(uint8 i = 0; i < 2; i++) {
		relativeContactPosition[i] = point - colliders[i]->centerOfMass_world;
		
		const RigidBodyComponent* rigidBody = colliders[i]->rigidBody;
		if(rigidBody == nullptr) return;

		const Vector3& bodyLinearVelocity = rigidBody->velocity;
		const Vector3& bodyAngularVelocity = rigidBody->angularVelocity;
		const Vector3& lastFrameAcceleration = rigidBody->getLastFrameAcceleration();
		const Vector3& lastFramePlanarAcceleration = lastFrameAcceleration - ((lastFrameAcceleration|normal) * normal);
		const Vector3& localVelocity_world =
			(bodyAngularVelocity % relativeContactPosition[i]) +
			bodyLinearVelocity +
			lastFramePlanarAcceleration;

		const float sign = (i == 0 ? -1.0f : 1.0f);

		closingVelocity_world += sign * localVelocity_world;
	}

	closingVelocity_contact = closingVelocity_world * matWorldToContact;

	coefRestitution = targetRestitution;
	coefFriction = targetFriction;

	derivedDataCalculated = true;
}

bool Contact::needsResolution() const {
	return penetration > 0;
}
