#pragma once

#include "core/EngineTypes.h"

#include "core/EngineResource.h"
#include "core/JSON.h"

class PhysicalMaterial : public EngineResource<PhysicalMaterial> {
	friend class EngineResource<PhysicalMaterial>;

private:
	static constexpr const engine_cstring _autoNameFormat = STR("Unnamed PhysicalMaterial %i");

	PhysicalMaterial(const engine_string& name);
	~PhysicalMaterial();

public:
	float restitution;
	float friction;

	static std::shared_ptr<PhysicalMaterial> createFromData(const engine_string& name,
															float restitution,
															float friction,
															NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);

	static std::shared_ptr<PhysicalMaterial> createFromJSON(const json &jsonKey,
															NameCollisionMode nameCollisionMode = IEngineResource::NameCollisionMode::ERNCM_RETURN_EXISTING);
};
