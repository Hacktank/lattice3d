#include "PhysicalMaterial.h"

PhysicalMaterial::PhysicalMaterial(const engine_string& name) :
	EngineResource(name) {
}

PhysicalMaterial::~PhysicalMaterial() {
}

std::shared_ptr<PhysicalMaterial> PhysicalMaterial::createFromData(const engine_string& name,float restitution,float friction,
																   NameCollisionMode nameCollisionMode) {
	auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,0);
	if(newResource->areInternalsValid) return newResource;

	newResource->restitution = restitution;
	newResource->friction = friction;

	newResource->areInternalsValid = true;
	return newResource;
}

std::shared_ptr<PhysicalMaterial> PhysicalMaterial::createFromJSON(const json &jsonKey,
																   NameCollisionMode nameCollisionMode) {
	try {
		const engine_string name = string_from_normal(jsonKey["name"].as<string_normal>());

		auto newResource = factoryGetTargetResourceHandle(name.length()>0 ? name : generateAutoName(_autoNameFormat),nameCollisionMode,0);
		if(newResource->areInternalsValid) return newResource;

		newResource->restitution = (float)jsonKey["restitution"].as<double>();
		newResource->friction = (float)jsonKey["friction"].as<double>();

		newResource->areInternalsValid = true;
		return newResource;
	} catch(const std::exception &e) {
		std::cerr << e.what() << '\n';
		return nullptr;
	}
}