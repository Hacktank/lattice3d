#pragma once

#include "ContactResolver.h"

#include <vector>
#include <list>

class Contact;

class ImpulseContactResolver : public ContactResolver {
protected:
	typedef std::vector<Contact*> ProcItemsCont;

	std::list<std::list<Contact*>> contactClusters;

	float calculateDesiredDeltaVelocity(Contact* contact);
	Vector3 calculateImpulse(Contact* contact);

	void buildContactClusters();
	void resolveContactCluster(ProcItemsCont &cluster);
public:
	ImpulseContactResolver(const ObjectConstructionData& ocd);
	virtual ~ImpulseContactResolver();

	virtual void frame(World* world);
};
