#include "ImpulseContactResolver.h"

#include "physics/RigidBodyComponent.h"
#include "physics/collider/ColliderComponent.h"

#include "core/World.h"

#include <algorithm>

float ImpulseContactResolver::calculateDesiredDeltaVelocity(Contact* contact) {
	float velocityFromAcceleration = 0;

	for(uint8 i = 0; i < 2; i++) {
		const RigidBodyComponent* rigidBody = contact->colliders[i]->rigidBody;
		const Vector3& bodyLastFrameAcceleration = (rigidBody != nullptr ? (rigidBody->getLastFrameAcceleration()) : (Vector3::Zero));

		const float sign = (i == 0 ? -1.0f : 1.0f);
		velocityFromAcceleration += sign * bodyLastFrameAcceleration | contact->normal;
	}

	const float effectiveRestitution = contact->closingVelocity_contact.x - velocityFromAcceleration >= lat_physics.minimumVelocityForRestitution ? contact->coefRestitution : 0.0f;

	return -contact->closingVelocity_contact.x - (effectiveRestitution * (contact->closingVelocity_contact.x - velocityFromAcceleration));
}

Vector3 ImpulseContactResolver::calculateImpulse(Contact* contact) {
	// accumulators
	float totalMass_inverse = 0;
	Matrix rotationalInertia_inv_world(0,0,0,0,
									   0,0,0,0,
									   0,0,0,0,
									   0,0,0,0);

	// accumulate data
	for(uint8 i = 0; i < 2; i++) {
		if(contact->colliders[i]->inverseMass == 0) continue;

		const Matrix impulseToTorque = skewSymmetric(contact->relativeContactPosition[i]); // matrix form of a vector cross-product
		Matrix myRotationalInertia_inv_local = -1.0f * impulseToTorque * contact->colliders[i]->inertialTensor_inverse * impulseToTorque;

		rotationalInertia_inv_world += myRotationalInertia_inv_local;

		totalMass_inverse += contact->colliders[i]->inverseMass;
	}

	rotationalInertia_inv_world._44 = -1;

	// currently represents the change in velocity due to rotation only
	Matrix totalInertia_inv_contact = contact->matContactToWorld * rotationalInertia_inv_world * contact->matWorldToContact;
	// include the change due to linear motion
	totalInertia_inv_contact._11 += totalMass_inverse;
	totalInertia_inv_contact._22 += totalMass_inverse;
	totalInertia_inv_contact._33 += totalMass_inverse;

	const Matrix impulsePerUnitVelocity = totalInertia_inv_contact.Invert();

	// find the velocity to kill
	const Vector3 velocityToKill(contact->resolution_impulse.desiredDeltaVelocity,
								 -contact->closingVelocity_contact.y,
								 -contact->closingVelocity_contact.z);

	// find the impulse required to kill this velocity
	Vector3 impulse_contact = velocityToKill * impulsePerUnitVelocity;

	// check to see if our tangential impulse is large enough to overcome static friction
	const float planarImpulse = sqrt(SQ(impulse_contact.y) + SQ(impulse_contact.z));
	if(planarImpulse > impulse_contact.x * contact->coefFriction) {
		// it is, calculate impulse with dynamic friction
		impulse_contact.y /= planarImpulse;
		impulse_contact.z /= planarImpulse;
		impulse_contact.x =
			totalInertia_inv_contact._11 +
			totalInertia_inv_contact._12 * contact->coefFriction * impulse_contact.y +
			totalInertia_inv_contact._13 * contact->coefFriction * impulse_contact.z;

		impulse_contact.x = contact->resolution_impulse.desiredDeltaVelocity / impulse_contact.x;
		impulse_contact.y *= contact->coefFriction * impulse_contact.x;
		impulse_contact.z *= contact->coefFriction * impulse_contact.x;

		impulse_contact *= -1;
	}

	return impulse_contact * contact->matContactToWorld;
}

void ImpulseContactResolver::resolveContactCluster(ProcItemsCont &cluster) {
	// TODO: resolve contacts in the order from their distance from a static object and from the direction of gravity
	// TODO: implement "weak welds" for contacts with static objects

	uint32 maxPenetrationIterations = lat_physics.solver.maximumPenetrationIterationsBase + uint32(round(lat_physics.solver.maximumPenetrationIterationsPerContactInCluster * cluster.size()));
	uint32 maxVelocityIterations = lat_physics.solver.maximumVelocityIterationsBase + uint32(round(lat_physics.solver.maximumVelocityIterationsPerContactInCluster * cluster.size()));

	uint32 numPenetrationIterationsUsed = 0;
	uint32 numVelocityIterationsUsed = 0;

	// calculate the internal information required for the penetration and velocity iterations
	for(auto& contact : cluster) {
		// ensure all rigid bodies are woken up
		for(uint8 i = 0; i < 2; i++) {
			auto* rigidBody = contact->colliders[i]->rigidBody;
			if(rigidBody != nullptr) rigidBody->awake = true;
		}

		contact->calculateDerivedData();
		contact->resolution_impulse.desiredDeltaVelocity = calculateDesiredDeltaVelocity(contact);
	}

	const float penetrationPercentToFixEachIteration = 1.0f;
	//const float penetrationPercentToFixEachIteration = pow(1.0f / float(cluster.size()),0.2f);
	//printf("penetrationPercentToFixEachIteration=%f\n",penetrationPercentToFixEachIteration);

	// first fix the inter-penetration
	for(numPenetrationIterationsUsed = 0; numPenetrationIterationsUsed < maxPenetrationIterations; numPenetrationIterationsUsed++) {
		// find the contact with the highest penetration
		Contact* contactWithHighestePenetration = nullptr;
		for(auto &contact : cluster) {
			if(contactWithHighestePenetration == nullptr || contact->penetration > contactWithHighestePenetration->penetration) {
				contactWithHighestePenetration = contact;
			}
		}

		// if a contact with a large enough penetration was not found, we are done
		if(contactWithHighestePenetration == nullptr || contactWithHighestePenetration->penetration <= lat_physics.solver.epsilon_penetration) {
			numPenetrationIterationsUsed++;
			break;
		}

		; {
			float totalInertia = 0;
			float linearInertia[2] = {0,0};
			float angularInertia[2] = {0,0};

			// calculate inertia of objects in normal direction
			for(uint8 i = 0; i < 2; i++) {
				auto* rigidBody = contactWithHighestePenetration->colliders[i]->rigidBody;
				auto* collider = contactWithHighestePenetration->colliders[i];
				if(rigidBody == nullptr ||
				   collider->isStatic()) continue;

				Vector3 angularInertiaWorld = contactWithHighestePenetration->relativeContactPosition[i] % contactWithHighestePenetration->normal;
				angularInertiaWorld = angularInertiaWorld * collider->inertialTensor_inverse;
				angularInertiaWorld = angularInertiaWorld % contactWithHighestePenetration->relativeContactPosition[i];
				angularInertia[i] = angularInertiaWorld | contactWithHighestePenetration->normal;

				linearInertia[i] = contactWithHighestePenetration->colliders[i]->inverseMass;

				totalInertia += linearInertia[i] + angularInertia[i];
			}

			for(uint8 i = 0; i < 2; i++) {
				auto* collider = contactWithHighestePenetration->colliders[i];
				if(collider->isStatic()) continue;

				auto* rigidBody = collider->rigidBody;

				const float sign = (float)(i==0 ? 1 : -1);
				float linearMovementMagnitude = penetrationPercentToFixEachIteration * sign * contactWithHighestePenetration->penetration * (linearInertia[i] / totalInertia);
				float angularMovementMagnitude = penetrationPercentToFixEachIteration * sign * contactWithHighestePenetration->penetration * (angularInertia[i] / totalInertia);

				// limit angular movement to keep high mass/low angular inertia objects from behaving strangely
				const Vector3 relativeContactPositionInNormalDirection =
					contactWithHighestePenetration->relativeContactPosition[i] +
					(contactWithHighestePenetration->normal * -(contactWithHighestePenetration->relativeContactPosition[i] | contactWithHighestePenetration->normal));

				float angularMovementLimit = 0.2f * relativeContactPositionInNormalDirection.Length();

				if(abs(angularMovementMagnitude) > angularMovementLimit) {
					float totalMove = linearMovementMagnitude + angularMovementMagnitude;
					angularMovementMagnitude = clamp(angularMovementMagnitude,-angularMovementLimit,angularMovementLimit);
					linearMovementMagnitude = totalMove - angularMovementMagnitude;
				}

				// calculate linear and angular deltas
				Vector3 linearDelta = contactWithHighestePenetration->normal * linearMovementMagnitude;
				Vector3 angularDelta = Vector3::Zero;
				if(angularMovementMagnitude != 0) {
					const Vector3 rotationDirection = contactWithHighestePenetration->relativeContactPosition[i] % contactWithHighestePenetration->normal;
					angularDelta = (rotationDirection * collider->inertialTensor_inverse) * (angularMovementMagnitude / angularInertia[i]);
				}

				// apply the changes
				rigidBody->transform.position += linearDelta;
				rigidBody->transform.rotation = MathUtilities::quatAddAngularOffset(rigidBody->transform.rotation,angularDelta);
				rigidBody->transform.rotation.Normalize();

				// propagate the changes among all effected contacts
				for(auto& contact : collider->collision_resolution.contactInvolvement) {
					if(!contact->isValid()) continue;

					const uint8 colliderIndInOtherContact = contact->colliders[0] == collider ? 0 : 1;
					const float sign = float(colliderIndInOtherContact==0 ? -1 : 1);

					const Vector3 deltaPosition = (linearDelta + (angularDelta % contact->relativeContactPosition[colliderIndInOtherContact]));
					contact->penetration += sign * (deltaPosition | contact->normal);

					contact->relativeContactPosition[1] += deltaPosition * sign;

					if(colliderIndInOtherContact == 0) {
						contact->point += deltaPosition;
						//contact->calculateDerivedData();// may or may not be necessary down the line
					}
				}
			}
		}
	}

	// fix velocities
	for(numVelocityIterationsUsed = 0; numVelocityIterationsUsed < maxVelocityIterations; numVelocityIterationsUsed++) {
		// find the contact with the lowest contact velocity
		Contact* contactWithHighestDesiredDeltaVelocity = nullptr;
		for(auto& contact : cluster) {
			if(contact->penetration < -0.1f) continue;

			if(contactWithHighestDesiredDeltaVelocity == nullptr || contact->resolution_impulse.desiredDeltaVelocity < contactWithHighestDesiredDeltaVelocity->resolution_impulse.desiredDeltaVelocity) {
				contactWithHighestDesiredDeltaVelocity = contact;
			}
		}

		// if a contact with a large enough penetration was not found, we are done
		if(contactWithHighestDesiredDeltaVelocity == nullptr || contactWithHighestDesiredDeltaVelocity->resolution_impulse.desiredDeltaVelocity >= -lat_physics.solver.epsilon_velocity) {
			numVelocityIterationsUsed++;
			break;
		}

		const Vector3 impulse = calculateImpulse(contactWithHighestDesiredDeltaVelocity);

		for(uint8 i = 0; i < 2; i++) {
			auto* collider = contactWithHighestDesiredDeltaVelocity->colliders[i];
			if(collider->isStatic()) continue;

			auto* rigidBody = collider->rigidBody;

			const float sign = (i == 0 ? 1.0f : -1.0f);

			float velyabs = fabs(rigidBody->velocity.y);
			const Vector3 velDelta = sign * (impulse * collider->inverseMass);
			//printf("vel={%5.1f, %5.1f, %5.1f}  velDelta={%5.1f, %5.1f, %5.1f}\n",VEC3TOARG(rigidBody->velocity),VEC3TOARG(velDelta));
			rigidBody->velocity += velDelta;

			const Vector3 avelDelta = sign * ((contactWithHighestDesiredDeltaVelocity->relativeContactPosition[i] % impulse) * collider->inertialTensor_inverse);
			//printf("velDelta={%7.2f, %7.2f, %7.2f}  avelDelta={%7.2f, %7.2f, %7.2f}\n",VEC3TOARG(velDelta),VEC3TOARG(avelDelta));
			rigidBody->angularVelocity += avelDelta;

			// propagate the changes among all effected contacts
			for(auto& contact : collider->collision_resolution.contactInvolvement) {
				if(!contact->isValid()) continue;

				const uint8 colliderIndInOtherContact = contact->colliders[0] == collider ? 0 : 1;
				const float sign = float(colliderIndInOtherContact==0 ? -1 : 1);

				const Vector3 delta = velDelta + (avelDelta % contact->relativeContactPosition[colliderIndInOtherContact]);
				contact->closingVelocity_world += sign * delta;
				contact->closingVelocity_contact = contact->closingVelocity_world * contact->matWorldToContact;
				contact->resolution_impulse.desiredDeltaVelocity = calculateDesiredDeltaVelocity(contact);
			}
		}
	}

	/*printf("ImpulseContactResolution:\n"
		   "  physFrame=%10I64u  physTime=%.5f  physTimeDelta=%.5f\n"
		   "  clusterSize=%3i  penItr=%2i/%-2i  velItr=%2i/%-2i\n",
		   myWorld->getPhysicsFrameNumber(),
		   (float)myWorld->physicsTime.count(),
		   (float)myWorld->physicsFrameTimeDelta.count(),
		   cluster.size(),
		   numPenetrationIterationsUsed,maxPenetrationIterations,
		   numVelocityIterationsUsed,maxVelocityIterations);*/
}

void ImpulseContactResolver::buildContactClusters() {
	contactClusters.clear();

	// clear runtime data
	for(auto& contact : myWorld->contacts) {
		if(!contact.isValid()) continue;

		for(uint8 i = 0; i < 2; i++) {
			contact.colliders[i]->collision_resolution.myContactCluster = nullptr;
		}
	}

	// build the contact clusters
	for(auto& contact : myWorld->contacts) {
		if(!contact.isValid()) continue;

		for(uint8 i = 0; i < 2; i++) {
			// if either collider's cluster is null, and it is dynamic, a new cluster is created and assigned
			if(contact.colliders[i]->collision_resolution.myContactCluster == nullptr && contact.colliders[i]->isDynamic()) {
				contactClusters.emplace_back();
				contact.colliders[i]->collision_resolution.myContactCluster = &contactClusters.back();
			}
		}

		ColliderComponent* const colliderA = contact.colliders[0];
		ColliderComponent* const colliderB = contact.colliders[1];

		std::list<Contact*>* clusterA = reinterpret_cast<std::list<Contact*>*>(colliderA->collision_resolution.myContactCluster);
		std::list<Contact*>* clusterB = reinterpret_cast<std::list<Contact*>*>(colliderB->collision_resolution.myContactCluster);

		if(clusterA == nullptr || clusterB == nullptr) {
			// at least one of the colliders is static, thus no splicing will occur
			if(clusterA != nullptr) clusterA->push_back(&contact);
			if(clusterB != nullptr) clusterB->push_back(&contact);
		} else {
			if(clusterA != clusterB) {
				// both colliders are dynamic and in separate clusters, merge both clusters
				clusterA->splice(clusterA->end(),*clusterB);
				colliderB->collision_resolution.myContactCluster = clusterB = clusterA;
			}
			clusterA->push_back(&contact);
		}
	}

	// remove all empty clusters
	contactClusters.remove_if([](const std::list<Contact*> &item)->bool {
		return item.size() == 0;
	});
}

ImpulseContactResolver::ImpulseContactResolver(const ObjectConstructionData& ocd) : ContactResolver(ocd) {
}

ImpulseContactResolver::~ImpulseContactResolver() {
}

void ImpulseContactResolver::frame(World* world) {
	__super::frame(world);

	buildContactClusters();

	// make a separate thread task for every contact cluster
	// TODO: distribute the contact clusters around into a more optimal number of
	//       work orders based on the number of contacts in each one

	for(auto &cluster : contactClusters) {
		ProcItemsCont procItems(cluster.begin(),cluster.end());
		auto completionFuture = lat_system.threadSpooler.enqueueWork(std::bind(&ImpulseContactResolver::resolveContactCluster,this,std::move(procItems)));

		completionFuture.wait();
	}
}