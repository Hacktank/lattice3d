#pragma once

#include "core/ThreadSpooler.h"
#include "core/LatObject.h"

#include <vector>

class World;

class ContactResolver : public LatObject {
protected:
	World* myWorld;

public:
	ContactResolver(const ObjectConstructionData& ocd);
	virtual ~ContactResolver();

	virtual void frame(World* world);
};
