#include "ContactResolver.h"

ContactResolver::ContactResolver(const ObjectConstructionData& ocd) : LatObject(ocd) {
	myWorld = nullptr;
}

ContactResolver::~ContactResolver() {
}

void ContactResolver::frame(World* world) {
	myWorld = world;
}