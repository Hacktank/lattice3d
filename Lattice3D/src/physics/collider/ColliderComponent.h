#pragma once

#include "core/ActorComponent.h"
#include "core/Engine.h"

#include "core/Tickable.h"

#include "graphics/Renderable.h"


class PhysicalMaterial;
class RigidBodyComponent;

class CollisionVolumeNode {
private:
	class ColliderComponent* myColliderComponent;
	typedef std::vector<CollisionVolumeNode*> ChildContainer_t;

	CollisionVolumeNode* parentNode;
	ChildContainer_t childNodes;

public:
	template<typename T,typename ...Args>
	T* createChild(Args&&... args);

	struct VolumeConstructionData {
		template<typename T,typename ...Args>
		friend T* CollisionVolumeNode::createChild(Args&&... args);
		friend class ColliderComponent;

	public:
		CollisionVolumeNode* parent;
		ColliderComponent* colliderComponent;

	private:
		VolumeConstructionData(CollisionVolumeNode* parent,
							   ColliderComponent* colliderComponent) :
			parent(parent),
			colliderComponent(colliderComponent) {
		};
	};

	// independent
	Transform offset;
	bool useMassOverride;
	float massOverride;

	// derived
	float mass;
	Vector3 centerOfMass_local;
	Vector3 centerOfMass_world;
	struct { Vector3 min,max; } aabb_world;
	Matrix inertialTensor; // rotation around centerOfMass_world NOT necessarily the local origin

	// COLLISION DETECTION DATA
	struct {
		mutable Matrix mtxColliderToWorld;
		mutable Quaternion quatColliderToWorld_rotation;
		mutable Quaternion quatWorldToCollider_rotation;
		mutable Vector3 vecScale;
	} mutable collision_detection;

	CollisionVolumeNode(const VolumeConstructionData& vcd);

	CollisionVolumeNode* getParent() const { parentNode; }
	const ChildContainer_t& getChildren() const { return childNodes; }
	void destroyAllChildren();

	bool shouldGenerateContacts();

	ColliderComponent* getCollider() const { return myColliderComponent; }
	void destroyNode();

	virtual void recalculateDerivedData();

	virtual Matrix calculateToWorldTransform() const;

	virtual Vector3 getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData = false) const;

protected:
	virtual ~CollisionVolumeNode();

	Vector3 _convertWorldSpaceDirectionToLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData) const;

	float getValidDensity() const;
	float calculateMass(float density,float volume) const;

	// static entities are those that have no mass, or are triggers
	// SHOULD ONLY BE CALLED ON A LEAF NODE
	virtual void recaculateDerivedDataAsStatic() = 0;

	// dynamic entities are all that are not static
	// SHOULD ONLY BE CALLED ON A LEAF NODE
	virtual void recaculateDerivedDataAsDynamic() = 0;

	virtual void updateDebugDraw();
};

template<typename T,typename ...Args>
T* CollisionVolumeNode::createChild(Args&&... args) {
	static_assert(std::is_base_of<CollisionVolumeNode,T>::value,"T must derive from CollisionVolumeNode");
	T *newObject = new T({this,myColliderComponent},std::forward<Args>(args)...);
	return newObject;
}

class CVContainer : public CollisionVolumeNode {
public:
	CVContainer(const VolumeConstructionData& vcd) : CollisionVolumeNode(vcd) {
	}
};

class ColliderComponent : public ActorComponent, public CVContainer, private Tickable, private Renderable {
private:
	CVContainer::offset; // using the Transform of ActorComponent
	CVContainer::destroyNode;
	CVContainer::getCollider;
	CVContainer::getParent;

protected:
	virtual void onInvalidate() override;

public:
	bool isTrigger;
	bool broadcastCollisionEvents;
	uint8 collisionLayer;
	RigidBodyComponent* rigidBody;
	std::shared_ptr<class PhysicalMaterial> physicalMaterial;

	// COLLISION RESOLUTION DATA
	struct {
		void* myContactCluster;
		pool_vector<class Contact*> contactInvolvement;
	} collision_resolution;

	// derived
	float inverseMass;
	Matrix inertialTensor_inverse; // rotation around centerOfMass_world NOT necessarily the local origin

	ColliderComponent(const ObjectConstructionData& ocd);
	virtual ~ColliderComponent();

	bool isStatic() const;
	bool isDynamic() const;

	virtual float getDensity() const;

	virtual Matrix calculateToWorldTransform() const override;
	virtual void recalculateDerivedData() override;

protected:
	virtual void recaculateDerivedDataAsStatic() override {}
	virtual void recaculateDerivedDataAsDynamic() override {}

private:
	void onTick(uint32 groups,engine_duration deltaTime) override;
	void render() override;
};