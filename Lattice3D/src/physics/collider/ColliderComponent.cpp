#include "ColliderComponent.h"

#include "../RigidBodyComponent.h"
#include "physics/material/PhysicalMaterial.h"

#include <algorithm>

#include <bgfx/framework/debugdraw/debugdraw.h>

Vector3 CollisionVolumeNode::_convertWorldSpaceDirectionToLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData) const {
	if(useCachedData) {
		return Vector3::Transform(dirWorldSpace,collision_detection.quatWorldToCollider_rotation).GetNormal();
	} else {
		const Quaternion currentRotation = Quaternion::CreateFromRotationMatrix(MathUtilities::removeScale(calculateToWorldTransform()));
		return Vector3::Transform(dirWorldSpace,currentRotation.Inverse()).GetNormal();
	}
}

float CollisionVolumeNode::getValidDensity() const {
	return getCollider()->rigidBody ? getCollider()->rigidBody->density : 0;
}

float CollisionVolumeNode::calculateMass(float density,float volume) const {
	bool useMassOverride = false;
	float overriddenMass = 0;

	const CollisionVolumeNode* cur = this;
	while(cur) {
		if(cur->useMassOverride) {
			useMassOverride = true;
			overriddenMass = cur->massOverride;
		}
		cur = cur->parentNode;
	}

	if(useMassOverride) return overriddenMass;

	return std::min(volume * density,lat_physics.maximumBodyMass);
}

void CollisionVolumeNode::updateDebugDraw() {
	for(auto& child : childNodes) {
		child->updateDebugDraw();
	}

	if(childNodes.size() != 1) {
		ddPush();
		; {
			ddSetColor(0xff0000ff);
			Aabb bgfxAABB = {VEC3TOARG(aabb_world.min),VEC3TOARG(aabb_world.max)};
			ddDraw(bgfxAABB);
		}
		ddPop();
	}
}

CollisionVolumeNode::CollisionVolumeNode(const VolumeConstructionData& vcd) {
	parentNode = vcd.parent;
	myColliderComponent = vcd.colliderComponent;

	if(parentNode) {
		parentNode->childNodes.push_back(this);
	}

	useMassOverride = false;
	massOverride = 0;
}

CollisionVolumeNode::~CollisionVolumeNode() {
	if(parentNode != nullptr) {
		std::remove(parentNode->childNodes.begin(),parentNode->childNodes.end(),this);
		for(auto& child : childNodes) {
			child->parentNode = parentNode;
		}
		std::copy(childNodes.begin(),childNodes.end(),std::back_inserter(parentNode->childNodes));
	}
}

void CollisionVolumeNode::destroyAllChildren() {
	decltype(childNodes) oldChildNodes;
	oldChildNodes.swap(childNodes);

	for(auto& child : oldChildNodes) {
		child->destroyNode();
	}
}

bool CollisionVolumeNode::shouldGenerateContacts() {
	return childNodes.size() == 0;
}

void CollisionVolumeNode::destroyNode() {
	delete this;
}

Matrix CollisionVolumeNode::calculateToWorldTransform() const {
	const Matrix parentTransform = parentNode != nullptr ? parentNode->calculateToWorldTransform() : Matrix::Identity;
	return parentTransform * offset.getMatrix();
}

DirectX::SimpleMath::Vector3 CollisionVolumeNode::getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData /*= false*/) const {
	const Vector3 actualDirection = _convertWorldSpaceDirectionToLocalSpace(dirWorldSpace,useCachedData);

	Vector3 furthestPoint = {0,0,0};
	float furthestDistance = 0;
	for(auto& child : childNodes) {
		const Vector3& childPoint_local = child->getSupportPointLocalSpace(dirWorldSpace,useCachedData) * child->offset.getMatrix();
		const float currentDistance = childPoint_local | actualDirection;

		if(currentDistance > furthestDistance) {
			furthestDistance = currentDistance;
			furthestPoint = childPoint_local;
		}
	}

	return furthestPoint;
}

void CollisionVolumeNode::recalculateDerivedData() {
	const Matrix& parentTransform = parentNode != nullptr ? parentNode->collision_detection.mtxColliderToWorld : calculateToWorldTransform();
	const Matrix myTransform = offset.getMatrix();

	collision_detection.mtxColliderToWorld = parentTransform * myTransform;
	collision_detection.quatColliderToWorld_rotation = Quaternion::CreateFromRotationMatrix(MathUtilities::removeScale(collision_detection.mtxColliderToWorld,&collision_detection.vecScale));
	collision_detection.quatWorldToCollider_rotation = collision_detection.quatColliderToWorld_rotation.Inverse();

	for(auto& child : childNodes) {
		// recursively call this method for all children - BREDTH FIRST
		child->recalculateDerivedData();
	}

	// if we have any children, we are a 'container' and thus only aggregate the mass properties of those children
	if(childNodes.size() == 0) {
		// calculate our own mass properties
		if(myColliderComponent->getDensity() > 0)
			recaculateDerivedDataAsDynamic();
		else
			recaculateDerivedDataAsStatic();
	} else {
		// recursively calculate derived data and aggregate it - DEPTH FIRST

		mass = 0;
		centerOfMass_world = {0,0,0};

		// calculate overall mass and center of mass
		for(auto& child : childNodes) {
			if(child->mass == 0) break; // infinite mass

			// weighted average, based on mass contribution to total mass
			centerOfMass_world += child->centerOfMass_world * child->mass;
			mass += child->mass;
		}
		if(mass == 0) { // does one of our children have infinite mass?
			// if so, the center of mass should be the average of all infinite mass children
			centerOfMass_world = {0,0,0};
			uint32 numChildrenWithInfiniteMass = 0;
			for(auto& child : childNodes) {
				if(child->mass > 0) break; // finite mass
				numChildrenWithInfiniteMass++;
				centerOfMass_world += child->centerOfMass_world;
			}
			centerOfMass_world /= float(numChildrenWithInfiniteMass);
		} else {
			centerOfMass_world /= mass;
		}

		centerOfMass_local = centerOfMass_world - collision_detection.mtxColliderToWorld.Translation();

		mass = std::min(mass,lat_physics.maximumBodyMass);

		// calculate overall aabb
		aabb_world = childNodes[0]->aabb_world;
		for(uint32 i = 1; i < childNodes.size(); i++) {
			aabb_world.min = minVec3(aabb_world.min,childNodes[i]->aabb_world.min);
			aabb_world.max = maxVec3(aabb_world.max,childNodes[i]->aabb_world.max);
		}

		// calculate overall inertial tensor
		// this is accomplished via the parallel axis theorem
		// J = I[i] + m[i](r.r * Matrix::Identity - rr^T)
		// r is the vector between the child's center of mass and the overall center of mass

		inertialTensor = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};

		for(auto& child : childNodes) {
			if(child->mass == 0) continue; // infinite mass

			const Vector3 r = centerOfMass_world - child->centerOfMass_world;
			const Matrix r_m = Matrix(r,{0,0,0},{0,0,0});
			Matrix& J = inertialTensor;
			Matrix& I = child->inertialTensor;
			const float& m = child->mass;

			Matrix rmt1 = r_m*r_m.Transpose();
			Matrix rmt2 = r_m.Transpose()*r_m;

			J += I + m * ((r|r) * Matrix::Identity - r_m*r_m.Transpose());
		}

		inertialTensor._14 = 0;
		inertialTensor._24 = 0;
		inertialTensor._34 = 0;
		inertialTensor._44 = 1;
	}
}

void ColliderComponent::onInvalidate() {
	__super::onInvalidate();

	for(auto& contact : collision_resolution.contactInvolvement) {
		contact->invalidate();
	}
}

ColliderComponent::ColliderComponent(const ObjectConstructionData& ocd) : ActorComponent(ocd),CVContainer(CollisionVolumeNode::VolumeConstructionData(nullptr,this)) {
	isTrigger = false;
	broadcastCollisionEvents = false;
	collisionLayer = lat_physics.collisionMatrix.getCollisionLayerByName(STR("default"));
	physicalMaterial = PhysicalMaterial::findByName(STR("default"));
	rigidBody = nullptr;

	tickGroup = TickGroup::PRE_PHYSICS;
}

ColliderComponent::~ColliderComponent() {
}

bool ColliderComponent::isStatic() const {
	return rigidBody == nullptr || rigidBody->getCollider() != this || mass == 0;
}

bool ColliderComponent::isDynamic() const {
	return !isStatic();
}

float ColliderComponent::getDensity() const {
	return (rigidBody != nullptr && rigidBody->getCollider() == this) ? rigidBody->density : 0;
}

Matrix ColliderComponent::calculateToWorldTransform() const {
	return getComponentToWorld();
}

void ColliderComponent::recalculateDerivedData() {
	CollisionVolumeNode::recalculateDerivedData();

	if(mass > 0) {
		inverseMass = 1.0f / mass;
		inertialTensor_inverse = inertialTensor.Invert();
	} else {
		inverseMass = 0;
		inertialTensor_inverse = Matrix::Identity;
	}
}

void ColliderComponent::onTick(uint32 groups,engine_duration deltaTime) {
	if(groups & TickGroup::PRE_PHYSICS) recalculateDerivedData();
}

void ColliderComponent::render() {
	bool shouldDrawDebug = lat_physics.debug.debugDrawEnabledGlobal && ((mass==0 && lat_physics.debug.debugDrawEnabledStatic) || (mass>0 && lat_physics.debug.debugDrawEnabledDynamic));

	if(shouldDrawDebug) {
		updateDebugDraw();
	}
}
