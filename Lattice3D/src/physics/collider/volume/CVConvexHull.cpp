#include "CVConvexHull.h"

#include "physics/RigidBodyComponent.h"

#include "core/ConvexHullResource.h"

__impl_CVConvexHull::__impl_CVConvexHull(const VolumeConstructionData& vcd) : CollisionVolumeNode(vcd) {
}

DirectX::SimpleMath::Vector3 __impl_CVConvexHull::getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData /*= false*/) const {
	const Vector3 localDirection = _convertWorldSpaceDirectionToLocalSpace(dirWorldSpace,useCachedData);

	const ConvexHull* myHull = getConvexHull();
	if(myHull->verticies.size() == 0) return {0,0,0};

	//hill-climbing algorithm using the hull's adjacency data:
	//pick an arbitrary vert to start with, then move to the best adjacent
	//one, continue until no better adjacent vert exists

	unsigned cur_vid = 0;
	float cur_dst = myHull->verticies[cur_vid] | localDirection;
	while(true) {
		unsigned new_vid = cur_vid;
		float new_dst = cur_dst;
		; {
			for(unsigned i = 0; i < myHull->adjacency[cur_vid].size(); i++) {
				unsigned vid = myHull->adjacency[cur_vid][i];
				float dst = myHull->verticies[vid] | localDirection;
				if(dst > new_dst) {
					new_vid = vid;
					new_dst = dst;
				}
			}
		}
		if(new_vid == cur_vid) break;
		cur_vid = new_vid;
		cur_dst = new_dst;
	}

	return myHull->verticies[cur_vid];
}

__impl_CVConvexHull::~__impl_CVConvexHull() {
}

void __impl_CVConvexHull::recaculateDerivedDataAsStatic() {
	const ConvexHull* myHull = getConvexHull();

	aabbGetTransformed(myHull->aabb.min,
					   myHull->aabb.max,
					   collision_detection.mtxColliderToWorld,
					   &aabb_world.min,
					   &aabb_world.max);

	mass = 0;
	centerOfMass_local = myHull->volumeData.centroid;
	centerOfMass_world = centerOfMass_local * collision_detection.mtxColliderToWorld;

	inertialTensor = Matrix(
		0,0,0,0,
		0,0,0,0,
		0,0,0,0,
		0,0,0,1
	);
}

void __impl_CVConvexHull::recaculateDerivedDataAsDynamic() {
	// TODO/note: mass and inertial tensor are calculated off the bounding box that encompasses the convex hull
	//            need to implement some means of doing this in a better way
	//            volume calculation: http://clb.demon.fi/MathGeoLib/nightly/docs/Polyhedron.cpp_code.html#283

	const ConvexHull* myHull = getConvexHull();

	aabbGetTransformed(myHull->aabb.min,
					   myHull->aabb.max,
					   collision_detection.mtxColliderToWorld,
					   &aabb_world.min,
					   &aabb_world.max);

	const Vector3 sizes = collision_detection.vecScale * (myHull->aabb.max - myHull->aabb.min);

	const float density = getCollider()->getDensity();
	const float volume = myHull->volumeData.volume * (collision_detection.vecScale|collision_detection.vecScale);
	mass = calculateMass(density,volume);

	centerOfMass_local = myHull->volumeData.centroid;
	centerOfMass_world = centerOfMass_local * collision_detection.mtxColliderToWorld;

	if(mass > 0) {
		inertialTensor = Matrix::Identity;

		const Vector3 squares = sizes * sizes;
		const float s = 1.f/12.f * mass;

		inertialTensor._11 = s * (squares.y + squares.z);
		inertialTensor._22 = s * (squares.x + squares.z);
		inertialTensor._33 = s * (squares.x + squares.y);
	} else {
		inertialTensor = Matrix(
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,1
		);
	}
}

const ConvexHull* CVConvexHullResource::getConvexHull() const {
	return &convexHullResource->data;
}
