#pragma once

#include "physics/collider/ColliderComponent.h"

class CVBox : public CollisionVolumeNode {
public:
	Vector3 halfSize;

	CVBox(const VolumeConstructionData& vcd,const Vector3& halfSize);

	virtual Vector3 getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData = false) const override;

protected:
	virtual ~CVBox();

	virtual void recaculateDerivedDataAsStatic() override;
	virtual void recaculateDerivedDataAsDynamic() override;
};
