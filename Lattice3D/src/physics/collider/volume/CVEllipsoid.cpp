#include "CVEllipsoid.h"

#include "physics/RigidBodyComponent.h"

CVEllipsoid::CVEllipsoid(const VolumeConstructionData& vcd,float radius) : CollisionVolumeNode(vcd),
radii(radius,radius,radius) {
}

CVEllipsoid::CVEllipsoid(const VolumeConstructionData& vcd,const Vector3& radii) : CollisionVolumeNode(vcd),
radii(radii) {
}

DirectX::SimpleMath::Vector3 CVEllipsoid::getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData /*= false*/) const {
	const Vector3 localDirection = _convertWorldSpaceDirectionToLocalSpace(dirWorldSpace,useCachedData);

	return radii*localDirection;
}

CVEllipsoid::~CVEllipsoid() {
}

void CVEllipsoid::recaculateDerivedDataAsStatic() {
	aabbGetTransformed(-radii,
					   radii,
					   collision_detection.mtxColliderToWorld,
					   &aabb_world.min,
					   &aabb_world.max);

	mass = 0;
	centerOfMass_local = {0,0,0};
	centerOfMass_world = centerOfMass_local * collision_detection.mtxColliderToWorld;
	inertialTensor = Matrix(
		0,0,0,0,
		0,0,0,0,
		0,0,0,0,
		0,0,0,1
	);
}

void CVEllipsoid::recaculateDerivedDataAsDynamic() {
	aabbGetTransformed(-radii,
					   radii,
					   collision_detection.mtxColliderToWorld,
					   &aabb_world.min,
					   &aabb_world.max);

	const Vector3 radii_world = radii * collision_detection.vecScale;

	const float density = getCollider()->getDensity();
	const float volume = 4.0f/3.0f * PI * radii_world.x * radii_world.y * radii_world.z;
	mass = calculateMass(density,volume);

	centerOfMass_local = {0,0,0};
	centerOfMass_world = centerOfMass_local * collision_detection.mtxColliderToWorld;

	if(mass > 0) {
		inertialTensor = Matrix::Identity;
		const Vector3 radii_sq = radii_world * radii_world;
		const float s = mass * 0.2f;
		inertialTensor._11 = s * (radii_sq.y + radii_sq.z);
		inertialTensor._22 = s * (radii_sq.x + radii_sq.z);
		inertialTensor._33 = s * (radii_sq.x + radii_sq.y);
	} else {
		inertialTensor = Matrix(
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,1
		);
	}
}