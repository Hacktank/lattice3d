#pragma once

#include "physics/collider/ColliderComponent.h"

class CVEllipsoid : public CollisionVolumeNode {
public:
	Vector3 radii;

	CVEllipsoid(const VolumeConstructionData& vcd,float radius);
	CVEllipsoid(const VolumeConstructionData& vcd,const Vector3& radii);

	virtual Vector3 getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData = false) const override;

protected:
	virtual ~CVEllipsoid();

	virtual void recaculateDerivedDataAsStatic() override;
	virtual void recaculateDerivedDataAsDynamic() override;
};
