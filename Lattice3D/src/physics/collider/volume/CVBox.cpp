#include "CVBox.h"

#include "physics/RigidBodyComponent.h"

CVBox::CVBox(const VolumeConstructionData& vcd,const Vector3& halfSize) : CollisionVolumeNode(vcd),
halfSize(halfSize) {
}

Vector3 CVBox::getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData /*= false*/) const {
	const Vector3 localDirection = _convertWorldSpaceDirectionToLocalSpace(dirWorldSpace,useCachedData);

	const Vector3 extentInDirection(
		SIGN(localDirection.x)*halfSize.x,
		SIGN(localDirection.y)*halfSize.y,
		SIGN(localDirection.z)*halfSize.z);

	return extentInDirection;
}

CVBox::~CVBox() {
}

void CVBox::recaculateDerivedDataAsStatic() {
	aabbGetTransformed(-halfSize,
					   halfSize,
					   collision_detection.mtxColliderToWorld,
					   &aabb_world.min,
					   &aabb_world.max);

	mass = 0;
	centerOfMass_local = {0,0,0};
	centerOfMass_world = centerOfMass_local * collision_detection.mtxColliderToWorld;

	inertialTensor = Matrix(
		0,0,0,0,
		0,0,0,0,
		0,0,0,0,
		0,0,0,1
	);
}

void CVBox::recaculateDerivedDataAsDynamic() {
	aabbGetTransformed(-halfSize,
					   halfSize,
					   collision_detection.mtxColliderToWorld,
					   &aabb_world.min,
					   &aabb_world.max);

	const Vector3 sizes = collision_detection.vecScale * 2 * halfSize;

	const float density = getCollider()->getDensity();
	const float volume = sizes.x * sizes.y * sizes.z;
	mass = calculateMass(density,volume);

	centerOfMass_local = {0,0,0};
	centerOfMass_world = centerOfMass_local * collision_detection.mtxColliderToWorld;

	if(mass > 0) {
		inertialTensor = Matrix::Identity;

		const Vector3 squares = sizes * sizes;
		const float s = 1.f/12.f * mass;

		inertialTensor._11 = s * (squares.y + squares.z);
		inertialTensor._22 = s * (squares.x + squares.z);
		inertialTensor._33 = s * (squares.x + squares.y);
	} else {
		inertialTensor = Matrix(
			0,0,0,0,
			0,0,0,0,
			0,0,0,0,
			0,0,0,1
		);
	}
}