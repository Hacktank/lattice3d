#pragma once

#include "physics/collider/ColliderComponent.h"

#include "core/ConvexHull.h"

class ConvexHullResource;

class __impl_CVConvexHull : public CollisionVolumeNode {
public:
	virtual const ConvexHull* getConvexHull() const = 0;

	__impl_CVConvexHull(const VolumeConstructionData& vcd);

	virtual Vector3 getSupportPointLocalSpace(const Vector3 &dirWorldSpace,bool useCachedData = false) const override;

protected:
	virtual ~__impl_CVConvexHull();

	virtual void recaculateDerivedDataAsStatic() override;
	virtual void recaculateDerivedDataAsDynamic() override;
};

class CVConvexHull : public __impl_CVConvexHull {
public:
	ConvexHull convexHull;

	virtual const ConvexHull* getConvexHull() const override { return &convexHull; }

	CVConvexHull(const VolumeConstructionData& vcd,const ConvexHull& convexHull) : __impl_CVConvexHull(vcd),convexHull(convexHull) {}

protected:
	virtual ~CVConvexHull() {}
};

class CVConvexHullResource : public __impl_CVConvexHull {
public:
	std::shared_ptr<ConvexHullResource> convexHullResource;

	virtual const ConvexHull* getConvexHull() const override;

	CVConvexHullResource(const VolumeConstructionData& vcd,std::shared_ptr<ConvexHullResource>& convexHullResource) : __impl_CVConvexHull(vcd),convexHullResource(convexHullResource) {}

protected:
	virtual ~CVConvexHullResource() {}
};
